<?php
return [
    'recipes all'    => 'all recipes',
    'recipes search' => 'recipes search',
    'only mine'      => 'only mine',
    'search by name' => 'search by name',
    'filter category' => 'filter category',
];
