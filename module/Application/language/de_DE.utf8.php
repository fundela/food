<?php
return [
    'recipes all'    => 'alle Rezepte',
    'recipes search' => 'Rezept Suche',
    'only mine'      => 'nur meine',
    'search by name' => 'nach Name suchen',
    'filter category' => 'Kategorien filtern',
];
