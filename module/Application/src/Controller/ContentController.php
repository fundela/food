<?php

namespace Application\Controller;

use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class ContentController extends AbstractActionController
{
    protected Logger $logger;
    protected string $templatePathStack = '';

    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function setTemplatePathStack(string $templatePathStack): void
    {
        $this->templatePathStack = $templatePathStack;
    }

    public function contentAction(): ViewModel
    {
        $viewModel = new ViewModel();
        $template = 'c/' . $this->params('name') . '.phtml';
        if (file_exists($this->templatePathStack . '/' . $template)) {
            $viewModel->setTemplate($template);
        }
        return $viewModel;
    }
}