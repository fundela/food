<?php

namespace Application\Factory\Controller;

use Application\Controller\ContentController;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Psr\Container\ContainerInterface;

class ContentControllerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): ContentController
    {
        $controller = new ContentController();
        $controller->setLogger($container->get('logger'));
        $templatePathStack = '';
        foreach ($container->get('config')['view_manager']['template_path_stack'] as $path) {
            if(strrpos($path, 'Application/config')) {
                $templatePathStack = $path;
            }
        }
        if(empty($templatePathStack)) {
            throw new ServiceNotCreatedException('Template path stack is empty in class ' . self::class);
        }
        $controller->setTemplatePathStack($templatePathStack);
        return $controller;
    }
}