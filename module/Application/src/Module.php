<?php

declare(strict_types=1);

namespace Application;

use Laminas\EventManager\EventInterface;
use Laminas\Http\Header\SetCookie;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\I18n\Translator\Translator;
use Laminas\I18n\View\Helper\NumberFormat;
use Laminas\Translator\TranslatorInterface;
use Laminas\ModuleManager\Feature\BootstrapListenerInterface;
use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\ModuleRouteListener;
use Laminas\Mvc\MvcEvent;
use Laminas\View\Model\ViewModel;

class Module implements ConfigProviderInterface, BootstrapListenerInterface
{

    /**
     * To show wich locales are installes type 'locale -a' in shell.
     *
     * @var array
     */
    private array $langMapping = [
        'de' => 'de_DE.utf8',
        'en' => 'en_GB.utf8',
    ];
    private string $langMappingDefaultKey = 'de';
    private string $locale = '';
    private string $langVar = '';

    /**
     *
     * @param EventInterface $e
     * @return array
     */
    public function onBootstrap(EventInterface $e)
    {
        if (!$e instanceof MvcEvent) {
            return [];
        }
        $eventManager = $e->getApplication()->getEventManager();
        /*
         * Ohne ModuleRouteListener-attach(EventManager) gibts ein 404 mit message:
         * The requested controller could not be mapped to an existing controller class.
         */
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $eventManager->attach(MvcEvent::EVENT_ROUTE,
            function (MvcEvent $e) {
            /** @var ViewModel $viewModel */
                $viewModel = $e->getViewModel();
                $viewModel->setVariable('routematch', $e->getRouteMatch()->getMatchedRouteName()); // erreichbar in layouts
                $params = $e->getRouteMatch()->getParams();
                $viewModel->setVariable('routematchParams', $params);
                $baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'];
                $viewModel->setVariable('baseUrl', $baseUrl);
                $basePath = parse_url($_SERVER['REQUEST_URI'],  PHP_URL_PATH);
                $viewModel->setVariable('basePath', $basePath);
                $viewModel->setVariable('fullUrl', $baseUrl . $basePath);
            });

        $eventManager->attach(MvcEvent::EVENT_ROUTE,
            function (MvcEvent $e) {
                $request = $e->getRequest();
                $response = $e->getResponse();
                if ($request instanceof Request && $response instanceof Response) {
                    $queryParams = $request->getQuery();
                    $langCookieRequest = substr((isset($request->getCookie()->lang) ? $request->getCookie()->lang : ''), 0, 2);
                    $cookieTime = time() + (30 * 24 * 60 * 60);
                    if (isset($queryParams['lang']) && array_key_exists($queryParams['lang'], $this->langMapping)) {
                        $this->langVar = $queryParams['lang'];
                        $this->locale = $this->langMapping[$queryParams['lang']];
                    } else if (empty($langCookieRequest)) {
                        $acceptLangs = explode(',', filter_input(INPUT_SERVER, 'HTTP_ACCEPT_LANGUAGE'));
                        $langsWithPrior = [];
                        foreach ($acceptLangs as $acceptLang) {
                            if (strpos($acceptLang, ';')) {
                                $tmpLangsWithPrior = explode(';', $acceptLang);
                                $lang = substr($tmpLangsWithPrior[0], 0, 2);
                                $prior = substr($tmpLangsWithPrior[1], 2);
                            } else {
                                $lang = substr($acceptLang, 0, 2);
                                $prior = 1;
                            }
                            $langsWithPrior[$prior] = $lang;
                        }
                        krsort($langsWithPrior);
                        $this->langVar = substr(current($langsWithPrior), 0, 2);
                        if (empty($this->langVar) || !isset($this->langMapping[$this->langVar])) {
                            $this->langVar = $this->langMappingDefaultKey;
                        }
                        $this->locale = $this->langMapping[$this->langVar];
                    } else {
                        $this->langVar = $langCookieRequest;
                        $this->locale = $this->langMapping[$langCookieRequest];
                    }
                    $langCookie = new SetCookie('lang', $this->langVar, $cookieTime, '/');
                    $response->getHeaders()->addHeader($langCookie);
                    /** @var Translator $translator */
                    $translator = $e->getApplication()->getServiceManager()->get(TranslatorInterface::class);
                    if ($translator instanceof Translator) {
                        $translator->setLocale($this->locale);
                    }
                    /** @var NumberFormat $numberFormat */
                    $numberFormat = $e->getApplication()->getServiceManager()->get('ViewHelperManager')->get(NumberFormat::class);
                    if ($numberFormat instanceof NumberFormat) {
                        $numberFormat->setLocale($this->locale);
                    }
                    $e->getViewModel()->setVariable('lang', $this->langVar); // erreichbar in layouts
                    $e->getViewModel()->setVariable('locale', $this->locale); // erreichbar in layouts
                }
            });

        return [];
    }

    public function getConfig(): array
    {
        return include __DIR__ . '/../config/module.config.php';
    }
}
