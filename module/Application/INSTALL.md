Composer must be installed: [getcomposer.org](https://getcomposer.org/)

PHP ^8.0 & PostgreSQL must be installed. [Erste Schritte mit PostgreSQL](https://blog.bitkorn.de/postgresql-zeugz/).

Install PHP Imagemagic:
```shell script
sudo apt install imagick
```

Required PHP extensions:
- simplexml

Clone the application:
```shell script
git clone https://gitlab.com/fundela/food.git
cd food
composer update
```

PostgreSQL Extension installieren (in der PostgreSQL Console):
```postgresql
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

Install the latest database dump from `./data/dbDump/` in a terminal:
```shell script
sudo module/Application/data/db_install.sh food
```

Make files and folders the right permissions:
```shell script
sudo module/Application/data/bin/ch_own_mod.local.sh linuxUsername
```

Configure a virtual Host in apache2 webserver.
Example vhost configuration: [/Application/data/apache/vhost.conf](./data/apache/vhost.conf).

Do not forget to put the url from vhost.conf into `/etc/hosts`.
Then restart apache2:
```shell script
sudo systemctl restart apache2
```

# install for development
### Xdebug mit u.a. PhpStorm
```shell script
# Xdebug installieren
sudo apt install php-xdebug
# eigene/custom PHP config erstellen
sudo gedit /etc/php/yourname.user.ini
# einen symbolischen Link zu dieser Datei "in einer PHP Version" erstellen
sudo ln -s /etc/php/yourname.user.ini /etc/php/7.4/apache2/conf.d/yourname.user.ini
# Apache neu starten
sudo systemctl restart apache2
```
Inhalt für `/etc/php/yourname.user.ini`:
```
[Xdebug]
zend_extension=xdebug.so
xdebug.remote_enable=true
xdebug.remote_autostart=true
xdebug.remote_port=9000
xdebug.idekey=PHPSTORM
```
