--
-- PostgreSQL database dump
--

-- Dumped from database version 14.10 (Ubuntu 14.10-0ubuntu0.22.04.1)
-- Dumped by pg_dump version 14.10 (Ubuntu 14.10-0ubuntu0.22.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


--
-- Name: enum_supported_lang_iso; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.enum_supported_lang_iso AS ENUM (
    'de',
    'en'
);


ALTER TYPE public.enum_supported_lang_iso OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: config; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.config (
    config_id integer NOT NULL,
    config_key character varying(100) NOT NULL,
    config_value text NOT NULL,
    config_desc text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.config OWNER TO postgres;

--
-- Name: COLUMN config.config_desc; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.config.config_desc IS 'for internal use';


--
-- Name: config_config_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.config_config_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.config_config_id_seq OWNER TO postgres;

--
-- Name: config_config_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.config_config_id_seq OWNED BY public.config.config_id;


--
-- Name: country; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.country (
    country_id integer,
    country_name character varying(100),
    country_iso character varying(3),
    country_member_eu integer,
    country_currency_euro integer
);


ALTER TABLE public.country OWNER TO postgres;

--
-- Name: food_ingrdnt; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_ingrdnt (
    food_ingrdnt_id integer NOT NULL,
    food_ingrdnt_name_de character varying(200) DEFAULT ''::character varying NOT NULL,
    food_ingrdnt_name_en character varying(200) DEFAULT ''::character varying NOT NULL,
    food_ingrdnt_name_scient character varying(200) DEFAULT ''::character varying NOT NULL,
    food_ingrdnt_require boolean DEFAULT false NOT NULL,
    food_ingrdnt_order_priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.food_ingrdnt OWNER TO postgres;

--
-- Name: COLUMN food_ingrdnt.food_ingrdnt_require; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_ingrdnt.food_ingrdnt_require IS 'require in food_raw_part for * food_raw ';


--
-- Name: food_ingrdnt_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_ingrdnt_category (
    food_ingrdnt_category_id integer NOT NULL,
    food_ingrdnt_category_name_de character varying(200) NOT NULL,
    food_ingrdnt_category_name_en character varying(200) NOT NULL,
    food_ingrdnt_category_name_scient character varying(200),
    food_ingrdnt_category_order_priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.food_ingrdnt_category OWNER TO postgres;

--
-- Name: food_ingrdnt_category_food_ingrdnt_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.food_ingrdnt_category_food_ingrdnt_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.food_ingrdnt_category_food_ingrdnt_category_id_seq OWNER TO postgres;

--
-- Name: food_ingrdnt_category_food_ingrdnt_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.food_ingrdnt_category_food_ingrdnt_category_id_seq OWNED BY public.food_ingrdnt_category.food_ingrdnt_category_id;


--
-- Name: food_ingrdnt_food_ingrdnt_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.food_ingrdnt_food_ingrdnt_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.food_ingrdnt_food_ingrdnt_id_seq OWNER TO postgres;

--
-- Name: food_ingrdnt_food_ingrdnt_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.food_ingrdnt_food_ingrdnt_id_seq OWNED BY public.food_ingrdnt.food_ingrdnt_id;


--
-- Name: food_raw; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_raw (
    food_raw_uuid uuid NOT NULL,
    food_raw_name_de character varying(200) DEFAULT ''::character varying NOT NULL,
    food_raw_name_en character varying(200) DEFAULT ''::character varying NOT NULL,
    food_raw_name_scient character varying(200) DEFAULT ''::character varying NOT NULL,
    food_raw_desc text DEFAULT ''::text NOT NULL,
    quantityunit_uuid uuid DEFAULT '88835c19-5d32-4f35-9be7-af8ecba46339'::uuid NOT NULL,
    quantityunit_gram real DEFAULT 1 NOT NULL
);


ALTER TABLE public.food_raw OWNER TO postgres;

--
-- Name: COLUMN food_raw.quantityunit_uuid; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_raw.quantityunit_uuid IS 'for food_recipe_part_qntty';


--
-- Name: COLUMN food_raw.quantityunit_gram; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_raw.quantityunit_gram IS 'for food_recipe_part_qntty z.B. Stck: Gramm pro Stck';


--
-- Name: food_raw_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_raw_category (
    food_raw_category_id integer NOT NULL,
    food_raw_category_label character varying(200) NOT NULL,
    food_raw_category_desc text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.food_raw_category OWNER TO postgres;

--
-- Name: food_raw_category_food_raw_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.food_raw_category_food_raw_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.food_raw_category_food_raw_category_id_seq OWNER TO postgres;

--
-- Name: food_raw_category_food_raw_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.food_raw_category_food_raw_category_id_seq OWNED BY public.food_raw_category.food_raw_category_id;


--
-- Name: food_raw_category_rel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_raw_category_rel (
    food_raw_category_rel_id integer NOT NULL,
    food_raw_category_id integer NOT NULL,
    food_raw_uuid uuid NOT NULL
);


ALTER TABLE public.food_raw_category_rel OWNER TO postgres;

--
-- Name: food_raw_category_rel_food_raw_category_rel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.food_raw_category_rel_food_raw_category_rel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.food_raw_category_rel_food_raw_category_rel_id_seq OWNER TO postgres;

--
-- Name: food_raw_category_rel_food_raw_category_rel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.food_raw_category_rel_food_raw_category_rel_id_seq OWNED BY public.food_raw_category_rel.food_raw_category_rel_id;


--
-- Name: food_raw_part; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_raw_part (
    food_raw_part_uuid uuid NOT NULL,
    food_raw_uuid uuid NOT NULL,
    food_ingrdnt_id integer NOT NULL,
    food_raw_part_qntty real DEFAULT 0 NOT NULL
);


ALTER TABLE public.food_raw_part OWNER TO postgres;

--
-- Name: COLUMN food_raw_part.food_raw_part_qntty; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_raw_part.food_raw_part_qntty IS 'in percent';


--
-- Name: food_recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_recipe (
    food_recipe_uuid uuid NOT NULL,
    food_recipe_label character varying(200) DEFAULT ''::character varying NOT NULL,
    food_recipe_alias character varying(200) NOT NULL,
    food_recipe_instrctn text NOT NULL,
    user_uuid uuid,
    food_recipe_ingrdnt_01 real DEFAULT 0 NOT NULL,
    food_recipe_ingrdnt_02 real DEFAULT 0 NOT NULL,
    food_recipe_ingrdnt_03 real DEFAULT 0 NOT NULL,
    food_recipe_ingrdnt_04 real DEFAULT 0 NOT NULL,
    food_recipe_ingrdnt_05 real DEFAULT 0 NOT NULL,
    food_recipe_meta_title character varying(200) DEFAULT ''::character varying NOT NULL,
    food_recipe_meta_keywords text DEFAULT ''::text NOT NULL,
    food_recipe_meta_desc text DEFAULT ''::text NOT NULL,
    food_recipe_time_create timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    food_recipe_time_update timestamp without time zone
);


ALTER TABLE public.food_recipe OWNER TO postgres;

--
-- Name: COLUMN food_recipe.food_recipe_ingrdnt_01; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_recipe.food_recipe_ingrdnt_01 IS 'mapping in fundela/food config [recipe_ingrdnt_id_mapping]';


--
-- Name: COLUMN food_recipe.food_recipe_ingrdnt_02; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_recipe.food_recipe_ingrdnt_02 IS 'mapping in fundela/food config [recipe_ingrdnt_id_mapping]';


--
-- Name: COLUMN food_recipe.food_recipe_ingrdnt_03; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_recipe.food_recipe_ingrdnt_03 IS 'mapping in fundela/food config [recipe_ingrdnt_id_mapping]';


--
-- Name: COLUMN food_recipe.food_recipe_ingrdnt_04; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_recipe.food_recipe_ingrdnt_04 IS 'mapping in fundela/food config [recipe_ingrdnt_id_mapping]';


--
-- Name: COLUMN food_recipe.food_recipe_ingrdnt_05; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_recipe.food_recipe_ingrdnt_05 IS 'mapping in fundela/food config [recipe_ingrdnt_id_mapping]';


--
-- Name: food_recipe_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_recipe_category (
    food_recipe_category_id integer NOT NULL,
    food_recipe_category_label character varying(200) NOT NULL,
    food_recipe_category_desc text DEFAULT ''::text NOT NULL
);


ALTER TABLE public.food_recipe_category OWNER TO postgres;

--
-- Name: food_recipe_category_food_recipe_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.food_recipe_category_food_recipe_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.food_recipe_category_food_recipe_category_id_seq OWNER TO postgres;

--
-- Name: food_recipe_category_food_recipe_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.food_recipe_category_food_recipe_category_id_seq OWNED BY public.food_recipe_category.food_recipe_category_id;


--
-- Name: food_recipe_category_rel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_recipe_category_rel (
    food_recipe_category_rel_id integer NOT NULL,
    food_recipe_category_id integer NOT NULL,
    food_recipe_uuid uuid NOT NULL
);


ALTER TABLE public.food_recipe_category_rel OWNER TO postgres;

--
-- Name: food_recipe_category_rel_food_recipe_category_rel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.food_recipe_category_rel_food_recipe_category_rel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.food_recipe_category_rel_food_recipe_category_rel_id_seq OWNER TO postgres;

--
-- Name: food_recipe_category_rel_food_recipe_category_rel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.food_recipe_category_rel_food_recipe_category_rel_id_seq OWNED BY public.food_recipe_category_rel.food_recipe_category_rel_id;


--
-- Name: food_recipe_image_rel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_recipe_image_rel (
    food_recipe_image_rel_id integer NOT NULL,
    food_recipe_uuid uuid NOT NULL,
    image_id integer,
    food_recipe_image_rel_order_priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.food_recipe_image_rel OWNER TO postgres;

--
-- Name: food_recipe_image_rel_food_recipe_image_rel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.food_recipe_image_rel_food_recipe_image_rel_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.food_recipe_image_rel_food_recipe_image_rel_id_seq OWNER TO postgres;

--
-- Name: food_recipe_image_rel_food_recipe_image_rel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.food_recipe_image_rel_food_recipe_image_rel_id_seq OWNED BY public.food_recipe_image_rel.food_recipe_image_rel_id;


--
-- Name: food_recipe_part; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.food_recipe_part (
    food_recipe_part_uuid uuid NOT NULL,
    food_recipe_uuid uuid NOT NULL,
    food_raw_uuid uuid NOT NULL,
    food_recipe_part_qntty real NOT NULL,
    food_recipe_part_comment character varying(500) DEFAULT ''::character varying NOT NULL,
    food_recipe_part_order_priority integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.food_recipe_part OWNER TO postgres;

--
-- Name: COLUMN food_recipe_part.food_recipe_part_qntty; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.food_recipe_part.food_recipe_part_qntty IS 'with food_raw.quantityunit_uuid';


--
-- Name: image; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.image (
    image_id integer NOT NULL,
    image_label character varying(200) NOT NULL,
    image_desc text DEFAULT ''::text NOT NULL,
    image_filename character varying(200) NOT NULL,
    image_ext character varying(10) NOT NULL,
    image_scaling json DEFAULT '{}'::json NOT NULL,
    image_time_create timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.image OWNER TO postgres;

--
-- Name: COLUMN image.image_scaling; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.image.image_scaling IS 'e.g. "magic2square": [1024,512,256,128,64,32]';


--
-- Name: image_image_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.image_image_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.image_image_id_seq OWNER TO postgres;

--
-- Name: image_image_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.image_image_id_seq OWNED BY public.image.image_id;


--
-- Name: quantityunit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.quantityunit (
    quantityunit_uuid uuid NOT NULL,
    quantityunit_label character varying(100) NOT NULL,
    quantityunit_order_priority integer DEFAULT 0 NOT NULL,
    quantityunit_resolution real DEFAULT 1 NOT NULL,
    quantityunit_resolution_origin uuid
);


ALTER TABLE public.quantityunit OWNER TO postgres;

--
-- Name: COLUMN quantityunit.quantityunit_resolution; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.quantityunit.quantityunit_resolution IS 'https://de.wikipedia.org/wiki/Vors%C3%A4tze_f%C3%BCr_Ma%C3%9Feinheiten';


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    user_uuid uuid NOT NULL,
    user_email character varying(200) NOT NULL,
    user_login character varying(100) NOT NULL,
    user_passwd character varying(200) NOT NULL,
    user_active boolean DEFAULT false NOT NULL,
    user_registration_hash character varying(200),
    user_forgot_passwd_hash character varying(200),
    user_lang_iso public.enum_supported_lang_iso DEFAULT 'de'::public.enum_supported_lang_iso NOT NULL,
    user_time_create timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    user_time_update timestamp without time zone,
    user_role_id integer DEFAULT 5 NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role (
    user_role_id integer NOT NULL,
    user_role_alias character varying(45) NOT NULL,
    user_role_desc character varying(200) DEFAULT ''::character varying NOT NULL,
    user_role_route character varying(200) DEFAULT 'bitkorn_user_html_dashboard_dashboard'::character varying NOT NULL
);


ALTER TABLE public.user_role OWNER TO postgres;

--
-- Name: COLUMN user_role.user_role_route; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.user_role.user_role_route IS 'e.g. route after login';


--
-- Name: user_role_permiss; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role_permiss (
    user_role_permiss_uuid uuid NOT NULL,
    user_role_permiss_alias character varying(100) NOT NULL,
    user_role_permiss_label character varying(100) NOT NULL
);


ALTER TABLE public.user_role_permiss OWNER TO postgres;

--
-- Name: user_role_permiss_rel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_role_permiss_rel (
    user_role_permiss_rel_uuid uuid NOT NULL,
    user_role_id integer NOT NULL,
    user_role_permiss_uuid uuid NOT NULL
);


ALTER TABLE public.user_role_permiss_rel OWNER TO postgres;

--
-- Name: view_recipe; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.view_recipe AS
 SELECT fr.food_recipe_uuid,
    fr.food_recipe_label,
    fr.food_recipe_alias,
    fr.food_recipe_instrctn,
    fr.user_uuid,
    fr.food_recipe_ingrdnt_01,
    fr.food_recipe_ingrdnt_02,
    fr.food_recipe_ingrdnt_03,
    fr.food_recipe_ingrdnt_04,
    fr.food_recipe_ingrdnt_05,
    fr.food_recipe_meta_title,
    fr.food_recipe_meta_keywords,
    fr.food_recipe_meta_desc,
    fr.food_recipe_time_create,
    fr.food_recipe_time_update,
    i.image_id,
    i.image_label,
    i.image_desc,
    i.image_filename,
    i.image_ext,
    i.image_scaling,
    i.image_time_create
   FROM (public.food_recipe fr
     LEFT JOIN public.image i ON ((i.image_id = ( SELECT food_recipe_image_rel.image_id
           FROM public.food_recipe_image_rel
          WHERE (food_recipe_image_rel.food_recipe_uuid = fr.food_recipe_uuid)
         LIMIT 1))));


ALTER TABLE public.view_recipe OWNER TO postgres;

--
-- Name: config config_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.config ALTER COLUMN config_id SET DEFAULT nextval('public.config_config_id_seq'::regclass);


--
-- Name: food_ingrdnt food_ingrdnt_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_ingrdnt ALTER COLUMN food_ingrdnt_id SET DEFAULT nextval('public.food_ingrdnt_food_ingrdnt_id_seq'::regclass);


--
-- Name: food_ingrdnt_category food_ingrdnt_category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_ingrdnt_category ALTER COLUMN food_ingrdnt_category_id SET DEFAULT nextval('public.food_ingrdnt_category_food_ingrdnt_category_id_seq'::regclass);


--
-- Name: food_raw_category food_raw_category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_category ALTER COLUMN food_raw_category_id SET DEFAULT nextval('public.food_raw_category_food_raw_category_id_seq'::regclass);


--
-- Name: food_raw_category_rel food_raw_category_rel_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_category_rel ALTER COLUMN food_raw_category_rel_id SET DEFAULT nextval('public.food_raw_category_rel_food_raw_category_rel_id_seq'::regclass);


--
-- Name: food_recipe_category food_recipe_category_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_category ALTER COLUMN food_recipe_category_id SET DEFAULT nextval('public.food_recipe_category_food_recipe_category_id_seq'::regclass);


--
-- Name: food_recipe_category_rel food_recipe_category_rel_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_category_rel ALTER COLUMN food_recipe_category_rel_id SET DEFAULT nextval('public.food_recipe_category_rel_food_recipe_category_rel_id_seq'::regclass);


--
-- Name: food_recipe_image_rel food_recipe_image_rel_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_image_rel ALTER COLUMN food_recipe_image_rel_id SET DEFAULT nextval('public.food_recipe_image_rel_food_recipe_image_rel_id_seq'::regclass);


--
-- Name: image image_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.image ALTER COLUMN image_id SET DEFAULT nextval('public.image_image_id_seq'::regclass);


--
-- Data for Name: config; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.config (config_id, config_key, config_value, config_desc) FROM stdin;
1	brand_name	KochRezepte.online	
\.


--
-- Data for Name: country; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.country (country_id, country_name, country_iso, country_member_eu, country_currency_euro) FROM stdin;
2	Ägypten	EG	0	0
3	Albanien	AL	0	0
4	Algerien	DZ	0	0
5	Andorra	AD	0	0
6	Angola	AO	0	0
7	Anguilla	AI	0	0
8	Antigua und Barbuda	AG	0	0
9	Äquatorialguinea	GQ	0	0
10	Argentinien	AR	0	0
11	Armenien	AM	0	0
12	Aruba	AW	0	0
13	Aserbaidschan	AZ	0	0
14	Äthiopien	ET	0	0
15	Australien	AU	0	0
16	Azoren	PT	0	0
17	Bahamas	BS	0	0
18	Bahrain	BH	0	0
19	Bangladesch	BD	0	0
20	Barbados	BB	0	0
21	Barbuda (Antigua und Barbuda)	AG	0	0
22	Belarus (Weißrussland)	BY	0	0
23	Belgien	BE	1	1
24	Belize	BZ	0	0
25	Benin	BJ	0	0
26	Bermuda	BM	0	0
27	Bhutan	BT	0	0
28	Bolivien	BO	0	0
29	Bonaire	BQ	0	0
30	Bosnien-Herzegowina	BA	0	0
31	Botswana	BW	0	0
32	Brasilien	BR	0	0
33	Brunei	BN	0	0
34	Bulgarien	BG	1	0
35	Burkina Faso	BF	0	0
36	Burundi	BI	0	0
37	Ceuta	ES	0	0
38	Chile	CL	0	0
39	China, Volksrepublik	CN	0	0
40	Cook-Inseln	CK	0	0
41	Costa Rica	CR	0	0
42	Côte d’Ivoire (Elfenbeinküste)	CI	0	0
43	Curaçao	CW	0	0
44	Dänemark	DK	1	0
48	Deutschland	DE	1	1
49	Dominica	DM	0	0
50	Dominikanische Republik	DO	0	0
51	Dschibuti	DJ	0	0
52	Ecuador	EC	0	0
53	El Salvador	SV	0	0
54	Elfenbeinküste (Côte d’Ivoire)	CI	0	0
55	England	GB	1	0
56	Eritrea	ER	0	0
57	Estland	EE	1	1
58	Färöer-Inseln	FO	0	0
59	Fidschi	FJ	0	0
60	Finnland	FI	1	1
61	Frankreich	FR	1	1
65	Französisch-Guyana	GF	0	0
66	Französisch-Polynesien	PF	0	0
67	Gabun	GA	0	0
68	Gambia	GM	0	0
69	Gaza (Westjordanland)	PS	0	0
70	Georgien	GE	0	0
71	Ghana	GH	0	0
72	Gibraltar	GI	0	0
73	Grenada	GD	0	0
74	Griechenland	GR	1	1
75	Grönland	GL	0	0
77	Großbritannien England	GB	1	0
78	Großbritannien Nordirland	GB	1	0
79	Großbritannien Schottland	GB	1	0
80	Großbritannien Wales	GB	1	0
81	Guadeloupe	GP	0	0
82	Guam	GU	0	0
83	Guatemala	GT	0	0
84	Guinea	GN	0	0
85	Guinea-Bissau	GW	0	0
86	Guyana	GY	0	0
88	Haiti	HT	0	0
89	Honduras	HN	0	0
90	Hongkong	HK	0	0
92	Indien	IN	0	0
93	Indonesien	ID	0	0
94	Irak	IQ	0	0
95	Irland	IE	1	1
96	Island	IS	0	0
97	Israel	IL	0	0
99	Italien	IT	1	1
100	Jamaika	JM	0	0
101	Japan	JP	0	0
102	Jemen	YE	0	0
103	Jordanien	JO	0	0
104	Jungferninseln, amerikanische	VI	0	0
105	Jungferninseln, britische	VG	0	0
106	Kaimaninseln	KY	0	0
107	Kambodscha	KH	0	0
108	Kamerun	CM	0	0
109	Kanada	CA	0	0
110	Kanalinseln (Guernsey)	GG	0	0
111	Kanalinseln (Jersey)	JE	0	0
112	Kanarische Inseln	IC	0	0
113	Kap Verde	CV	0	0
114	Kasachstan	KZ	0	0
115	Katar	QA	0	0
116	Kenia	KE	0	0
117	Kirgistan	KG	0	0
118	Kiribati	KI	0	0
119	Kolumbien	CO	0	0
120	Komoren	KM	0	0
121	Kongo (Brazzaville)	CG	0	0
122	Kongo, Demokratische Republik	CD	0	0
123	Korea, Republik (Südkorea)	KR	0	0
124	Kosovo	RS	0	0
125	Kosrae (Föderierte Staaten von Mikoronesien)	FM	0	0
126	Kroatien	HR	1	0
127	Kuwait	KW	0	0
128	Laos	LA	0	0
129	Lesotho	LS	0	0
130	Lettland	LV	1	1
131	Libanon	LB	0	0
132	Liberia	LR	0	0
133	Libyen	LY	0	0
134	Liechtenstein	LI	0	0
135	Litauen	LT	1	1
136	Luxemburg	LU	1	1
137	Macau	MO	0	0
138	Madagaskar	MG	0	0
139	Madeira	PT	0	0
140	Malawi	MW	0	0
141	Malaysia	MY	0	0
142	Malediven	MV	0	0
143	Mali	ML	0	0
144	Malta	MT	1	1
145	Marianen, Nördliche	MP	0	0
146	Marokko	MA	0	0
147	Marshall-Inseln	MH	0	0
148	Martinique	MQ	0	0
149	Mauretanien	MR	0	0
150	Mauritius	MU	0	0
151	Mayotte	YT	0	0
152	Mazedonien	MK	0	0
153	Melilla	ES	0	0
154	Mexiko	MX	0	0
155	Mikronesien (Föderierte Staaten)	FM	0	0
156	Moldawien	MD	0	0
158	Monaco	MC	0	0
159	Mongolei	MN	0	0
160	Montenegro	ME	0	0
161	Montserrat	MS	0	0
162	Mosambik	MZ	0	0
163	Namibia	NA	0	0
164	Nepal	NP	0	0
165	Neukaledonien	NC	0	0
166	Neuseeland	NZ	0	0
167	Nevis (St. Kitts-Nevis)	KN	0	0
168	Nicaragua	NI	0	0
169	Niederlande	NL	1	1
170	Niger	NE	0	0
171	Nigeria	NG	0	0
172	Nordirland	GB	0	0
173	Norwegen	NO	0	0
174	Oman	OM	0	0
180	Österreich	AT	1	1
181	Osttimor	TL	0	0
182	Pakistan	PK	0	0
183	Palau	PW	0	0
184	Panama	PA	0	0
185	Papua-Neuguinea	PG	0	0
186	Paraguay	PY	0	0
187	Peru	PE	0	0
188	Philippinen	PH	0	0
190	Polen	PL	1	0
191	Pohnpei (Föderierte Staaten von Mikronesien)	FM	0	0
192	Portugal (ausgenommen Azoren und Madeira)	PT	1	1
193	Puerto Rico	PR	0	0
194	Réunion	RE	0	0
195	Rota (Nördliche Marianen)	MP	0	0
196	Ruanda	RW	0	0
197	Rumänien	RO	1	0
198	Russland	RU	0	0
199	Saba	BQ	0	0
200	Saipan (Nördliche Marianen)	MP	0	0
201	Salomonen	SB	0	0
202	Sambia	ZM	0	0
203	Samoa	WS	0	0
204	Samoa-Inseln, amerikanische	AS	0	0
205	San Marino	SM	0	0
206	Saudi-Arabien	SA	0	0
207	Schweden	SE	1	0
208	Schweiz	CH	0	0
209	Schottland	GB	0	0
210	Senegal	SN	0	0
211	Serbien	RS	0	0
212	Seychellen	SC	0	0
213	Sierra Leone	SL	0	0
214	Simbabwe	ZW	0	0
215	Singapur	SG	0	0
216	Slowakische Republik	SK	1	1
217	Slowenien	SI	1	1
218	Spanien (ausgenommen Kanarische Inseln: Ceuta und Melilla)	ES	1	1
219	Sri Lanka	LK	0	0
220	St. Barthélemy	BL	0	0
221	St. Christopher (St. Kitts-Nevis)	KN	0	0
222	St. Croix (Amerik. Jungferninseln)	VI	0	0
223	St. Eustatius	BQ	0	0
224	St. John (Amerik. Jungferninseln)	VI	0	0
225	St. Kitts (St. Kitts-Nevis)	KN	0	0
226	St. Lucia	LC	0	0
227	St. Maarten	SX	0	0
228	St. Martin (Guadeloupe)	SX	0	0
229	St. Thomas (Amerik. Jungferninseln)	VI	0	0
230	St. Vincent und Grenadinen	VC	0	0
231	Südafrika	ZA	0	0
232	Surinam	SR	0	0
233	Swasiland	SZ	0	0
234	Tadschikistan	TJ	0	0
235	Tahiti	PF	0	0
236	Taiwan	TW	0	0
237	Tansania	TZ	0	0
238	Thailand	TH	0	0
239	Tinian (Nördliche Marianen)	MP	0	0
240	Tansania	TZ	0	0
241	Thailand	TH	0	0
242	Tinian (Nördliche Marianen)	MP	0	0
243	Togo	TG	0	0
244	Tonga	TO	0	0
245	Tortola (Britische Jungferninseln)	VG	0	0
246	Trinidad und Tobago	TT	0	0
247	Chuuk (Föderierte Staaten von Mikronesien)	FM	0	0
248	Tschad	TD	0	0
249	Tschechische Republik	CZ	1	0
250	Tunesien	TN	0	0
251	Türkei	TR	0	0
252	Turkmenistan	TM	0	0
253	Turks und Caicos-Inseln	TC	0	0
254	Tuvalu	TV	0	0
255	Uganda	UG	0	0
256	Ukraine	UA	0	0
257	Ungarn	HU	1	0
258	Union-Insel (St. Vincent und Grenadinen)	VC	0	0
259	Uruguay	UY	0	0
260	Usbekistan	UZ	0	0
261	Vanuatu	VU	0	0
262	Venezuela	VE	0	0
263	Vereinigte Arabische Emirate	AE	0	0
264	Vereinigte Staaten von Amerika	US	0	0
265	Vietnam	VN	0	0
266	Virgin-Gorda (Brit. Jungferninseln)	VG	0	0
267	Wales	GB	0	0
268	Wallis und Futuna-Inseln	WF	0	0
269	Weißrussland	BY	0	0
270	Westjordanland (Gaza)	PS	0	0
271	Yap (Föderierte Staaten von Mikronesien)	FM	0	0
272	Zentralafrikanische Republik	CF	0	0
273	Zypern	CY	1	1
274	England - Nordirland	GB	1	0
1	Afghanistan	AF	0	0
\.


--
-- Data for Name: food_ingrdnt; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_ingrdnt (food_ingrdnt_id, food_ingrdnt_name_de, food_ingrdnt_name_en, food_ingrdnt_name_scient, food_ingrdnt_require, food_ingrdnt_order_priority) FROM stdin;
4	Energie	Energie	kJ	t	200
2	Fett	fat	fat	t	190
1	Kohlehydrate	Carb	Saccharide	t	180
5	Eiweis	protein	protein	t	160
6	Wasser	water	H2O	f	0
3	Ballaststoffe	roughage	roughage	t	170
\.


--
-- Data for Name: food_ingrdnt_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_ingrdnt_category (food_ingrdnt_category_id, food_ingrdnt_category_name_de, food_ingrdnt_category_name_en, food_ingrdnt_category_name_scient, food_ingrdnt_category_order_priority) FROM stdin;
1	Hauptnährstoffe	base nutrient	\N	100
2	Vitamine	vitamin	\N	90
\.


--
-- Data for Name: food_raw; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_raw (food_raw_uuid, food_raw_name_de, food_raw_name_en, food_raw_name_scient, food_raw_desc, quantityunit_uuid, quantityunit_gram) FROM stdin;
e13cd0ba-1346-42fe-9e7d-99c65e5385c0	Kakaopulver	cocoa powder		echtes Kakaopulver	88835c19-5d32-4f35-9be7-af8ecba46339	0
11f00dba-6320-4646-8015-ddb2d8b1ca32	Schokotropfen zartbitter	chocolate drops dark		zum Backen	88835c19-5d32-4f35-9be7-af8ecba46339	1
98016611-cb90-467a-82b1-4fc40a9c0487	Butter	butter	butter	Deutsche Markenbutter (de.wikipedia.org/wiki/Butter)	88835c19-5d32-4f35-9be7-af8ecba46339	1
fafcdc34-a19d-4668-a736-6d1c875f1916	Zucker	sugar	Saccharose	umgangssprachlich Haushaltszucker	88835c19-5d32-4f35-9be7-af8ecba46339	1
6538727b-e6d3-4171-a3cd-97d6913bf0d5	Weizenmehl (Type 550)	wheat flour		backstark f&uuml;r feinporige Teige und als Vielzweckmehl verwendbar	88835c19-5d32-4f35-9be7-af8ecba46339	1
1ea9b53a-633d-4e50-bf84-d4dca3436c36	Weizenmehl (Type 405)	wheat flour		bevorzugtes Haushaltsmehl, gute Backeigenschaften	88835c19-5d32-4f35-9be7-af8ecba46339	1
8c3a88b0-6f0e-4976-8335-376a78616f6d	Mandeln	almond		de.wikipedia.org/wiki/Mandelbaum	88835c19-5d32-4f35-9be7-af8ecba46339	1
68f199e3-3b9b-4687-9c3d-d1a0521a4985	Backpulver	baking powder		de.wikipedia.org/wiki/Backpulver 1 Tüte = 16 g	88835c19-5d32-4f35-9be7-af8ecba46339	1
cb08777e-bfb0-4578-98a4-30ab9ec1057f	Kuhmilch (Vollmilch)	cow milk		frische (Supermarkt) Vollmilch	a8a5a9ca-bb34-4107-ac24-f69cec17f10b	1020
e387aa26-ac3c-40e3-ab42-3bf3c58031a6	H&uuml;hnerei (M)	hen egg	Ovum	Gr&ouml;&szlig;e M	39964a72-7c38-4046-958e-515c4286c7da	58
\.


--
-- Data for Name: food_raw_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_raw_category (food_raw_category_id, food_raw_category_label, food_raw_category_desc) FROM stdin;
1	Hülsenfrüchte	Hülsenfrüchte wie Erbsen, Bohnen, Linsen etc
3	Instant und Tüten	Fertigmischungen wie Gemüsebrühe oder Fertig-Pudding
4	Milchprodukte	Milch, Jogurt, Schmand etc
5	Obst	Obst
6	Gemüse	Gemüse
2	Gewürze und Kräuter	Kräuter und Gewürze wie Petersilie oder Pfeffer
7	Convenience-Produkte	Convenience-Produkte sind fertige Gerichte oder Teil-Gerichte
\.


--
-- Data for Name: food_raw_category_rel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_raw_category_rel (food_raw_category_rel_id, food_raw_category_id, food_raw_uuid) FROM stdin;
1	3	e13cd0ba-1346-42fe-9e7d-99c65e5385c0
2	4	98016611-cb90-467a-82b1-4fc40a9c0487
3	1	8c3a88b0-6f0e-4976-8335-376a78616f6d
4	4	cb08777e-bfb0-4578-98a4-30ab9ec1057f
\.


--
-- Data for Name: food_raw_part; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_raw_part (food_raw_part_uuid, food_raw_uuid, food_ingrdnt_id, food_raw_part_qntty) FROM stdin;
458de2df-a560-4258-9cfb-3f8f0772d921	11f00dba-6320-4646-8015-ddb2d8b1ca32	4	2170
325d6912-8b8e-4d26-af2e-834c203900f1	11f00dba-6320-4646-8015-ddb2d8b1ca32	3	8.2
93aac7db-7786-42c0-8632-304efa72f2ab	11f00dba-6320-4646-8015-ddb2d8b1ca32	5	6.3
c5d445f9-00a7-49fe-91fd-4bb06a3e9c95	fafcdc34-a19d-4668-a736-6d1c875f1916	4	1676
045f78ec-d69d-4237-8146-a793ebea222b	fafcdc34-a19d-4668-a736-6d1c875f1916	2	0
e4e3dfc6-0b1c-47f0-9e17-604addf53eb7	fafcdc34-a19d-4668-a736-6d1c875f1916	1	99.9
e85af5a0-398f-45a2-9e33-0573a4bb8bb6	fafcdc34-a19d-4668-a736-6d1c875f1916	5	0
1ea08d57-9016-45e6-858d-74250906f523	fafcdc34-a19d-4668-a736-6d1c875f1916	3	0
323242d0-b42f-49b6-a9de-ec58ef6da77e	68f199e3-3b9b-4687-9c3d-d1a0521a4985	4	648520
5cff0400-2cc7-401c-8bb2-694031498335	68f199e3-3b9b-4687-9c3d-d1a0521a4985	2	0
76720fe8-42d7-475f-a8c4-fda5a83ba5dc	68f199e3-3b9b-4687-9c3d-d1a0521a4985	1	38
2b9b7ef8-5009-4036-bb89-84e5af10b4ee	68f199e3-3b9b-4687-9c3d-d1a0521a4985	5	0.5
78015c51-577e-4e8b-ab7b-59672f9b7655	8c3a88b0-6f0e-4976-8335-376a78616f6d	6	25.9
f3dc41f7-93ec-4d58-990a-66b496627a8a	8c3a88b0-6f0e-4976-8335-376a78616f6d	3	12.2
87da0d5f-d1ba-4c19-9ffa-6a5dd2a20397	cb08777e-bfb0-4578-98a4-30ab9ec1057f	4	272
611f6757-6ebc-4ab0-b0ba-68315d987627	cb08777e-bfb0-4578-98a4-30ab9ec1057f	2	3.5
b5cf9111-710a-4631-9a1b-5118f30b93f7	cb08777e-bfb0-4578-98a4-30ab9ec1057f	1	4.7
49e312b1-a138-4f41-8751-12277b1bff2c	cb08777e-bfb0-4578-98a4-30ab9ec1057f	5	3.4
292b3b09-2b15-491d-a7db-968bda270f4a	cb08777e-bfb0-4578-98a4-30ab9ec1057f	6	87.4
e3cfb26f-8ab8-436b-8f04-d21a8480463e	98016611-cb90-467a-82b1-4fc40a9c0487	4	3066
acc058c4-424d-447a-b1c5-5b84d38ad86b	98016611-cb90-467a-82b1-4fc40a9c0487	2	82
3c92f73e-db15-40b4-a25e-0b24ae6aed3c	98016611-cb90-467a-82b1-4fc40a9c0487	1	0.6
442f6f05-dcc8-4c01-b592-d1e2b64c45bb	98016611-cb90-467a-82b1-4fc40a9c0487	5	0.4
448fcf8f-030a-4dd4-b574-8a84152d9f71	e387aa26-ac3c-40e3-ab42-3bf3c58031a6	4	572
65872193-3731-428a-b2ce-6d7ec3b2e5ae	e387aa26-ac3c-40e3-ab42-3bf3c58031a6	2	9.3
7826c4a0-6064-44ba-b605-70a52a63f018	e387aa26-ac3c-40e3-ab42-3bf3c58031a6	1	1.5
182f456a-c5f2-47ab-9fa2-55f50b5c3ed7	e387aa26-ac3c-40e3-ab42-3bf3c58031a6	5	12
69f8aa5b-6f78-4719-b727-06c2c623a917	e13cd0ba-1346-42fe-9e7d-99c65e5385c0	1	8.8
30869bbc-1ad2-483d-8289-b72e1d048dfe	e13cd0ba-1346-42fe-9e7d-99c65e5385c0	4	1491
0da64a89-9b33-4f52-9078-caeab8b98e8e	e13cd0ba-1346-42fe-9e7d-99c65e5385c0	2	21
d96ccf74-7d21-451f-a16c-6e33cc0cb144	e13cd0ba-1346-42fe-9e7d-99c65e5385c0	5	20
a97a783b-4394-4e20-83db-c00efc9ee313	8c3a88b0-6f0e-4976-8335-376a78616f6d	4	2492
0f093dd0-35b8-45db-98fc-857424350d0b	8c3a88b0-6f0e-4976-8335-376a78616f6d	2	52
2c96f8f2-c605-4e8b-99b7-282588479e79	8c3a88b0-6f0e-4976-8335-376a78616f6d	1	5.8
e367ada1-3f31-4ab8-98f4-c5e19b2d8be6	8c3a88b0-6f0e-4976-8335-376a78616f6d	5	21
75208a9f-cf62-45ee-927e-d27f6cbb8498	1ea9b53a-633d-4e50-bf84-d4dca3436c36	1	72
7fd8a9da-1882-4a13-af5f-3984afc33301	1ea9b53a-633d-4e50-bf84-d4dca3436c36	4	1459
c8ad7515-2655-465e-8780-f206f6e407e8	1ea9b53a-633d-4e50-bf84-d4dca3436c36	2	1
230b611c-8ed8-45a6-af6d-4f089e518c82	1ea9b53a-633d-4e50-bf84-d4dca3436c36	5	10
a6aa4212-1695-4ead-a28c-3a8cc07c7286	11f00dba-6320-4646-8015-ddb2d8b1ca32	2	29
ecb571be-4793-4ea3-b772-bfcf453a0fc5	11f00dba-6320-4646-8015-ddb2d8b1ca32	1	54
e2a5bada-81da-4861-947c-b3d80de5473b	68f199e3-3b9b-4687-9c3d-d1a0521a4985	3	0
34dad477-f21b-4235-9b35-07c23111b29c	1ea9b53a-633d-4e50-bf84-d4dca3436c36	3	20
5380b80c-97c8-4bf0-9f5f-3b42f9f43831	98016611-cb90-467a-82b1-4fc40a9c0487	3	20
48818156-80a3-4c59-ad29-827b52461fbe	cb08777e-bfb0-4578-98a4-30ab9ec1057f	3	87.4
5457cbfd-69e5-4d9f-98d8-3a31a8485200	e13cd0ba-1346-42fe-9e7d-99c65e5385c0	3	20
45c337df-b6ea-4a67-bbeb-332ff46f9a1c	e387aa26-ac3c-40e3-ab42-3bf3c58031a6	3	20
\.


--
-- Data for Name: food_recipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_recipe (food_recipe_uuid, food_recipe_label, food_recipe_alias, food_recipe_instrctn, user_uuid, food_recipe_ingrdnt_01, food_recipe_ingrdnt_02, food_recipe_ingrdnt_03, food_recipe_ingrdnt_04, food_recipe_meta_title, food_recipe_meta_keywords, food_recipe_meta_desc, food_recipe_time_create, food_recipe_time_update, food_recipe_ingrdnt_05) FROM stdin;
a1689395-85c4-4a8a-a723-59629f3784b8	Brownie-Muffin (Low Carb)	brownie-muffin-low-carb	- die Butter in eine Sch&uuml;ssel tun\r\n  - in der Mikrowelle oder Wasserbad weich machen\r\n- Mandeln, Eier, Zucker und Backpulver dazu geben\r\n  - mit einem Handr&uuml;hrger&auml;t lange gut mischen\r\n- das Kakaopulver dazu geben\r\n  - vorsichtig unterr&uuml;hren, sonst gibt es Kakao Wolken :)\r\n  - mit einem Handr&uuml;hrger&auml;t lange gut mischen\r\n- nach Gef&uuml;hl Milch zu geben\r\n  - so viel, dass der Teig noch feste ist, sonst sammeln sich die Schokotropfen beim Backen unten\r\n- mit einem Handr&uuml;hrger&auml;t lange gut mischen\r\n- die Schokotropfen unterr&uuml;hren\r\n\r\n- eine flache Backform fetten und den Teig in die Backform geben\r\n- bei 180&deg;, 30 - 40 Minuten backen\r\n\r\n[spezial] das Mark einer Vanilleschote dazu geben wenn die Butter weich ist.	2c928c37-1939-4ea5-bd29-26f574088b7f	11678.16	34.490097	18.152761	10.246476	Brownie-Muffin - Low Carb	brownie,muffin,low carb	Brownie-Muffin mit Mandelmehl statt Weizenmehl	2020-09-26 05:40:28.974375	2023-12-20 10:46:54.802702	20.814095
\.


--
-- Data for Name: food_recipe_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_recipe_category (food_recipe_category_id, food_recipe_category_label, food_recipe_category_desc) FROM stdin;
1	Frühstück	Frühstück
2	Snack	Snack
3	Salat	Salat
4	Low Carb	Low Carb - wenig Kohlehydrate
5	Suppe	Suppe
6	Vegetarisch	Vegetarisch
7	Vegan	Vegan
8	Grundrezept	Grundrezept
9	Brotaufstrich	Brotaufstrich
\.


--
-- Data for Name: food_recipe_category_rel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_recipe_category_rel (food_recipe_category_rel_id, food_recipe_category_id, food_recipe_uuid) FROM stdin;
29	4	a1689395-85c4-4a8a-a723-59629f3784b8
\.


--
-- Data for Name: food_recipe_image_rel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_recipe_image_rel (food_recipe_image_rel_id, food_recipe_uuid, image_id, food_recipe_image_rel_order_priority) FROM stdin;
\.


--
-- Data for Name: food_recipe_part; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.food_recipe_part (food_recipe_part_uuid, food_recipe_uuid, food_raw_uuid, food_recipe_part_qntty, food_recipe_part_comment, food_recipe_part_order_priority) FROM stdin;
37d9689a-49f7-406a-bc34-ba38b55bc835	a1689395-85c4-4a8a-a723-59629f3784b8	98016611-cb90-467a-82b1-4fc40a9c0487	150	in der Mikrowelle weich machen	1000
2e562354-8800-40c3-8070-af44b4303b0d	a1689395-85c4-4a8a-a723-59629f3784b8	8c3a88b0-6f0e-4976-8335-376a78616f6d	300	fein gemahlen	990
23952550-f1e6-4d21-ae3f-28d56716b655	a1689395-85c4-4a8a-a723-59629f3784b8	e387aa26-ac3c-40e3-ab42-3bf3c58031a6	4	no comment	980
a0b96513-bd6b-4733-ad2f-fe2fc9a1668f	a1689395-85c4-4a8a-a723-59629f3784b8	68f199e3-3b9b-4687-9c3d-d1a0521a4985	16	eine T&uuml;te	970
b10cafa2-c47a-47d9-acb5-4ad8ac96126f	a1689395-85c4-4a8a-a723-59629f3784b8	fafcdc34-a19d-4668-a736-6d1c875f1916	50	brauner Zucker ist lecker :)	960
cdd34ff8-5597-4b2b-a161-de7584f232c3	a1689395-85c4-4a8a-a723-59629f3784b8	cb08777e-bfb0-4578-98a4-30ab9ec1057f	0.1	so viel, dass der Teig noch fest ist. Sonst sacken die Schokotropfen ab	950
de099b24-f283-481d-a53e-e5de4e998f60	a1689395-85c4-4a8a-a723-59629f3784b8	e13cd0ba-1346-42fe-9e7d-99c65e5385c0	80		940
6f64ebbb-01af-4508-8ee7-cccfdee05b23	a1689395-85c4-4a8a-a723-59629f3784b8	11f00dba-6320-4646-8015-ddb2d8b1ca32	200		930
\.


--
-- Data for Name: image; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.image (image_id, image_label, image_desc, image_filename, image_ext, image_scaling, image_time_create) FROM stdin;
20	Essen Foto :)	keine desc	essen-foto_1600247696	jpg	{"original":1,"m2s":[1024,512,256,128,64,32,16]}	2020-09-16 11:14:56.975909
21	Brownie Muffins	Brownie Muffins aus dem Netz gezockt	brownie-muffins_1600337927	jpg	{"original":1,"m2s":[1024,512,256,128,64,32,16]}	2020-09-17 12:18:47.194718
22	Chocolate Brownie Muffins	Chocolate Brownie Muffins aus dem Netz gezockt	chocolate-brownie-muffins_1600337953	jpg	{"original":1,"m2s":[1024,512,256,128,64,32,16]}	2020-09-17 12:19:13.760353
\.


--
-- Data for Name: quantityunit; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.quantityunit (quantityunit_uuid, quantityunit_label, quantityunit_order_priority, quantityunit_resolution, quantityunit_resolution_origin) FROM stdin;
39964a72-7c38-4046-958e-515c4286c7da	Stck.	100	1	\N
a8a5a9ca-bb34-4107-ac24-f69cec17f10b	Liter	75	1	\N
efede62c-77a1-4c7f-8960-64847d7c63dd	ml	73	0.001	a8a5a9ca-bb34-4107-ac24-f69cec17f10b
88835c19-5d32-4f35-9be7-af8ecba46339	g	150	1	\N
\.


--
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (user_uuid, user_email, user_login, user_passwd, user_active, user_registration_hash, user_forgot_passwd_hash, user_lang_iso, user_time_create, user_time_update, user_role_id) FROM stdin;
2c928c37-1939-4ea5-bd29-26f574088b7f	mail@bitkorn.de	allapow	7e313ef519ab7c32e796f2367294e450bb79aa0bc58d28d49e258d4ec9ded9afe2dc0e48c7c718fb906bcab02d3ecefcb40ef565316cf3c5c42bd31ba891ff68	t	\N	\N	de	2020-07-09 21:14:21.659977	\N	1
99719440-f153-4226-b5c7-ded7b1235265	testtext@bitkorn.de	stefan	7e313ef519ab7c32e796f2367294e450bb79aa0bc58d28d49e258d4ec9ded9afe2dc0e48c7c718fb906bcab02d3ecefcb40ef565316cf3c5c42bd31ba891ff68	t	\N	\N	en	2020-08-13 20:11:55.140326	\N	4
9264724e-6847-40b1-ada8-d5e8e8937e21	amandia@bitkorn.de	sone	7e313ef519ab7c32e796f2367294e450bb79aa0bc58d28d49e258d4ec9ded9afe2dc0e48c7c718fb906bcab02d3ecefcb40ef565316cf3c5c42bd31ba891ff68	t	\N	\N	de	2020-08-17 14:14:21.659	\N	1
f2160630-5632-4845-8689-e0272038d72e	onkel@bitkorn.de	onkel	7e313ef519ab7c32e796f2367294e450bb79aa0bc58d28d49e258d4ec9ded9afe2dc0e48c7c718fb906bcab02d3ecefcb40ef565316cf3c5c42bd31ba891ff68	t		\N	de	2020-09-18 09:32:08.522192	\N	5
\.


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_role (user_role_id, user_role_alias, user_role_desc, user_role_route) FROM stdin;
1	god		bitkorn_user_html_dashboard_dashboard
2	admin		bitkorn_user_html_dashboard_dashboard
3	manager		bitkorn_user_html_dashboard_dashboard
4	author		bitkorn_user_html_dashboard_dashboard
5	user		bitkorn_user_html_dashboard_dashboard
\.


--
-- Data for Name: user_role_permiss; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_role_permiss (user_role_permiss_uuid, user_role_permiss_alias, user_role_permiss_label) FROM stdin;
\.


--
-- Data for Name: user_role_permiss_rel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_role_permiss_rel (user_role_permiss_rel_uuid, user_role_id, user_role_permiss_uuid) FROM stdin;
\.


--
-- Name: config_config_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.config_config_id_seq', 1, true);


--
-- Name: food_ingrdnt_category_food_ingrdnt_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.food_ingrdnt_category_food_ingrdnt_category_id_seq', 2, true);


--
-- Name: food_ingrdnt_food_ingrdnt_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.food_ingrdnt_food_ingrdnt_id_seq', 6, true);


--
-- Name: food_raw_category_food_raw_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.food_raw_category_food_raw_category_id_seq', 7, true);


--
-- Name: food_raw_category_rel_food_raw_category_rel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.food_raw_category_rel_food_raw_category_rel_id_seq', 4, true);


--
-- Name: food_recipe_category_food_recipe_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.food_recipe_category_food_recipe_category_id_seq', 9, true);


--
-- Name: food_recipe_category_rel_food_recipe_category_rel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.food_recipe_category_rel_food_recipe_category_rel_id_seq', 34, true);


--
-- Name: food_recipe_image_rel_food_recipe_image_rel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.food_recipe_image_rel_food_recipe_image_rel_id_seq', 14, true);


--
-- Name: image_image_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.image_image_id_seq', 22, true);


--
-- Name: food_ingrdnt food_ingrdnt_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_ingrdnt
    ADD CONSTRAINT food_ingrdnt_pk PRIMARY KEY (food_ingrdnt_id);


--
-- Name: food_raw_category food_raw_category_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_category
    ADD CONSTRAINT food_raw_category_pk PRIMARY KEY (food_raw_category_id);


--
-- Name: food_raw_category_rel food_raw_category_rel_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_category_rel
    ADD CONSTRAINT food_raw_category_rel_pk PRIMARY KEY (food_raw_category_rel_id);


--
-- Name: food_raw_part food_raw_part_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_part
    ADD CONSTRAINT food_raw_part_pk PRIMARY KEY (food_raw_part_uuid);


--
-- Name: food_raw food_raw_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw
    ADD CONSTRAINT food_raw_pk PRIMARY KEY (food_raw_uuid);


--
-- Name: food_recipe_category food_recipe_category_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_category
    ADD CONSTRAINT food_recipe_category_pk PRIMARY KEY (food_recipe_category_id);


--
-- Name: food_recipe_category_rel food_recipe_category_rel_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_category_rel
    ADD CONSTRAINT food_recipe_category_rel_pk PRIMARY KEY (food_recipe_category_rel_id);


--
-- Name: food_recipe_image_rel food_recipe_image_rel_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_image_rel
    ADD CONSTRAINT food_recipe_image_rel_pk PRIMARY KEY (food_recipe_image_rel_id);


--
-- Name: food_recipe_part food_recipe_part_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_part
    ADD CONSTRAINT food_recipe_part_pk PRIMARY KEY (food_recipe_part_uuid);


--
-- Name: food_recipe food_recipe_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe
    ADD CONSTRAINT food_recipe_pk PRIMARY KEY (food_recipe_uuid);


--
-- Name: image image_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.image
    ADD CONSTRAINT image_pk PRIMARY KEY (image_id);


--
-- Name: quantityunit quantityunit_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quantityunit
    ADD CONSTRAINT quantityunit_pk PRIMARY KEY (quantityunit_uuid);


--
-- Name: user user_email_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_email_un UNIQUE (user_email);


--
-- Name: user user_login_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_login_un UNIQUE (user_login);


--
-- Name: user user_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (user_uuid);


--
-- Name: user_role_permiss user_role_permiss_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role_permiss
    ADD CONSTRAINT user_role_permiss_pk PRIMARY KEY (user_role_permiss_uuid);


--
-- Name: user_role_permiss_rel user_role_permiss_rel_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role_permiss_rel
    ADD CONSTRAINT user_role_permiss_rel_pk PRIMARY KEY (user_role_permiss_rel_uuid);


--
-- Name: user_role user_role_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role
    ADD CONSTRAINT user_role_pk PRIMARY KEY (user_role_id);


--
-- Name: config_config_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX config_config_id_uindex ON public.config USING btree (config_id);


--
-- Name: config_config_key_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX config_config_key_uindex ON public.config USING btree (config_key);


--
-- Name: food_raw_category_food_raw_category_label_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX food_raw_category_food_raw_category_label_uindex ON public.food_raw_category USING btree (food_raw_category_label);


--
-- Name: food_recipe_category_food_recipe_category_label_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX food_recipe_category_food_recipe_category_label_uindex ON public.food_recipe_category USING btree (food_recipe_category_label);


--
-- Name: food_recipe_food_recipe_alias_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX food_recipe_food_recipe_alias_uindex ON public.food_recipe USING btree (food_recipe_alias);


--
-- Name: i_user_role_id; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX i_user_role_id ON public.user_role USING btree (user_role_id);


--
-- Name: quantityunit_quantityunit_label_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX quantityunit_quantityunit_label_uindex ON public.quantityunit USING btree (quantityunit_label);


--
-- Name: user_role_user_role_alias_idx; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX user_role_user_role_alias_idx ON public.user_role USING btree (user_role_alias);


--
-- Name: food_raw_category_rel food_raw_category_rel_food_raw_category_food_raw_category_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_category_rel
    ADD CONSTRAINT food_raw_category_rel_food_raw_category_food_raw_category_id_fk FOREIGN KEY (food_raw_category_id) REFERENCES public.food_raw_category(food_raw_category_id);


--
-- Name: food_raw_category_rel food_raw_category_rel_food_raw_food_raw_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_category_rel
    ADD CONSTRAINT food_raw_category_rel_food_raw_food_raw_uuid_fk FOREIGN KEY (food_raw_uuid) REFERENCES public.food_raw(food_raw_uuid);


--
-- Name: food_raw_part food_raw_part_food_ingrdnt_food_ingrdnt_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_part
    ADD CONSTRAINT food_raw_part_food_ingrdnt_food_ingrdnt_id_fk FOREIGN KEY (food_ingrdnt_id) REFERENCES public.food_ingrdnt(food_ingrdnt_id);


--
-- Name: food_raw_part food_raw_part_food_raw_food_raw_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw_part
    ADD CONSTRAINT food_raw_part_food_raw_food_raw_uuid_fk FOREIGN KEY (food_raw_uuid) REFERENCES public.food_raw(food_raw_uuid);


--
-- Name: food_raw food_raw_quantityunit_quantityunit_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_raw
    ADD CONSTRAINT food_raw_quantityunit_quantityunit_uuid_fk FOREIGN KEY (quantityunit_uuid) REFERENCES public.quantityunit(quantityunit_uuid);


--
-- Name: food_recipe_category_rel food_recipe_category_rel_food_recipe_category_food_recipe_categ; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_category_rel
    ADD CONSTRAINT food_recipe_category_rel_food_recipe_category_food_recipe_categ FOREIGN KEY (food_recipe_category_id) REFERENCES public.food_recipe_category(food_recipe_category_id);


--
-- Name: food_recipe_category_rel food_recipe_category_rel_food_recipe_food_recipe_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_category_rel
    ADD CONSTRAINT food_recipe_category_rel_food_recipe_food_recipe_uuid_fk FOREIGN KEY (food_recipe_uuid) REFERENCES public.food_recipe(food_recipe_uuid);


--
-- Name: food_recipe_part food_recipe_part_food_raw_food_raw_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_part
    ADD CONSTRAINT food_recipe_part_food_raw_food_raw_uuid_fk FOREIGN KEY (food_raw_uuid) REFERENCES public.food_raw(food_raw_uuid);


--
-- Name: food_recipe_part food_recipe_part_food_recipe_food_recipe_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe_part
    ADD CONSTRAINT food_recipe_part_food_recipe_food_recipe_uuid_fk FOREIGN KEY (food_recipe_uuid) REFERENCES public.food_recipe(food_recipe_uuid);


--
-- Name: food_recipe food_recipe_user_user_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.food_recipe
    ADD CONSTRAINT food_recipe_user_user_uuid_fk FOREIGN KEY (user_uuid) REFERENCES public."user"(user_uuid);


--
-- Name: quantityunit quantityunit_quantityunit_quantityunit_uuid_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.quantityunit
    ADD CONSTRAINT quantityunit_quantityunit_quantityunit_uuid_fk FOREIGN KEY (quantityunit_resolution_origin) REFERENCES public.quantityunit(quantityunit_uuid);


--
-- Name: user_role_permiss_rel user_role_permiss_rel_user_role_permiss_user_role_permiss_uuid_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role_permiss_rel
    ADD CONSTRAINT user_role_permiss_rel_user_role_permiss_user_role_permiss_uuid_ FOREIGN KEY (user_role_permiss_uuid) REFERENCES public.user_role_permiss(user_role_permiss_uuid);


--
-- Name: user_role_permiss_rel user_role_permiss_rel_user_role_user_role_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_role_permiss_rel
    ADD CONSTRAINT user_role_permiss_rel_user_role_user_role_id_fk FOREIGN KEY (user_role_id) REFERENCES public.user_role(user_role_id);


--
-- Name: user user_user_role_user_role_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_user_role_user_role_id_fk FOREIGN KEY (user_role_id) REFERENCES public.user_role(user_role_id);


--
-- PostgreSQL database dump complete
--

