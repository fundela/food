#!/usr/bin/env bash
dbName="food"
verz="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
dateString=$(date +%Y%m%d%H%M)
fqfn="$verz/dbDump/${dbName}_$dateString.sql"
sudo -u postgres pg_dump $dbName | sudo tee "$fqfn"
sudo chmod 644 "$fqfn"
sudo chown allapow:www-data "$fqfn"
