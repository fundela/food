alter table public.food_recipe
    add food_recipe_prep_time varchar(20);

comment on column public.food_recipe.food_recipe_prep_time is 'ISO 8601';

alter table public.food_recipe
    add food_recipe_cook_time varchar(20);

comment on column public.food_recipe.food_recipe_cook_time is 'ISO 8601';

alter table public.food_recipe
    add food_recipe_yield varchar(100);

comment on column public.food_recipe.food_recipe_yield is 'Ergebnis / Ausbeute';

-- UPDATE view: add 3 columns to the view
create or replace view public.view_recipe
as
SELECT fr.food_recipe_uuid,
       fr.food_recipe_label,
       fr.food_recipe_alias,
       fr.food_recipe_instrctn,
       fr.user_uuid,
       fr.food_recipe_ingrdnt_01,
       fr.food_recipe_ingrdnt_02,
       fr.food_recipe_ingrdnt_03,
       fr.food_recipe_ingrdnt_04,
       fr.food_recipe_ingrdnt_05,
       fr.food_recipe_meta_title,
       fr.food_recipe_meta_keywords,
       fr.food_recipe_meta_desc,
       fr.food_recipe_time_create,
       fr.food_recipe_time_update,
       fr.food_recipe_prep_time,
       fr.food_recipe_cook_time,
       fr.food_recipe_yield,
       i.image_id,
       i.image_label,
       i.image_desc,
       i.image_filename,
       i.image_ext,
       i.image_scaling,
       i.image_time_create
FROM food_recipe fr
         LEFT JOIN image i ON i.image_id = ((SELECT food_recipe_image_rel.image_id
                                             FROM food_recipe_image_rel
                                             WHERE food_recipe_image_rel.food_recipe_uuid = fr.food_recipe_uuid
    LIMIT 1));

alter table public.view_recipe
    owner to postgres;

-- only a comment :)
comment on table public.food_recipe_part is 'or food_recipe_raw';
comment on table public.food_raw_part is 'or food_raw_ingrdnt';

-- NEW view
CREATE OR REPLACE VIEW public.view_recipe_part
AS
SELECT frp.food_recipe_part_uuid,
       frp.food_recipe_uuid,
       frp.food_raw_uuid,
       frp.food_recipe_part_qntty,
       frp.food_recipe_part_comment,
       frp.food_recipe_part_order_priority,
       fr.food_raw_name_de,
       fr.food_raw_name_en,
       fr.food_raw_name_scient,
       fr.food_raw_desc,
       fr.quantityunit_uuid,
       fr.quantityunit_gram,
       qu.quantityunit_label,
       qu.quantityunit_resolution,
       qu.quantityunit_resolution_origin,
       quo.quantityunit_label AS quantityunit_label_origin,
       (SELECT food_raw_part_qntty FROM food_raw_part WHERE food_raw_uuid = fr.food_raw_uuid AND food_ingrdnt_id = 1) AS ingrdnt_01_percent,
       (SELECT food_raw_part_qntty FROM food_raw_part WHERE food_raw_uuid = fr.food_raw_uuid AND food_ingrdnt_id = 2) AS ingrdnt_02_percent,
       (SELECT food_raw_part_qntty FROM food_raw_part WHERE food_raw_uuid = fr.food_raw_uuid AND food_ingrdnt_id = 3) AS ingrdnt_03_percent,
       (SELECT food_raw_part_qntty FROM food_raw_part WHERE food_raw_uuid = fr.food_raw_uuid AND food_ingrdnt_id = 4) AS ingrdnt_04_percent,
       (SELECT food_raw_part_qntty FROM food_raw_part WHERE food_raw_uuid = fr.food_raw_uuid AND food_ingrdnt_id = 5) AS ingrdnt_05_percent
FROM food_recipe_part frp
         LEFT JOIN food_raw fr ON frp.food_raw_uuid = fr.food_raw_uuid
         LEFT JOIN quantityunit qu ON fr.quantityunit_uuid = qu.quantityunit_uuid
         LEFT JOIN quantityunit quo ON qu.quantityunit_resolution_origin = quo.quantityunit_uuid;

ALTER TABLE public.view_recipe_part
    OWNER TO postgres;

