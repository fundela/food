#!/bin/bash
if [ $# -eq 0 ] || [ -z "$1" ]
then
  echo "no Linux user is supplied"
  exit
fi
username=$1
verz="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$verz/../../../../" || exit
chmodFolder=775
chmodFile=664

if [ ! -d "data/cache" ]
then
mkdir "data/cache"
fi

if [ ! -d "data/log" ]
then
mkdir "data/log"
fi

if [ ! -f "data/log/app.log" ]
then
touch "data/log/app.log"
fi

if [ ! -d "data/files" ]
then
mkdir "data/files"
fi

if [ ! -d "data/files/upload" ]
then
mkdir "data/files/upload"
fi

if [ ! -d "data/tmp" ]
then
mkdir "data/tmp"
fi

if [ ! -d "public/img/fundela" ]
then
mkdir "public/img/fundela"
fi

find . -type d -not -path "./.git*" -exec chmod "$chmodFolder" {} \;
find . -type f -not -path "./.git*" -exec chmod "$chmodFile" {} \;

find . -name "*.sh" -type f -not -path "./.git*" -not -path "./vendor" -exec chmod ug+x {} \;

chown -R "$username":www-data .
