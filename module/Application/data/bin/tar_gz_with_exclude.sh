#!/bin/bash
verz="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$verz/../../../"
tarDir="godata2"
outDir="/home/allapow/Downloads"
dateString=`date +%Y%m%d%H%M`
fileNamePost="_some.tar.gz"
tar -czf "$outDir/godata2_$dateString$fileNamePost" --exclude=".git" --exclude="vendor" --exclude="nbproject" --exclude=".idea" $tarDir
