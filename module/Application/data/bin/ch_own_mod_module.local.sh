#!/bin/bash
if [ $# -eq 0 ] || [ -z "$1" ]
then
  echo "no user is supplied"
  exit
fi
username=$1
verz="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
cd "$verz/../../../../" || exit
chmodFolder=775
chmodFile=664

find . -type d -not -path "./.git*" -exec chmod "$chmodFolder" {} \;
find . -type f -not -path "./.git*" -exec chmod "$chmodFile" {} \;

find . -name "*.sh" -type f -not -path "./.git*" -not -path "./vendor" -exec chmod ug+x {} \;

chown -R "$username":www-data .
