#!/bin/bash
verz="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
cd "$verz" || exit
dbName="food"
echo "lösche Datenbank: $dbName"
sudo -u postgres dropdb $dbName

echo "erstelle Datenbank: $dbName"
sudo -u postgres createdb "$dbName"

filename=$(ls -t dbDump | head -n1)
echo "fülle Datenbank aus Datei dbDump/${filename}"
sudo -u postgres psql "$dbName" < "dbDump/${filename}"
