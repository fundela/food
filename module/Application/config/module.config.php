<?php

declare(strict_types=1);

namespace Application;

use Laminas\I18n\Translator\Loader\PhpArray;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use Laminas\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'content' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/c/:name',
                    'constraints' => [
                        'name' => '[A-Za-z-]+',
                    ],
                    'defaults' => [
                        'controller' => Controller\ContentController::class,
                        'action' => 'content',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            Controller\IndexController::class => InvokableFactory::class,
            Controller\ContentController::class => Factory\Controller\ContentControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => [
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'layout/clean' => __DIR__ . '/../view/layout/clean.phtml',
            'layout/empty' => __DIR__ . '/../view/layout/empty.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'translator' => [
        /**
         * translation for the application
         */
        'locale' => ['de_DE.utf8', 'en_GB.utf8'],
        'translation_file_patterns' => [
            [
                'type'     => PhpArray::class,
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.php',
                'text_domain' => 'food'
            ],
        ],
    ],
];
