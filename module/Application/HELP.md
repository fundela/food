1 kcal = 4,184 kJ

1 cal = 4,184 J


## Wikipedia.org
[Nährwert](https://de.wikipedia.org/wiki/N%C3%A4hrwert)

## Daten
- [ernaehrung.de](http://www.ernaehrung.de/) u.a. Nährwerte Datenbank durchsuchen
  - auch eigene Rezeptsoftware [nutrigourmet](http://www.ernaehrung.de/rezepte/nutrigourmet/)
- [vegane-proteinquellen.de](https://vegane-proteinquellen.de/lebensmittel/nuesse-samen/)

### Kategorisierung
- food_raw_category
  - [wikipedia.org/Bundeslebensmittelschlüssel](https://de.wikipedia.org/wiki/Bundeslebensmittelschl%C3%BCssel)
  - [ndr.de Zutaten-Lexikon](https://www.ndr.de/ratgeber/kochen/rezepte/rezeptdb145.html?kid=3)

- food_recipe_category
  - [GU Rezepte nach Kategorien](https://www.kuechengoetter.de/rezepte)
  - [eatsmarter.de/rezeptkategorien/suche-nach-hauptkategorien](https://eatsmarter.de/rezepte/rezeptkategorien/suche-nach-hauptkategorien)

- zum Kategorisieren der food_ingrdnt
  - [lebensmittelverband.de/inhaltsstoffe](https://www.lebensmittelverband.de/de/lebensmittel/inhaltsstoffe)


- Ingredient Kategorien
  - Basic (Energie, Fett, Kohlehydrate, Ballaststoffe, Eiweiß & Salz)
  - Vitamine
  - Mineralstoffe
  - Zusatzstoffe
  - Süßstoffe
  - Aromen

