<?php

namespace Fundela\FoodAdmin;

use Fundela\FoodAdmin\Controller\FoodIngrdntController;
use Fundela\FoodAdmin\Controller\FoodRawController;
use Fundela\FoodAdmin\Factory\Controller\FoodIngrdntControllerFactory;
use Fundela\FoodAdmin\Factory\Controller\FoodRawControllerFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            /*
             * FoodIngrdnt
             */
            'fundela_foodadmin_ingrdnt_form' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/ingrdnt-form[/:food_ingrdnt_id][/]',
                    'constraints' => [
                        'food_ingrdnt_id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => FoodIngrdntController::class,
                        'action' => 'form',
                    ],
                ],
            ],
            'fundela_foodadmin_ingrdnt_ingrdnts' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/ingrdnts',
                    'defaults' => [
                        'controller' => FoodIngrdntController::class,
                        'action' => 'ingrdnts',
                    ],
                ],
            ],
            /*
             * FoodRaw
             */
            'fundela_foodadmin_raw_form' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/raw-form[/:food_raw_uuid][/]',
                    'constraints' => [
                        'food_raw_uuid' => '[0-9a-zA-Z-]+',
                    ],
                    'defaults' => [
                        'controller' => FoodRawController::class,
                        'action' => 'form',
                    ],
                ],
            ],
            'fundela_foodadmin_raw_raws' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/raws',
                    'defaults' => [
                        'controller' => FoodRawController::class,
                        'action' => 'raws',
                    ],
                ],
            ],
            'fundela_foodadmin_raw_rawparts' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/rawparts[/:food_raw_uuid][/]',
                    'constraints' => [
                        'food_raw_uuid' => '[0-9a-zA-Z-]+',
                    ],
                    'defaults' => [
                        'controller' => FoodRawController::class,
                        'action' => 'rawParts',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            FoodIngrdntController::class => FoodIngrdntControllerFactory::class,
            FoodRawController::class => FoodRawControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [],
        'invokables' => [],
    ],
    'view_helpers' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
