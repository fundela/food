<?php

namespace Fundela\FoodAdmin\Factory\Controller;

use Fundela\Food\Form\Raw\FoodRawForm;
use Fundela\Food\Service\Raw\FoodRawPartService;
use Fundela\Food\Service\Raw\FoodRawService;
use Fundela\User\Service\UserService;
use Fundela\FoodAdmin\Controller\FoodRawController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRawControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodRawController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodRawForm($container->get(FoodRawForm::class));
        $controller->setFoodRawService($container->get(FoodRawService::class));
        $controller->setFoodRawPartService($container->get(FoodRawPartService::class));
        return $controller;
    }
}
