<?php

namespace Fundela\FoodAdmin\Controller;

use Fundela\Food\Form\Ingrdnt\FoodIngrdntForm;
use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Entity\Messages;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Paginator\Adapter\LaminasDb\DbSelect;
use Laminas\Paginator\Paginator;
use Laminas\View\Model\ViewModel;

class FoodIngrdntController extends AbstractUserController
{
    protected FoodIngrdntForm $foodIngrdntForm;
    protected FoodIngrdntService $foodIngrdntService;

    /**
     * @param FoodIngrdntForm $foodIngrdntForm
     */
    public function setFoodIngrdntForm(FoodIngrdntForm $foodIngrdntForm): void
    {
        $this->foodIngrdntForm = $foodIngrdntForm;
    }

    /**
     * @param FoodIngrdntService $foodIngrdntService
     */
    public function setFoodIngrdntService(FoodIngrdntService $foodIngrdntService): void
    {
        $this->foodIngrdntService = $foodIngrdntService;
    }

    /**
     * @return Response|ViewModel
     */
    public function formAction()
    {
        $viewModel = new ViewModel();
        if (!$this->userService->isUserInRole(4)) {
            return $this->redirect()->toRoute('home');
        }
        $request = $this->getRequest();
        $foodIngrdntId = $this->params('food_ingrdnt_id');
        if ($foodIngrdntId) {
            $this->foodIngrdntForm->setPrimaryKeyAvailable(true);
            $this->foodIngrdntForm->init();
            if ($request->isget()) {
                $foodIngrdnt = $this->foodIngrdntService->getFoodIngrdnt($foodIngrdntId);
                $this->foodIngrdntForm->setData($foodIngrdnt);
            }
        } else {
            $this->foodIngrdntForm->init();
            $viewModel->setVariable('isNew', true);
        }
        if ($request->isPost()) {
            $post = $request->getPost()->toArray();
            $this->foodIngrdntForm->setData($post);
            if ($this->foodIngrdntForm->isValid()) {
                $formData = $this->foodIngrdntForm->getData();
                if (($foodIngrdntId = $this->foodIngrdntService->insertOrUpdateFoodIngrdnt($formData)) < 0) {
                    $this->layout()->message = Messages::MESSAGE_PERSIST_ERROR;
                } else {
                    if(!empty($post['submit_back'])) {
                        return $this->redirect()->toRoute('fundela_foodadmin_ingrdnt_ingrdnts');
                    }
                    return $this->redirect()->toRoute('fundela_foodadmin_ingrdnt_form', ['food_ingrdnt_id' => $foodIngrdntId]);
                }
            }
        }
        $viewModel->setVariable('foodIngrdntForm', $this->foodIngrdntForm);
        return $viewModel;
    }

    /**
     * @return Response|ViewModel
     */
    public function ingrdntsAction(): Response|ViewModel
    {
        $viewModel = new ViewModel();
        if (!$this->userService->isUserInRole(4)) {
            return $this->redirect()->toRoute('home');
        }
        $request = $this->getRequest();
        $query = $request->getQuery()->toArray();
        $listQueryBase = new ListQueryBase($query);
        if ($request->isPost() && !empty($limit = $request->getPost('limit_post', 0))) {
            $listQueryBase->setLimit($limit);
            $listQueryBase->setOffset(0);
        }
        $ingrdntName = '' . filter_input(INPUT_POST, 'ingrdnt_name', FILTER_SANITIZE_STRING);

        $select = $this->foodIngrdntService->searchFoodIngrdntDbSelect($listQueryBase, $ingrdntName);
        $pgntor = new Paginator(new DbSelect($select, $this->foodIngrdntService->getFoodIngrdntAdapter()));
        $pgntor->setCurrentPageNumber($listQueryBase->getOffset() + 1);
        $pgntor->setItemCountPerPage($listQueryBase->getLimit());
        $ingrdnts = $pgntor->getCurrentItems();
        $viewModel->setVariable('ingrdnts', $ingrdnts);
        $viewModel->setVariable('pgntor', $pgntor);
        $viewModel->setVariable('listQueryBase', $listQueryBase);
        $viewModel->setVariable('limit', $listQueryBase->getLimit());
        $viewModel->setVariable('ingrdntName', $ingrdntName);
        return $viewModel;
    }
}
