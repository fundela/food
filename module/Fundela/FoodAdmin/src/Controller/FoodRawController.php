<?php

namespace Fundela\FoodAdmin\Controller;

use Fundela\Food\Form\Raw\FoodRawForm;
use Fundela\Food\Service\Raw\FoodRawPartService;
use Fundela\Food\Service\Raw\FoodRawService;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Entity\Messages;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Paginator\Adapter\LaminasDb\DbSelect;
use Laminas\Paginator\Paginator;
use Laminas\Validator\Uuid;
use Laminas\View\Model\ViewModel;

class FoodRawController extends AbstractUserController
{
    protected FoodRawForm $foodRawForm;
    protected FoodRawService $foodRawService;
    protected FoodRawPartService $foodRawPartService;

    /**
     * @param FoodRawForm $foodRawForm
     */
    public function setFoodRawForm(FoodRawForm $foodRawForm): void
    {
        $this->foodRawForm = $foodRawForm;
    }

    /**
     * @param FoodRawService $foodRawService
     */
    public function setFoodRawService(FoodRawService $foodRawService): void
    {
        $this->foodRawService = $foodRawService;
    }

    /**
     * @param FoodRawPartService $foodRawPartService
     */
    public function setFoodRawPartService(FoodRawPartService $foodRawPartService): void
    {
        $this->foodRawPartService = $foodRawPartService;
    }

    /**
     * @return ViewModel|Response
     */
    public function formAction(): Response|ViewModel
    {
        $viewModel = new ViewModel();
        if (!$this->userService->isUserInRole(4)) {
            return $this->redirect()->toRoute('home');
        }
        $request = $this->getRequest();
        $foodRawUuid = $this->params('food_raw_uuid');
        $this->foodRawForm->init();
        if ($foodRawUuid) {
            $this->foodRawForm->setPrimaryKeyAvailable(true);
            $this->foodRawForm->init();
            if ($request->isGet()) {
                $foodRaw = $this->foodRawService->getFoodRaw($foodRawUuid);
                $this->foodRawForm->setData($foodRaw);
            }
        } else {
            $this->foodRawForm->init();
            $viewModel->setVariable('isNew', true);
        }
        if ($request->isPost()) {
            $post = $request->getPost()->toArray();
            $this->foodRawForm->setData($post);
            if ($this->foodRawForm->isValid()) {
                $formData = $this->foodRawForm->getData();
                if (empty($foodRawUuid = $this->foodRawService->insertOrUpdateFoodRaw($formData))) {
                    $this->layout()->message = Messages::MESSAGE_PERSIST_ERROR;
                } else {
                    if(!empty($post['submit_back'])) {
                        return $this->redirect()->toRoute('fundela_foodadmin_raw_raws');
                    }
                    return $this->redirect()->toRoute('fundela_foodadmin_raw_form', ['food_raw_uuid' => $foodRawUuid]);
                }
            }
        }
        $viewModel->setVariable('foodRawForm', $this->foodRawForm);
        return $viewModel;
    }

    /**
     * @return Response|ViewModel
     */
    public function rawsAction(): Response|ViewModel
    {
        $viewModel = new ViewModel();
        if (!$this->userService->isUserInRole(4)) {
            return $this->redirect()->toRoute('home');
        }
        $request = $this->getRequest();
        $query = $request->getQuery()->toArray();
        $listQueryBase = new ListQueryBase($query);
        if($request->isPost() && !empty($limit = $request->getPost('limit_post', 0))) {
            $listQueryBase->setLimit($limit);
            $listQueryBase->setOffset(0);
        }
        $rawName = '' . filter_input(INPUT_POST, 'raw_name', FILTER_SANITIZE_STRING);
        $rawCatId = intval(filter_var($this->params()->fromQuery('raw_cat', 0), FILTER_SANITIZE_NUMBER_INT));

        $select = $this->foodRawService->searchFoodRawDbSelect($listQueryBase, $rawName, $rawCatId);
        $pgntor = new Paginator(new DbSelect($select, $this->foodRawService->getFoodRawAdapter()));
        $pgntor->setCurrentPageNumber($listQueryBase->getOffset() + 1);
        $pgntor->setItemCountPerPage($listQueryBase->getLimit());
        $viewModel->setVariable('raws', $pgntor->getCurrentItems());
        $viewModel->setVariable('pgntor', $pgntor);
        $viewModel->setVariable('listQueryBase', $listQueryBase);
        $viewModel->setVariable('limit', $listQueryBase->getLimit());
        $viewModel->setVariable('rawName', $rawName);
        return $viewModel;
    }

    public function rawPartsAction(): Response|ViewModel
    {
        $viewModel = new ViewModel();
        if (!$this->userService->isUserInRole(4)) {
            return $this->redirect()->toRoute('home');
        }
        $rawUuid = $this->params('food_raw_uuid');
        if(!(new Uuid())->isValid($rawUuid)) {
            return $this->redirect()->toRoute('fundela_foodadmin_raw_raws');
        }
        $foodRaw = $this->foodRawService->getFoodRaw($rawUuid);
        if(empty($foodRaw)) {
            return $this->redirect()->toRoute('fundela_foodadmin_raw_raws');
        }
        $request = $this->getRequest();


        $viewModel->setVariable('foodRaw', $foodRaw);
        $parts = $this->foodRawPartService->getFoodRawPartsForFoodRaw($rawUuid);
        $viewModel->setVariable('foodRawParts', $parts);
        return $viewModel;
    }
}
