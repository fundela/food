<?php

namespace Fundela\FoodFront\Controller\Food;

use Fundela\Food\Entity\FoodRecipeEntity;
use Fundela\Food\Entity\FoodRecipePartEntity;
use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\Food\Service\Recipe\FoodRecipeImageService;
use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Food\Service\Recipe\JSONLDRecipeService;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Filter\BlNl2NbspBrFilter;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Paginator\Adapter\LaminasDb\DbSelect;
use Laminas\Paginator\Paginator;
use Laminas\View\Helper\HeadScript;
use Laminas\View\Model\ViewModel;

class FoodRecipeFrontController extends AbstractUserController
{
    protected FoodRecipeService $foodRecipeService;
    protected FoodRecipePartService $foodRecipePartService;
    protected array $recipeIngrdntIdMapping;
    protected FoodIngrdntService $foodIngrdntService;
    protected FoodRecipeImageService $foodRecipeImageService;
    protected ListQueryBase $listQueryBase;
    protected int $itemCountPerPage;
    protected JSONLDRecipeService $jsonldRecipeService;

    public function setFoodRecipeService(FoodRecipeService $foodRecipeService): void
    {
        $this->foodRecipeService = $foodRecipeService;
    }

    public function setFoodRecipePartService(FoodRecipePartService $foodRecipePartService): void
    {
        $this->foodRecipePartService = $foodRecipePartService;
    }

    public function setRecipeIngrdntIdMapping(array $recipeIngrdntIdMapping): void
    {
        $this->recipeIngrdntIdMapping = $recipeIngrdntIdMapping;
    }

    public function setFoodIngrdntService(FoodIngrdntService $foodIngrdntService): void
    {
        $this->foodIngrdntService = $foodIngrdntService;
    }

    public function setFoodRecipeImageService(FoodRecipeImageService $foodRecipeImageService): void
    {
        $this->foodRecipeImageService = $foodRecipeImageService;
    }

    public function setItemCountPerPage(int $itemCountPerPage): void
    {
        $this->itemCountPerPage = $itemCountPerPage;
    }

    public function setJsonldRecipeService(JSONLDRecipeService $jsonldRecipeService): void
    {
        $this->jsonldRecipeService = $jsonldRecipeService;
    }

    /**
     * @return ViewModel
     */
    public function recipesAction(): ViewModel
    {
        $viewModel = new ViewModel();
        $viewModel->setVariable('userUuid', $this->userService->getUserUuid());
        $viewModel->setVariable('categoryIdAssoc', $this->foodRecipeService->getFoodRecipeCategoryIdAssoc());
        $ingrdntsIdAssoc = $this->foodIngrdntService->getFoodIngrdntsIdAssoc();
        $ingrdntIdMapping = [];
        foreach ($this->recipeIngrdntIdMapping as $key => $value) {
            $ingrdntIdMapping[$key] = $ingrdntsIdAssoc[$value];
        }
        $viewModel->setVariable('ingrdntIdMapping', $ingrdntIdMapping);
        $request = $this->getRequest();
        $post = $request->getPost()->toArray();
        $listQueryBase = new ListQueryBase($post);
        $listQueryBase->setAndPrependOrderFieldSql('food_recipe_');
        $recipeLabel = strip_tags(filter_input(INPUT_POST, 'recipe_label', FILTER_UNSAFE_RAW)) ?? '';
        $categoryId = filter_input(INPUT_POST, 'category_id', FILTER_SANITIZE_NUMBER_INT) ?? 0;
        $onlyMine = filter_input(INPUT_POST, 'only_mine', FILTER_SANITIZE_NUMBER_INT) ?? 0;
        $viewModel->setVariable('listQueryBase', $listQueryBase);
        $viewModel->setVariable('recipeLabel', $recipeLabel);
        $viewModel->setVariable('categoryId', $categoryId);
        $viewModel->setVariable('onlyMine', $onlyMine);
        $viewModel->setVariable('recipes', $this->foodRecipeService->searchFoodRecipes($recipeLabel, $categoryId, $listQueryBase, $onlyMine, $this->userService->getUserUuid()));
        // Pagination
        $recipesSelect = $this->foodRecipeService->searchFoodRecipesSelect($recipeLabel, $categoryId, $listQueryBase, $onlyMine, $this->userService->getUserUuid());
        $recipeTableSql = $this->foodRecipeService->getFoodRecipeTableSql();
        $paginator = new Paginator(new DbSelect($recipesSelect, $recipeTableSql));
        $paginator->setCurrentPageNumber($listQueryBase->getPage())
            ->setItemCountPerPage($this->itemCountPerPage);
        $viewModel->setVariable('paginator', $paginator);
        $viewModel->setVariable('recipesCount', $paginator->getCurrentItemCount());
        return $viewModel;
    }

    /**
     * @return ViewModel
     */
    public function recipeAction(): ViewModel
    {
        $viewModel = new ViewModel();
        $recipeEntity = new FoodRecipeEntity();
        if (empty($recipe = $this->foodRecipeService->getFoodRecipeByAlias($this->params('recipe_alias')))
            || !$recipeEntity->exchangeArrayFromDatabase($recipe)) {
            return $viewModel;
        }
        $parts = $this->foodRecipePartService->getFoodRecipeParts($recipe['food_recipe_uuid']);
        $partEntitys = [];
        foreach ($parts as $part) {
            $partEntity = new FoodRecipePartEntity();
            if ($partEntity->exchangeArrayFromDatabase($part)) {
                $partEntitys[] = $partEntity;
            } else {
                $this->foodRecipePartService->getLogger()->err(__CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__ . ' error while exchange array');
            }
        }
        $request = $this->getRequest();
        $uri = $request->getUri();
        $baseUrl = $uri->getScheme() . '://' . $uri->getHost();
        $images = $this->foodRecipeImageService->getFoodRecipeImages($recipe['food_recipe_uuid']);
        $this->jsonldRecipeService->setRecipe($recipeEntity);
        $this->jsonldRecipeService->setParts($partEntitys);
        $this->jsonldRecipeService->setImage($images[0] ?? [], $baseUrl);
        $this->jsonldRecipeService->computeJsonLdData();
        $jsonLd = $this->jsonldRecipeService->getJsonLdString();
        $this->getHeadScriptViewHelper()->appendScript($jsonLd, 'application/ld+json', 'raw');

        $recipe['food_recipe_instrctn'] = (new BlNl2NbspBrFilter())->filter($recipe['food_recipe_instrctn']);
        $viewModel->setVariable('recipe', $recipe);
        $viewModel->setVariable('parts', $parts);
        $viewModel->setVariable('images', $images);
        $viewModel->setVariable('ingrdntsIdAssoc', $this->foodIngrdntService->getFoodIngrdntsIdAssoc());
        $viewModel->setVariable('recipeIngrdntIdMapping', $this->recipeIngrdntIdMapping);
        if ($this->userService->loggedIn()) {
            $viewModel->setVariable('userUuid', $this->userService->getUserUuid());
        }
        return $viewModel;
    }

    protected function getHeadScriptViewHelper(): HeadScript
    {
        return $this->getEvent()->getApplication()->getServiceManager()->get('ViewHelperManager')->get('HeadScript');
    }

    /**
     * @return ViewModel
     */
    public function recipesAllAction(): ViewModel
    {
        $viewModel = new ViewModel();
        if (empty($recipes = $this->foodRecipeService->getFoodRecipesAll())) {
            return $viewModel;
        }
        $viewModel->setVariable('recipes', $recipes);

        $ingrdntsIdAssoc = $this->foodIngrdntService->getFoodIngrdntsIdAssoc();
        $ingrdntIdMapping = [];
        foreach ($this->recipeIngrdntIdMapping as $key => $value) {
            $ingrdntIdMapping[$key] = $ingrdntsIdAssoc[$value];
        }
        $viewModel->setVariable('ingrdntIdMapping', $ingrdntIdMapping);
        return $viewModel;
    }
}
