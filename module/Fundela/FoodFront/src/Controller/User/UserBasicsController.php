<?php

namespace Fundela\FoodFront\Controller\User;

use Fundela\Trinket\Entity\Messages;
use Fundela\User\Controller\AbstractUserController;
use Fundela\User\Entity\UserEntity;
use Fundela\User\Form\UserForm;
use Fundela\User\Service\UserService;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\I18n\Filter\Alnum;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class UserBasicsController extends AbstractUserController
{

    protected UserForm $userForm;

    /**
     * @param UserForm $userForm
     */
    public function setUserForm(UserForm $userForm): void
    {
        $this->userForm = $userForm;
    }

    /**
     * @return \Laminas\Http\Response|ViewModel
     */
    public function loginAction()
    {
        if ($this->userService->loggedIn()) {
            return $this->redirect()->toRoute('home');
        }
        $viewModel = new ViewModel();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $username = (new Alnum())->filter($request->getPost('username'));
            $viewModel->setVariable('username', $username);
            $passwd = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
            if (!empty($username) && !empty($passwd)) {
                if ($this->userService->loginWithUsernamePasswd($username, $passwd)) {
                    $cookie = $request->getCookie()->getArrayCopy();
                    if (!empty($cookie['beforeloginhref'])) {
                        return $this->redirect()->toUrl($cookie['beforeloginhref']);
                    }
                    return $this->redirect()->toRoute('home');
                }
            }

        }
        return $viewModel;
    }

    /**
     * @return \Laminas\Http\Response
     */
    public function logoutAction()
    {
        if ($this->userService->loggedIn()) {
            $this->userService->logout();
        }
        return $this->redirect()->toRoute('home');
    }

    public function registerAction()
    {
        if ($this->userService->loggedIn()) {
            return $this->redirect()->toRoute('home');
        }
        $viewModel = new ViewModel();
        $this->userForm->init();
        $viewModel->setVariable('userForm', $this->userForm);
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $this->userForm->setData($postData);
            if (!$this->userForm->isValid()) {
                return $viewModel;
            }
            $d = $this->userForm->getData();
            if (empty($this->userService->registerUser($d['user_login'], $d['user_passwd'], $d['user_email'], $d['user_lang_iso']))) {
                return $viewModel;
            }
            if ($this->userService->sendVerifyEmail($d['user_email'], $d['user_login'])) {
                $this->layout()->message = UserEntity::MESSAGE_SEND_VERIFYEMAIL;
            }
        }
        return $viewModel;
    }

    public function registerVerifyAction()
    {
        $viewModel = new ViewModel();
        $hash = $this->params()->fromQuery('hash');
        if ($this->userService->verifyRegistration($hash)) {
            $this->layout()->message = UserEntity::MESSAGE_REGISTER_VERIFY_SUCCESS;
        } else {
            $this->layout()->message = Messages::MESSAGE_PERSIST_ERROR;
        }
        return $viewModel;
    }
}
