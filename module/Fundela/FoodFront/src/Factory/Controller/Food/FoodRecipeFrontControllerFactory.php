<?php

namespace Fundela\FoodFront\Factory\Controller\Food;

use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\Food\Service\Recipe\FoodRecipeImageService;
use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Food\Service\Recipe\JSONLDRecipeService;
use Fundela\User\Service\UserService;
use Fundela\FoodFront\Controller\Food\FoodRecipeFrontController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRecipeFrontControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodRecipeFrontController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodRecipeService($container->get(FoodRecipeService::class));
        $controller->setFoodRecipePartService($container->get(FoodRecipePartService::class));
        $controller->setRecipeIngrdntIdMapping($container->get('config')['fundela_food']['recipe_ingrdnt_id_mapping']);
        $controller->setFoodIngrdntService($container->get(FoodIngrdntService::class));
        $controller->setFoodRecipeImageService($container->get(FoodRecipeImageService::class));
        $controller->setItemCountPerPage($container->get('config')['fundela_food']['item_count_per_page']);
        $controller->setJsonldRecipeService($container->get(JSONLDRecipeService::class));
        return $controller;
    }
}
