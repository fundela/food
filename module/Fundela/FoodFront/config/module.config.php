<?php

namespace Fundela\FoodFront;

use Fundela\FoodFront\Controller\Food\FoodRecipeFrontController;
use Fundela\FoodFront\Controller\User\UserBasicsController;
use Fundela\FoodFront\Factory\Controller\Food\FoodRecipeFrontControllerFactory;
use Fundela\FoodFront\Factory\Controller\User\UserBasicsControllerFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            /*
             * ####### food
             */
            'fundela_foodfront_food_foodrecipefront_recipes' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/food-recipes',
                    'defaults' => [
                        'controller' => FoodRecipeFrontController::class,
                        'action' => 'recipes',
                    ],
                ],
            ],
            'fundela_foodfront_food_foodrecipefront_recipe' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/food-recipe/:recipe_alias',
                    'constrains' => [
                        'recipe_alias' => '[0-9a-z-]+'
                    ],
                    'defaults' => [
                        'controller' => FoodRecipeFrontController::class,
                        'action' => 'recipe',
                    ],
                ],
            ],
            'fundela_foodfront_food_foodrecipefront_recipes_all' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/food-recipes-all',
                    'defaults' => [
                        'controller' => FoodRecipeFrontController::class,
                        'action' => 'recipesAll',
                    ],
                ],
            ],
            /*
             * ####### user
             */
            'fundela_foodfront_user_userbasics_login' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/login',
                    'defaults' => [
                        'controller' => UserBasicsController::class,
                        'action' => 'login',
                    ],
                ],
            ],
            'fundela_foodfront_user_userbasics_logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/logout',
                    'defaults' => [
                        'controller' => UserBasicsController::class,
                        'action' => 'logout',
                    ],
                ],
            ],
            'fundela_foodfront_user_userbasics_register' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/register',
                    'defaults' => [
                        'controller' => UserBasicsController::class,
                        'action' => 'register',
                    ],
                ],
            ],
            'fundela_foodfront_user_userbasics_registerverify' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/register-verify',
                    'defaults' => [
                        'controller' => UserBasicsController::class,
                        'action' => 'registerVerify',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            UserBasicsController::class => UserBasicsControllerFactory::class,
            FoodRecipeFrontController::class => FoodRecipeFrontControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
