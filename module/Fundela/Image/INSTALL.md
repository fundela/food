Look at [composer.json](composer.json)

Copy `./data/watermark.png` (or another PNG) into folder:
`['config']['fundela_image']['watermark_path_absolute']`

Folder `/public/img/fundela` must exist and be writeable by users.
