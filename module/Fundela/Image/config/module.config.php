<?php

namespace Fundela\Image;

use Fundela\Image\Factory\Form\ImageFormFactory;
use Fundela\Image\Factory\Service\ImageServiceFactory;
use Fundela\Image\Factory\Table\ImageTableFactory;
use Fundela\Image\Factory\View\ImageSrcFactory;
use Fundela\Image\Form\ImageForm;
use Fundela\Image\Service\ImageService;
use Fundela\Image\Table\ImageTable;
use Fundela\Image\View\ImageSrc;

return [
    'router' => [
        'routes' => [],
    ],
    'controllers' => [
        'factories' => [],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            // form
            ImageForm::class => ImageFormFactory::class,
            // table
            ImageTable::class => ImageTableFactory::class,
            // service
            ImageService::class => ImageServiceFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers' => [
        'factories' => [
            ImageSrc::class => ImageSrcFactory::class,
        ],
        'invokables' => [],
        'aliases' => [
            'imageSrc' => ImageSrc::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'fundela_image' => [
        'image_path_absolute' => __DIR__ . '/../../../../public/img/fundela',
        'image_path_relative' => '/img/fundela',
        'watermark_path_absolute' => __DIR__ . '/../../../../data/files/watermark.png' // *.png image for watermark
    ]
];
