<?php

namespace Fundela\Image\View;

use Fundela\Image\Entity\ImageEntity;
use Fundela\Image\Service\ImageService;
use Fundela\Image\Tool\ImageScale;
use Laminas\View\Helper\AbstractHelper;

class ImageSrc extends AbstractHelper
{
    protected ImageService $imageService;

    public function setImageService(ImageService $imageService): void
    {
        $this->imageService = $imageService;
    }

    /**
     * @param array $image Database data/row for one image.
     * @param array $imageScale E.g. [magic2square => 128]
     * @return string
     */
    public function __invoke(array $image, array $imageScale): string
    {
        if (
            empty($image) || empty($image['image_scaling'])
            || empty($imageScale) || !is_array($imageScale)
            || empty($imageScalings = json_decode($image['image_scaling'], true))
            || !isset($imageScalings[array_key_first($imageScale)])
        ) {
            return '';
        }
        $type = array_key_first($imageScale);
        if(!in_array($imageScale[$type], $imageScalings[$type])) {
            return '';
        }
        $filenameCompl = $image['image_filename'];
        switch($type) {
            case ImageScale::KEY_MAGIC_TWO_SQUARE:
                $filenameCompl .= '_' . ImageScale::KEY_MAGIC_TWO_SQUARE . '_' . $imageScale[$type] . '.' . $image['image_ext'];
                break;
            default:
                $filenameCompl .=  '.' . $image['image_ext'];
        }
        return $this->imageService->getImageRelativePath($image['image_time_create'], $image['image_id'], $filenameCompl);
    }
}
