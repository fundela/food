<?php

namespace Fundela\Image\Factory\Service;

use Fundela\Image\Service\ImageService;
use Fundela\Image\Table\ImageTable;
use Fundela\Trinket\Service\Filesystem\FolderToolService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ImageServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new ImageService();
        $service->setLogger($container->get('logger'));
        $service->setImageConfig($container->get('config')['fundela_image']);
        $service->setImageTable($container->get(ImageTable::class));
        $service->setFolderToolService($container->get(FolderToolService::class));
        return $service;
    }
}
