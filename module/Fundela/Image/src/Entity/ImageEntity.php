<?php

namespace Fundela\Image\Entity;

use Fundela\Trinket\Entity\AbstractEntity;

class ImageEntity extends AbstractEntity
{
	public array $mapping = [
		'image_id' => 'image_id',
		'image_label' => 'image_label',
		'image_desc' => 'image_desc',
		'image_filename' => 'image_filename',
		'image_ext' => 'image_ext',
		'image_scaling' => 'image_scaling',
		'image_time_create' => 'image_time_create',
	];

	public function getImageId(): int
	{
		if(!isset($this->storage['image_id'])) {
			return 0;
		}
		return $this->storage['image_id'];
	}

	public function getImageLabel(): string
	{
		if(!isset($this->storage['image_label'])) {
			return '';
		}
		return $this->storage['image_label'];
	}

	public function getImageDesc(): string
	{
		if(!isset($this->storage['image_desc'])) {
			return '';
		}
		return $this->storage['image_desc'];
	}

	public function getImageFilename(): string
	{
		if(!isset($this->storage['image_filename'])) {
			return '';
		}
		return $this->storage['image_filename'];
	}

	public function getImageExt(): string
	{
		if(!isset($this->storage['image_ext'])) {
			return '';
		}
		return $this->storage['image_ext'];
	}

    /**
     * @return string JSON
     */
	public function getImageScaling(): string
	{
		if(!isset($this->storage['image_scaling'])) {
			return '';
		}
		return $this->storage['image_scaling'];
	}

    public function getImageScalingArray(): array
    {
        if(!isset($this->storage['image_scaling'])) {
            return [];
        }
        return json_decode($this->storage['image_scaling'], true);
    }

	public function getImageTimeCreate(): string
	{
		if(!isset($this->storage['image_time_create'])) {
			return '';
		}
		return $this->storage['image_time_create'];
	}
}
