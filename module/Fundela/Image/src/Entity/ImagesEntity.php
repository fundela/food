<?php

namespace Fundela\Image\Entity;

use Fundela\Trinket\Entity\AbstractEntity;

class ImagesEntity extends AbstractEntity
{
	public array $mapping = [
		'image_id' => 'image_id',
		'image_label' => 'image_label',
		'image_desc' => 'image_desc',
		'image_filename' => 'image_filename',
		'image_ext' => 'image_ext',
		'image_scaling' => 'image_scaling',
		'image_time_create' => 'image_time_create',
	];

    protected array $storageItems = [];

    public function exchangeArrayImages(array $images)
    {
        foreach ($images as $image) {
            if (!is_array($image) || empty($image)) {
                continue;
            }
            $this->storage = [];
            foreach ($this->mapping as $key => $value) {
                if (isset($image[$key])) {
                    if ($this->isMappingFlipped) {
                        $this->storage[$value] = $image[$key];
                    } else {
                        $this->storage[$key] = $image[$key];
                    }
                }
            }
            $this->storageItems[$image['image_id']] = $this->storage;
        }
        if (empty($this->storageItems)) {
            return false;
        }
        $this->storage = [];
        return true;
    }

	public function getImageLabel(int $imageId): string
	{
		if(!isset($this->storageItems[$imageId]['image_label'])) {
			return '';
		}
		return $this->storageItems[$imageId]['image_label'];
	}

	public function getImageDesc(int $imageId): string
	{
		if(!isset($this->storageItems[$imageId]['image_desc'])) {
			return '';
		}
		return $this->storageItems[$imageId]['image_desc'];
	}

	public function getImageFilename(int $imageId): string
	{
		if(!isset($this->storageItems[$imageId]['image_filename'])) {
			return '';
		}
		return $this->storageItems[$imageId]['image_filename'];
	}

	public function getImageExt(int $imageId): string
	{
		if(!isset($this->storageItems[$imageId]['image_ext'])) {
			return '';
		}
		return $this->storageItems[$imageId]['image_ext'];
	}

    /**
     * @param int $imageId
     * @return string JSON
     */
	public function getImageScaling(int $imageId): string
	{
		if(!isset($this->storageItems[$imageId]['image_scaling'])) {
			return '';
		}
		return $this->storageItems[$imageId]['image_scaling'];
	}

	public function getImageTimeCreate(int $imageId): string
	{
		if(!isset($this->storageItems[$imageId]['image_time_create'])) {
			return '';
		}
		return $this->storageItems[$imageId]['image_time_create'];
	}
}
