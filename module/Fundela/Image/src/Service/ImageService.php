<?php

namespace Fundela\Image\Service;

use Fundela\Image\Table\ImageTable;
use Fundela\Image\Tool\ImageScale;
use Fundela\Image\Tool\ImageWatermark;
use Fundela\Trinket\Service\AbstractService;
use Fundela\Trinket\Service\Filesystem\FolderToolService;
use Fundela\Trinket\Tool\StringTool;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;

class ImageService extends AbstractService
{
    const FUNDELA_IMAGE_BRAND = '';
    protected array $imageConfig;
    protected ImageTable $imageTable;
    protected FolderToolService $folderToolService;

    public function setImageConfig(array $imageConfig): void
    {
        $this->imageConfig = $imageConfig;
    }

    public function setImageTable(ImageTable $imageTable): void
    {
        $this->imageTable = $imageTable;
    }

    public function setFolderToolService(FolderToolService $folderToolService): void
    {
        $this->folderToolService = $folderToolService;
    }

    protected function computeImageFolder(string $rootFolderAbsolute, int $year, string $month, int $imageId): string
    {
        return $this->folderToolService->computeStringDateStringFolder($rootFolderAbsolute, '', $year, $month, $imageId . '');
    }

    protected function getImageFolder(string $rootFolderAbsolute, int $year, string $month, int $imageId): string
    {
        return $this->folderToolService->getStringDateStringFolder($rootFolderAbsolute, '', $year, $month, $imageId . '');
    }

    /**
     * @param array $storage Data from \Fundela\Image\Form\ImageForm
     * @return int -1 on error
     */
    public function insertAndUploadImageWithStandardScalings(array $storage): int
    {
        if($storage['image_image']['error'] != UPLOAD_ERR_OK
        || empty($dotPos = strrpos($storage['image_image']['name'], '.'))) {
            $this->message = 'File extension is missing.';
            return -1;
        }
        $extension = substr($storage['image_image']['name'], ++$dotPos);
        $currentTime = time();
        $success = true;
        $adapter = $this->imageTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        $newImageId = $this->imageTable->saveImage([
            'image_label' => $storage['image_label'],
            'image_desc' => $storage['image_desc'],
            'image_filename' => '', // can not be NULL
            'image_ext' => '', // can not be NULL
        ]);
        $folderFqfn = $this->imageConfig['image_path_absolute'] . DIRECTORY_SEPARATOR
            . $this->computeImageFolder($this->imageConfig['image_path_absolute'], date('Y', $currentTime), date('m', $currentTime), $newImageId);
        if ($newImageId > 0 && file_exists($folderFqfn)) {
            $filename = StringTool::computeAlias($storage['image_label']) . '_' . $currentTime;
            $filenameCompl = $filename . '.' . $extension;
            if (!ImageWatermark::doWatermark($storage['image_image']['tmp_name'], $this->imageConfig['watermark_path_absolute'])) {
                $success = false;
            }
            if ($success && move_uploaded_file($storage['image_image']['tmp_name'], $folderFqfn . '/' . $filenameCompl)) {
                $imageScaling = [];
                $imageScaling['original'] = 1;
                $imageScaling[ImageScale::KEY_MAGIC_TWO_SQUARE] = ImageScale::makeMagicTwoSquare($folderFqfn, $filename, $extension);
                if ($this->imageTable->updateImage($newImageId, [
                        'image_filename' => $filename,
                        'image_ext' => $extension,
                        'image_scaling' => json_encode($imageScaling)
                    ]) < 0) {
                    $success = false;
                }
            } else {
                $success = false;
            }
        } else {
            $success = false;
        }
        if (!$success) {
            $this->message = 'Es gab einen Fehler beim Speichern des Bildes';
            $this->folderToolService->deleteDirRecursive($folderFqfn);
            $connection->rollback();
            return -1;
        }
        $connection->commit();
        return $newImageId;
    }

    public function deleteImageComplete(int $imageId): bool
    {
        $adapter = $this->imageTable->getAdapter();
        /** @var AbstractConnection $connection */
        $connection = $adapter->getDriver()->getConnection();
        $connection->beginTransaction();
        $image = $this->imageTable->getImage($imageId);
        if ($this->imageTable->deleteImage($imageId) < 1) {
            $connection->rollback();
            return false;
        }
        $folder = $this->getImageFolder($this->imageConfig['image_path_absolute']
            , date('Y', $image['image_time_create_unix'])
            , date('m', $image['image_time_create_unix']), $imageId);
        if (!$this->folderToolService->deleteDirRecursive($folder)) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function getImagesRelativePath(): string
    {
        if (!isset($this->imageConfig['image_path_relative'])) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . 'Images relative path is not set in config.');
        }
        return $this->imageConfig['image_path_relative'];
    }

    public function getImageRelativePath(string $timestamp, int $imageId, string $filenameCompl): string
    {
        return $this->getImagesRelativePath() . '/' . substr($timestamp, 0, 4) . '/' . substr($timestamp, 5, 2) . '/' . $imageId . '/' . $filenameCompl;
    }

    public function getImage(int $imageId): array
    {
        return $this->imageTable->getImage($imageId);
    }

    public function getImages(): array
    {
        return $this->imageTable->getImages();
    }
}
