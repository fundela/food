<?php

namespace Fundela\Image\Form;

use Fundela\Trinket\Filter\FilterChainStringSanitize;
use Fundela\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\File\FilesSize;
use Laminas\Validator\File\ImageSize;
use Laminas\Validator\File\MimeType;
use Laminas\Validator\StringLength;

class ImageForm extends AbstractForm implements InputFilterProviderInterface
{
    const MIMETYPES = 'image/jpeg,image/webp,image/png';

    public function init()
    {
        $this->add(['name' => 'image_image']);
        $this->add(['name' => 'image_label']);
        $this->add(['name' => 'image_desc']);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['image_image'] = [
            'required' => true,
            'validators' => [
                [
                    'name' => 'filemimetype',
                    'break_chain_on_failure' => true,
                    'options' => [
                        'mimeType' => self::MIMETYPES,
                        'magicFile' => false, // wegen: exit signal Segmentation fault (11), possible coredump in /etc/apache2
                    ]
                ],
                [
                    'name' => FilesSize::class,
                    'options' => [
                        'max' => '10MB', // maximum of 10MB
                    ],
                ],
                [
                    'name' => ImageSize::class,
                    'options' => [
                        // https://de.wikipedia.org/wiki/Bildauflösungen_in_der_Digitalfotografie
                        'minWidth' => 200,
                        'minHeight' => 200,
                        'maxWidth' => 4048,
                        'maxHeight' => 4048,
                    ],
                ],
            ],
        ];

        $filter['image_label'] = [
            'required' => true,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['image_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => FilterChainStringSanitize::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
