<?php

namespace Fundela\Image\Tool;

class ImageScale
{
    const KEY_MAGIC_TWO_SQUARE = 'm2s';

    private static array $magicTwoSquares = [
        16 => [16, 16],
        32 => [32, 32],
        64 => [64, 64],
        128 => [128, 128],
        256 => [256, 256],
        512 => [512, 512],
        1024 => [1024, 1024]
    ];

    /**
     *
     * @param string $fqfn Full qualified folder name ...without a ending slash.
     * @param string $imageName Image name without file-extension (only the nude name).
     * @param string $imageFormat
     * @param array $magicTwoSquares Keys of `self::$magicTwoSquares`
     * @return array
     */
    public static function makeMagicTwoSquare(string $fqfn, string $imageName, string $imageFormat = 'jpg', array $magicTwoSquares = []): array
    {
        if (empty($fqfn) || empty($imageName)) {
            throw new \RuntimeException('call ' . __CLASS__ . '->' . __FUNCTION__ . '() without imagepath or imagename.');
        }
        if (empty($magicTwoSquares)) {
            $magicTwoSquares = array_keys(self::$magicTwoSquares);
        }
        $biggestMagicTwoSquare = 0;
        foreach ($magicTwoSquares as $magicTwoSquare) {
            if (!array_key_exists($magicTwoSquare, self::$magicTwoSquares)) {
                throw new \RuntimeException('call ' . __CLASS__ . '->' . __FUNCTION__ . '() invalid $magicTwoSquares.');
            }
            if ($biggestMagicTwoSquare < $magicTwoSquare) {
                $biggestMagicTwoSquare = $magicTwoSquare;
            }
        }

        $images = [];
        try {
            $imagick = new \Imagick($fqfn . DIRECTORY_SEPARATOR . $imageName . '.' . $imageFormat);
            $geo = $imagick->getimagegeometry();
            if (($geo['width'] / $biggestMagicTwoSquare) < ($geo['height'] / $biggestMagicTwoSquare)) {
                $imagick->cropImage($geo['width'], floor($biggestMagicTwoSquare * $geo['width'] / $biggestMagicTwoSquare), 0,
                    (($geo['height'] - ($biggestMagicTwoSquare * $geo['width'] / $biggestMagicTwoSquare)) / 2));
            } else {
                $imagick->cropImage(ceil($biggestMagicTwoSquare * $geo['height'] / $biggestMagicTwoSquare), $geo['height'],
                    (($geo['width'] - ($biggestMagicTwoSquare * $geo['height'] / $biggestMagicTwoSquare)) / 2), 0);
            }
            rsort($magicTwoSquares); // biggest first
            foreach ($magicTwoSquares as $magicTwoSquare) {
                $imagick->scaleimage($magicTwoSquare, $magicTwoSquare, true);
                $imagick->setimageformat($imageFormat);
                if (
                    file_put_contents($fqfn . DIRECTORY_SEPARATOR . $imageName
                    . '_' . self::KEY_MAGIC_TWO_SQUARE . '_' . $magicTwoSquare . '.' . $imageFormat
                    , $imagick->getimageblob())
                ) {
                    $images[] = $magicTwoSquare;
                }
            }
            $imagick->clear();
            $imagick->destroy();
        } catch (\ImagickException $exception) {
            return [];
        }
        return $images;
    }

}
