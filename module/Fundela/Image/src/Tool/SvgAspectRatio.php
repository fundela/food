<?php

namespace Fundela\Image\Tool;

class SvgAspectRatio
{

    protected \SimpleXMLElement $xml;

    protected float $width;
    protected float $height;

    /**
     * Aspect ratio width:height.
     *
     * @var array [width,height]
     */
    protected array $aspectRatio = [1, 1];

    public function getWidth(): float
    {
        return $this->width;
    }

    public function getHeight(): float
    {
        return $this->height;
    }

    public function loadSvg(string $svgPath): bool
    {
        $this->xml = simplexml_load_file(realpath($svgPath));
        if ($this->xml === false) {
            return false;
        }
        $atts = $this->xml->attributes();
        $this->width = floatval($atts->width);
        $this->height = floatval($atts->height);
        if (empty($this->width) || empty($this->height)) {
            return false;
        }
        $this->aspectRatio = [1, ($this->height / $this->width)];
        return true;
    }
}
