<?php

namespace Fundela\Image\Tool;

class ImageWatermark
{

    /**
     * @param string $fqfnImage Path to image.
     * @param string $fqfnWatermark Path to watermark image.
     * @return bool
     */
    public static function doWatermark(string $fqfnImage, string $fqfnWatermark): bool
    {
        if(!file_exists($fqfnWatermark = realpath($fqfnWatermark)) || !is_file($fqfnWatermark)) {
            return false;
        }
        try {
            $imageW = new \Imagick();
            $imageW->readImageBlob(file_get_contents($fqfnWatermark));
            $image = new \Imagick($fqfnImage);
        } catch (\ImagickException $exception) {
            return false;
        }
        $imageW->setImageFormat('png24');

        $iGeo = $image->getimagegeometry();
        if (!$imageW->resizeImage($iGeo['width'], $iGeo['height'], \Imagick::FILTER_QUADRATIC, 0.9, true)) {
            return false;
        }
        $wGeo = $imageW->getimagegeometry();
        $image->setimagegravity(\Imagick::GRAVITY_CENTER);
        $image->compositeimage($imageW, $imageW->getimagecompose(), $iGeo['width'] / 2 - $wGeo['width'] / 2, $iGeo['height'] / 2 - $wGeo['height'] / 2);
        $success = $image->writeimage($fqfnImage);
        $image->clear();
        $image->destroy();
        return $success;
    }
}
