<?php

namespace Fundela\Image\Table;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ImageTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'image';

    /**
     * @param int $id
     * @return array
     */
    public function getImage(int $id): array
    {
        $select = $this->sql->select();
        try {
            $select->columns([
                Select::SQL_STAR,
                'image_time_create_unix' => new Expression('extract(EPOCH from date_trunc(\'second\', image_time_create)::timestamp(0))'),
            ]);
            $select->where(['image_id' => $id]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function getImages(): array
    {
        $select = $this->sql->select();
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $return = $result->toArray();
                return $return;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function saveImage(array $storage)
    {
        $insert = $this->sql->insert();
        try {
            $insert->values($storage);
            $this->insertWith($insert);
            $newId = $this->adapter->getDriver()->getConnection()->getLastGeneratedValue('public.image_image_id_seq');
            if (empty($newId)) {
                return -1;
            }
            return $newId;
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateImage(int $imageId, array $storage): int
    {
        if (empty($imageId)) {
            return -1;
        }
        unset($storage['image_id']);
        $update = $this->sql->update();
        try {
            $update->set($storage);
            $update->where(['image_id' => $imageId]);
            return $this->updateWith($update);
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteImage(int $imageId): int
    {
        return $this->delete(['image_id' => $imageId]);
    }
}
