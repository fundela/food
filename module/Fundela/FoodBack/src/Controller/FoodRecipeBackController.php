<?php

namespace Fundela\FoodBack\Controller;

use Fundela\Food\Form\Recipe\FoodRecipeForm;
use Fundela\Food\Form\Recipe\FoodRecipePartForm;
use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Trinket\Entity\Messages;
use Fundela\Trinket\Filter\BlNl2NbspBrFilter;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Validator\Uuid;
use Laminas\View\Model\ViewModel;

class FoodRecipeBackController extends AbstractUserController
{
    protected FoodRecipeForm $foodRecipeForm;
    protected FoodRecipeService $foodRecipeService;
    protected FoodRecipePartForm $foodRecipePartForm;
    protected FoodRecipePartService $foodRecipePartService;

    public function setFoodRecipeForm(FoodRecipeForm $foodRecipeForm): void
    {
        $this->foodRecipeForm = $foodRecipeForm;
    }

    public function setFoodRecipeService(FoodRecipeService $foodRecipeService): void
    {
        $this->foodRecipeService = $foodRecipeService;
    }

    public function setFoodRecipePartForm(FoodRecipePartForm $foodRecipePartForm): void
    {
        $this->foodRecipePartForm = $foodRecipePartForm;
    }

    public function setFoodRecipePartService(FoodRecipePartService $foodRecipePartService): void
    {
        $this->foodRecipePartService = $foodRecipePartService;
    }

    /**
     * @return \Laminas\Http\Response|ViewModel
     */
    public function formAction(): \Laminas\Http\Response|ViewModel
    {
        $viewModel = new ViewModel();
        if (!$this->userService->isUserInRole(5)) {
            return $this->redirect()->toRoute('home');
        }
        $request = $this->getRequest();
        if (!$request instanceof Request) {
            return $viewModel;
        }
        $recipeUuid = $this->params('food_recipe_uuid', '');
        if ($request->isGet() && isset($recipeUuid) && (new Uuid())->isValid($recipeUuid)) {
            $viewModel->setTemplate('fundela/food-back/food-recipe-back/form-edit');
            $foodRecipe = $this->foodRecipeService->getFoodRecipe($recipeUuid);
            if (!$this->foodRecipeService->checkEditAllowed($recipeUuid, $this->userService->getUserUuid())) {
                return $this->redirect()->toRoute('fundela_foodfront_food_foodrecipefront_recipe', ['recipe_alias' => $foodRecipe['food_recipe_alias']]);
            }
            $viewModel->setVariable('instrctn', (new BlNl2NbspBrFilter())->filter($foodRecipe['food_recipe_instrctn']));
            $viewModel->setVariable('recipeAlias', $foodRecipe['food_recipe_alias']);
            $this->foodRecipeForm->setPrimaryKeyAvailable(true);
            $this->foodRecipeForm->init();
            $this->foodRecipeForm->setData($foodRecipe);
        } else if ($request->isGet() && empty($recipeUuid)) {
            $viewModel->setTemplate('fundela/food-back/food-recipe-back/form-new');
            $viewModel->setVariable('isNew', true);
            $this->foodRecipeForm->init();
        }
        if ($request->isPost() && $this->foodRecipeService->checkEditAllowed($recipeUuid, $this->userService->getUserUuid())) {
            $post = $request->getPost()->toArray();
            $isNew = true;
            $isOwner = false;
            if (!empty($post['food_recipe_uuid'])) {
                if ($this->foodRecipeService->checkFoodRecipeUserUuid(
                    $post['food_recipe_uuid']
                    , $this->userService->getUserUuid()
                )) {
                    $isOwner = true;
                }
                $isNew = false;
                $this->foodRecipeForm->setPrimaryKeyAvailable(true);
            }
            $this->foodRecipeForm->init();
            $this->foodRecipeForm->setData($post);
            if ($this->foodRecipeForm->isValid()) {
                $formData = $this->foodRecipeForm->getData();
                $success = false;
                if ($isNew) {
                    $formData['food_recipe_alias'] = $this->foodRecipeService->computeFoodRecipeAliasUnique($formData['food_recipe_label']);
                    if (!empty($recipeUuid = $this->foodRecipeService->insertFoodRecipe($formData, $this->userService->getUserUuid()))) {
                        $success = true;
                    }
                }
                if (!$isNew && $isOwner) {
                    if (!empty($recipeUuid = $this->foodRecipeService->updateFoodRecipe($formData))) {
                        $success = true;
                    }
                }
                if ($success) {
                    $ingrdnts = $this->foodRecipeService->computeAndSaveFoodRecipeIngrdnts($recipeUuid);
                    if (!empty($post['submit_back'])) {
                        return $this->redirect()->toRoute('fundela_foodfront_food_foodrecipefront_recipes');
                    }
                    return $this->redirect()->toRoute('fundela_foodback_foodrecipe_form', ['food_recipe_uuid' => $recipeUuid]);
                } else {
                    return $this->redirect()->toRoute('home');
                }
            } else {
                $viewModel->setVariable('invalidForm', true);
            }
        }

        $viewModel->setVariable('recipeUuid', $recipeUuid);
        $viewModel->setVariable('foodRecipeForm', $this->foodRecipeForm);
        $this->foodRecipePartForm->setWithSubmit(false);
        $this->foodRecipePartForm->init();
        $viewModel->setVariable('foodRecipePartForm', $this->foodRecipePartForm);
        $viewModel->setVariable('loaded', 1);
        return $viewModel;
    }
}
