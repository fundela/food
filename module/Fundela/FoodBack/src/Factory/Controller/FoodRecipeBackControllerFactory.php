<?php

namespace Fundela\FoodBack\Factory\Controller;

use Fundela\Food\Form\Recipe\FoodRecipeForm;
use Fundela\Food\Form\Recipe\FoodRecipePartForm;
use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Image\Form\ImageForm;
use Fundela\User\Service\UserService;
use Fundela\FoodBack\Controller\FoodRecipeBackController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRecipeBackControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodRecipeBackController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodRecipeForm($container->get(FoodRecipeForm::class));
        $controller->setFoodRecipeService($container->get(FoodRecipeService::class));
        $controller->setFoodRecipePartForm($container->get(FoodRecipePartForm::class));
        $controller->setFoodRecipePartService($container->get(FoodRecipePartService::class));
        return $controller;
    }
}
