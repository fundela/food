<?php

namespace Fundela\FoodBack;

use Fundela\FoodBack\Controller\FoodRecipeBackController;
use Fundela\FoodBack\Factory\Controller\FoodRecipeBackControllerFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router'          => [
        'routes' => [
            'fundela_foodback_foodrecipe_form' => [
                'type'    => Segment::class,
                'options' => [
                    'route'       => '/recipe-form[/:food_recipe_uuid][/]',
                    'constraints' => [
                        'food_recipe_uuid' => '[0-9a-zA-Z-]*',
                    ],
                    'defaults'    => [
                        'controller' => FoodRecipeBackController::class,
                        'action'     => 'form',
                    ],
                ],
            ],
        ],
    ],
    'controllers'     => [
        'factories'  => [
            FoodRecipeBackController::class => FoodRecipeBackControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories'  => [],
        'invokables' => [],
    ],
    'view_helpers'    => [
        'factories'  => [],
        'invokables' => [],
        'aliases'    => [],
    ],
    'view_manager'    => [
        'template_map'        => [
            'fundela/food-back/food-recipe-back/form-new'  => __DIR__ . '/../view/fundela/food-back/food-recipe-back/form.phtml',
            'fundela/food-back/food-recipe-back/form-edit' => __DIR__ . '/../view/fundela/food-back/food-recipe-back/form.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
];
