<?php

namespace Fundela\Trinket\Filter;

use Laminas\Filter\FilterInterface;

class Nl2BlFilter implements FilterInterface
{
    public function filter($value)
    {
        return str_ireplace(["\r\n", "\n"], ' ', $value);
    }
}
