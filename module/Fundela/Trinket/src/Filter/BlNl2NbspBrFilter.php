<?php

namespace Fundela\Trinket\Filter;

use Laminas\Filter\FilterInterface;

class BlNl2NbspBrFilter implements FilterInterface
{
    public function filter($value)
    {
        $bl2nbsp = (new Bl2NbspFilter())->filter($value);
        return nl2br($bl2nbsp);
    }
}
