<?php

namespace Fundela\Trinket\Filter;

use Laminas\Filter\FilterInterface;

class Bl2NbspFilter implements FilterInterface
{
    public function filter($value)
    {
        return str_ireplace(' ', '&nbsp;', $value);
    }
}
