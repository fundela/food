<?php

namespace Fundela\Trinket\Filter;

use Laminas\Filter\Exception;
use Laminas\Filter\FilterInterface;

class SanitizeStringFilter implements FilterInterface
{

    public function filter($value)
    {
        return htmlspecialchars($value, ENT_QUOTES);
    }
}
