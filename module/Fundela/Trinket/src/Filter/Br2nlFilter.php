<?php

namespace Fundela\Trinket\Filter;

use Laminas\Filter\FilterInterface;

class Br2nlFilter implements FilterInterface
{
    public function filter($value)
    {
        return str_ireplace(['<br />', '<br>', '<br/>'], "\r\n", $value);
    }
}
