<?php

namespace Fundela\Trinket\Filter;

use Laminas\Filter\FilterInterface;

class SnakeCaseFilter implements FilterInterface
{
    public function filter($value)
    {
        $matches = [];
        $r = preg_match_all('/[a-z0-9_]/', $value, $matches);
        if (!$r || empty($matches[0]) || !is_array($matches[0])) {
            return '';
        }
        return implode('', $matches[0]);
    }
}
