<?php

namespace Fundela\Trinket\Factory\Service\Filesystem;

use Fundela\Trinket\Service\Filesystem\FolderToolService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FolderToolServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FolderToolService();
        $service->setLogger($container->get('logger'));
        $config = $container->get('config')['fundela_trinket'];
        $service->setChmodOctalFile($config['chmod_octal_file']);
        $service->setChmodOctalFolder($config['chmod_octal_folder']);
        return $service;
    }
}
