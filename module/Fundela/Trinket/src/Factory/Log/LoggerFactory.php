<?php


namespace Fundela\Trinket\Factory\Log;

use Interop\Container\ContainerInterface;
use Laminas\Log\Logger;
use Laminas\Log\Writer\Stream;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class LoggerFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when creating a service.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $path = $container->get('config')['fundela_trinket']['path_log'];
        if (!file_exists($path) || !is_writable($path)) {
            throw new ServiceNotCreatedException(__CLASS__ . '() The log file is not writable');
        }
        $logger = new Logger();
        $logfilePath = realpath($path);
        $writer = new Stream($logfilePath);
        $logger->addWriter($writer);
        return $logger;
    }
}
