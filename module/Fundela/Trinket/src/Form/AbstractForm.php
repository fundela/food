<?php

namespace Fundela\Trinket\Form;

use Laminas\Form\Form;

/**
 *
 * @author Torsten Brieskorn
 */
class AbstractForm extends Form
{
    /**
     * @var bool
     */
    protected bool $primaryKeyAvailable = false;

    /**
     * @return bool
     */
    public function isPrimaryKeyAvailable(): bool
    {
        return $this->primaryKeyAvailable;
    }

    /**
     * @param bool $primaryKeyAvailable
     */
    public function setPrimaryKeyAvailable(bool $primaryKeyAvailable): void
    {
        $this->primaryKeyAvailable = $primaryKeyAvailable;
    }

    /**
     * AJAX Calls kleben mit Hilfe des Element Namen die Messages ann die Elemente.
     * @param string $elementName
     */
    public function getMessages(?string $elementName = null):array
    {
        $messages = parent::getMessages($elementName);
        $messagesClear = [];
        foreach ($messages as $elementName => $message) {
            if (empty($message)) {
                continue;
            }
            $m = array_shift($message);
            if(is_array($m)) {
                foreach($m as $key => $value) {
                    $messagesClear[] = $elementName . '|#|' . $key . ' - ' . $value;
                }
            } elseif (is_string($m)) {
                $messagesClear[] = $elementName . '|#|' . $m;
            } else {
                $messagesClear[] = $elementName . '|#|' . $m;
            }
        }
        return $messagesClear;
    }

    /**
     *
     * @param string $targetMode string | integer
     * @param string $thisElementName The form element name.
     * @param string $thisat
     * @return bool Dateformat default = 'Y-m-d'
     */
    public function switchFormElementDate(string $targetMode, string $thisElementName, string $thisat = 'Y-m-d'): bool
    {
        $date = $this->get($thisElementName)->getValue();
        if ($targetMode == 'string') {
            if (empty($date)) {
                $this->get($thisElementName)->setValue('');
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = '';
                }
                return true;
            }
            if (is_numeric($date)) {
                $this->get($thisElementName)->setValue(date($thisat, $date));
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = date($thisat, $date);
                }
                return true;
            }
        } else if ($targetMode == 'integer') {
            if (empty($date)) {
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = 0;
                }
                return true;
            }
            try {
                $dateTime = new \DateTime($date);
            } catch (\Exception $exception) {
                return false;
            }
            if ($dateTime) {
                $this->get($thisElementName)->setValue($dateTime->getTimestamp());
                if (isset($this->data[$thisElementName])) {
                    $this->data[$thisElementName] = $dateTime->getTimestamp();
                }
                return true;
            }
        }
        return false;
    }

}
