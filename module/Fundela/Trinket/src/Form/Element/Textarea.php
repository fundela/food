<?php

namespace Fundela\Trinket\Form\Element;

class Textarea extends ElementHtmlEntityDecode
{
    /**
     * Seed attributes
     *
     * @var array
     */
    protected $attributes = [
        'type' => 'textarea',
    ];
}
