<?php

namespace Fundela\Trinket\Form\Element;

use Laminas\Form\Element;

class ElementHtmlEntityDecode extends Element
{

    /**
     * Retrieve the element value
     * @return mixed
     */
    public function getValue()
    {
        return html_entity_decode($this->value);
    }

    public function getValueHowItIs()
    {
        return $this->value;
    }

}
