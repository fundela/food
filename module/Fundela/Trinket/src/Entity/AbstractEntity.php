<?php

namespace Fundela\Trinket\Entity;

/**
 * All Entities extends AbstractEntity
 * Entities manage the relation from Data-Keys in Frontend to Data-Keys in database.
 *
 * @author allapow
 */
class AbstractEntity
{

    /**
     * @var array Initial: key=snake case; value=camel case
     */
    protected array $mapping = [];

    protected bool $isMappingFlipped = false;

    /**
     * @var array key in snake case. To fetch storage with camel case, use $this->>getPropertyStorage().
     */
    protected array $storage = [];

    public array $storageAside = [];

    /**
     * @var string Can be set by inheriting classes
     */
    protected string $unique = '';

    /**
     * @var string Used to set $this->unique in $this->exchangeArray() or to unset it in $this->storage.
     */
    protected string $primaryKey = '';

    /**
     * Flip if data comes with key in camel case ...e.g. HTTP request
     * @param bool $onlyIfNotFlipped
     */
    public function flipMapping(bool $onlyIfNotFlipped = true)
    {
        if ($this->isMappingFlipped && $onlyIfNotFlipped) {
            return;
        } else if ($this->isMappingFlipped && !$onlyIfNotFlipped) {
            $this->mapping = array_flip($this->mapping);
            $this->isMappingFlipped = false;
        } else if (!$this->isMappingFlipped) {
            $this->mapping = array_flip($this->mapping);
            $this->isMappingFlipped = true;
        }
    }

    /**
     * Flip back if data comes from DB, Form etc. ...with snake case as key
     */
    public function flipMappingBack()
    {
        if ($this->isMappingFlipped) {
            $this->mapping = array_flip($this->mapping);
            $this->isMappingFlipped = false;
        }
    }

    /**
     * Sets the values only if there is a corresponding mapping.
     * Exchange $data to an Array.
     *
     * @param array $data
     * @return bool
     */
    private function exchangeArray(array $data): bool
    {
        $this->storage = [];
        foreach ($this->mapping as $key => $value) {
            if (isset($data[$key])) {
                if ($this->isMappingFlipped) {
                    $this->storage[$value] = $data[$key];
                } else {
                    $this->storage[$key] = $data[$key];
                }
            }
        }
        if (empty($this->storage)) {
            return false;
        }
        if (!empty($this->primaryKey) && !empty($this->storage[$this->primaryKey])) {
            $this->unique = $this->storage[$this->primaryKey];
        }
        return true;
    }

    /**
     * Use this if data come from database or form with the same field names like database.
     * @param array $data
     * @return bool
     */
    public function exchangeArrayFromDatabase(array $data): bool
    {
        $this->flipMappingBack();
        return $this->exchangeArray($data);
    }

    /**
     * Use this if data come from the client side.
     * @param array $data
     * @return bool
     */
    public function exchangeArrayFromRequest(array $data): bool
    {
        $this->flipMapping();
        $result = $this->exchangeArray($data);
        $this->flipMappingBack();
        return $result;
    }

    /**
     * @return array
     */
    public function getStorage(): array
    {
        return $this->storage;
    }

    /**
     * Send to the client in camel case.
     * @return array keys in camel case
     */
    public function getPropertyStorage(): array
    {
        $this->flipMappingBack();
        $storage = [];
        foreach ($this->mapping as $key => $val) {
            $storage[$val] = $this->storage[$key];
        }
        return $storage;
    }

    /**
     * @param string $storageKey snake case
     * @return mixed|null The value or NULL.
     */
    public function getStorageValue(string $storageKey)
    {
        if (!isset($this->storage[$storageKey])) {
            return null;
        }
        return $this->storage[$storageKey];
    }

    /**
     * Exchange $data to an Array.
     * @param array $data
     * @return bool
     */
    public function exchangeAsiteArray($data): bool
    {
        if (!is_array($data)) {
            return false;
        }
        foreach ($data as $key => $value) {
            if (isset($this->mapping[$key])) {
                $this->storageAside[$this->mapping[$key]] = $value;
            }
        }
        if (empty($this->storageAside)) {
            return false;
        }
        return true;
    }

    /**
     *
     * @return array Storage keys wich have unequal values.
     * @throws \RuntimeException
     */
    public function compareStorageValues()
    {
        if (!isset($this->storage) || !isset($this->storageAside)) {
            throw new \RuntimeException('One of the storage is empty. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        if (!is_array($this->storage) || !is_array($this->storageAside)) {
            throw new \RuntimeException('One of the storage is not an Array. ' . __CLASS__ . '->' . __FUNCTION__);
        }
        $storageKeys = array_values($this->mapping);
        $unequals = [];
        foreach ($storageKeys as $storageKey) {
            if (isset($this->storageAside[$storageKey]) && $this->storage[$storageKey] != $this->storageAside[$storageKey]) {
                $unequals[] = $storageKey;
            }
        }
        return $unequals;
    }

    public function getMapping()
    {
        return $this->mapping;
    }

    public function isMappingFlipped()
    {
        return $this->isMappingFlipped;
    }

    /**
     * @return string
     */
    public function getUnique(): string
    {
        return $this->unique;
    }

    /**
     * @param string $unique
     */
    public function setUnique(string $unique): void
    {
        if (!isset($this->primaryKey)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() PrimaryKey is not set.');
        }
        $this->storage[$this->primaryKey] = $this->unique = $unique;
    }

    public function setPrimaryKeyValue($value): void
    {
        $this->storage[$this->primaryKey] = $value;
    }

    /**
     *
     */
    public function unsetPrimaryKey(): void
    {
        if (!isset($this->storage[$this->primaryKey])) {
            return;
        }
        unset($this->storage[$this->primaryKey]);
    }

    /**
     *
     */
    public function unsetEmptyValues(): void
    {
        if (empty($this->storage)) {
            return;
        }
        foreach ($this->storage as $key => $value) {
            if (empty($this->storage[$key])) {
                unset($this->storage[$key]);
            }
        }
    }

    /**
     * @param string $field
     * @return bool
     */
    public function fieldExistDatabase(string $field): bool
    {
        $this->flipMappingBack();
        return in_array($field, $this->mapping);
    }

    /**
     * @return array
     * @deprecated is getz sowieso snake case in $this->storage keys
     */
    public function generateTableInsertArray(): array
    {
        if (empty($this->storage)) {
            throw new \RuntimeException(__CLASS__ . '()->' . __FUNCTION__ . '() - do not call me without storage.');
        }
        $this->flipMappingBack();
        $insertArray = [];
        foreach ($this->mapping as $dbField) {
            $insertArray[$dbField] = isset($this->storage[$dbField]) ? $this->storage[$dbField] : null;
        }
        return $insertArray;
    }
}
