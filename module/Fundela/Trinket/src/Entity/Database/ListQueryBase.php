<?php

namespace Fundela\Trinket\Entity\Database;

use Fundela\Trinket\Filter\SnakeCaseFilter;

class ListQueryBase
{
    protected $mapping = [
        'order_field' => 'orderField',
        'order_direc' => 'orderDirec',
        'offset' => 'offset',
        'limit' => 'limit',
        'count' => 'count',
    ];

    protected SnakeCaseFilter $snakeCaseFilter;

    /**
     * ListQueryBase constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->snakeCaseFilter = new SnakeCaseFilter();
        $this->exchangeQuery($data);
    }

    protected function exchangeQuery(array $data): void
    {
        if (empty($data) || !is_array($data)) {
            return;
        }
        foreach ($this->mapping as $key => $val) {
            if (isset($data[$key])) {
                switch ($key) {
                    case 'order_field':
                    case 'order_direc':
                        $this->{$val} = $this->snakeCaseFilter->filter($data[$key]);
                        break;
                    case 'offset':
                    case 'limit':
                    case 'count':
                        $this->{$val} = intval($data[$key]);
                }
            }
        }
    }

    public function getAsQueryParams(int $offsetAdd = 0, int $page = 0): string
    {
        if ($page > 0) {
            $offset = $page - 1;
        } else {
            $offset = $this->getOffset() + $offsetAdd;
        }
        return 'offset=' . $offset . '&limit=' . $this->getLimit() . '&order_field=' . $this->getOrderField() . '&order_direc=' . $this->getOrderDirec();
    }

    public function getOrderSql(): string
    {
        if(!$this->orderFieldSql || !$this->orderDirec) {
            return '';
        }
        return $this->orderFieldSql . ' ' . $this->orderDirec;
    }

    /**
     * @var string Which field should be used for sorting (the GUI string)
     */
    protected string $orderField = '';

    /**
     * @var string Which field should be used for sorting (the database string)
     */
    protected string $orderFieldSql = '';

    /**
     * @var string Order direction
     */
    protected string $orderDirec = 'ASC';

    /**
     * @var int 0 = side one, 1 = side two, 2 = side three, ...
     */
    protected int $offset = 0;

    /**
     * @var int SQL LIMIT
     */
    protected int $limit = 10;

    /**
     * @var int Number of result rows (after query the database).
     */
    protected int $count = 0;

    /**
     * @return string
     */
    public function getOrderField(): string
    {
        return $this->orderField;
    }

    /**
     * @param string $orderField
     */
    public function setOrderField(string $orderField): void
    {
        $this->orderField = $orderField;
    }

    public function setAndPrependOrderFieldSql(string $string): void
    {
        if($this->orderField) {
            $this->orderFieldSql = $string . $this->orderField;
        }
    }

    /**
     * @return string
     */
    public function getOrderDirec(): string
    {
        return $this->orderDirec;
    }

    /**
     * @param string $orderDirec
     */
    public function setOrderDirec(string $orderDirec): void
    {
        $this->orderDirec = $orderDirec;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int offset + 1
     */
    public function getPage(): int
    {
        return $this->offset + 1;
    }

    /**
     * @return int ($this->offset + 1) * $this->limit: if $this->>limit == 0, it returns 0, else it returns
     */
    public function getOffsetLimitResult(): int
    {
        return ($this->offset + 1) * $this->limit;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return intval($this->limit);
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount(int $count): void
    {
        $this->count = $count;
    }
}
