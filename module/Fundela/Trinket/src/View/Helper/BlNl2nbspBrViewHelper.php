<?php

namespace Fundela\Trinket\View\Helper;

use Fundela\Trinket\Filter\BlNl2NbspBrFilter;
use Laminas\View\Helper\AbstractHelper;

class BlNl2nbspBrViewHelper extends AbstractHelper
{
    protected BlNl2NbspBrFilter $blNl2nbspBrFilter;

    /**
     * @param BlNl2NbspBrFilter $blNl2nbspBrFilter
     */
    public function setBlNl2nbspBrFilter(BlNl2NbspBrFilter $blNl2nbspBrFilter): void
    {
        $this->blNl2nbspBrFilter = $blNl2nbspBrFilter;
    }

    /**
     * @param string $text
     * @return string
     */
    public function __invoke(string $text)
    {
        if (empty($text)) {
            return '';
        }
        if (!isset($this->blNl2nbspBrFilter)) {
            $this->blNl2nbspBrFilter = new BlNl2NbspBrFilter();
        }
        return $this->blNl2nbspBrFilter->filter($text);
    }
}
