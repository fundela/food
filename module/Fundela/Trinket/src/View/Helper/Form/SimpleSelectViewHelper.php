<?php

namespace Fundela\Trinket\View\Helper\Form;

use Laminas\View\Helper\AbstractHelper;

class SimpleSelectViewHelper extends AbstractHelper
{

    public function __invoke(array $idAssoc, $currentValue = '', $formFieldName = 'some_name', $cssClass = 'w3-select', $withEmptyValue = true, $emptyValueDisplay = '-- bitte w&auml;hlen --', $emptyValue = '')
    {
        if (empty($idAssoc)) {
            return '';
        }
        $select = '<select name="' . $formFieldName . '" id="' . $formFieldName . '" class="' . $cssClass . '">' . "\n";
        $options = '';
        $isSelected = false;
        if ($withEmptyValue) {
            $selectEmpty = '<option value="' . $emptyValue . '">' . $emptyValueDisplay . '</option>';
        }
        foreach ($idAssoc as $value => $displayValue) {
            $options .= '<option value="' . $value . '"';
            if ($currentValue == $value) {
                $isSelected = true;
                $options .= ' selected';
            }
            $options .= '>' . $displayValue . '</option>' . "\n";
        }
        $selected = '';
        if (!$isSelected) {
            $selected = 'selected';
        }
        if ($withEmptyValue) {
            $options = '<option value="' . $emptyValue . '" ' . $selected . '>' . $emptyValueDisplay . '</option>' . "\n" . $options;
        }
        $select .= $options . "\n" . '</select>';
        return $select;
    }

    protected function getEmptyOption(): string
    {

    }
}
