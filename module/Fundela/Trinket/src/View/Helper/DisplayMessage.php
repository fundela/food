<?php

namespace Fundela\Trinket\View\Helper;

use Laminas\View\Helper\AbstractHelper;

class DisplayMessage extends AbstractHelper
{
    public function __invoke(array $message = null): string
    {
        if (isset($message) && is_array($message) && isset($message['level']) && isset($message['text'])) {
            $this->computeLevelCSSClass($message);
            return $this->flashMessageStyle($message['text'], $message['level']);
        }
        return '';
    }

    private function flashMessageStyle($message, $level): string
    {
        $html = '<div class="w3-panel ' . $level . ' display-message">';
        $html .= "<span onclick=\"this.parentElement.style.display='none'\" class=\"w3-button w3-blue-gray w3-right\"><i class=\"fas fa-times\"></i></span>";
        $html .= '<p>';
        $html .= $message;
        $html .= '</p>';
        $html .= '</div>';
        return $html;
    }

    private function computeLevelCSSClass(&$message): void
    {
        switch ($message['level']) {
            case 'error':
                $message['level'] = 'w3-red';
                break;
            case 'warn':
                $message['level'] = 'w3-orange';
                break;
            case 'success':
                $message['level'] = 'w3-green';
                break;
            case 'info':
                $message['level'] = 'w3-blue';
                break;
        }
    }
}
