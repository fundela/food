<?php

namespace Fundela\Trinket\Tool;

/**
 * Description of StringTool
 *
 * @author allapow
 */
class StringTool
{

    private static string $pregexAlias = "/[a-z0-9_]+/";

    /**
     * $inputString macht mit html_entity_decode() alles zu "normalen" Zeichen.
     * Per Regex werden nur Buchstaben und Zahlen durch gelassen.
     *
     * @param string $inputString
     * @return string An empty String on failure.
     */
    public static function computeAlias(string $inputString): string
    {
        $inputString = html_entity_decode(preg_replace(['/ä/i', '/ü/i', '/ö/i', '/ß/i'], ['ae', 'ue', 'oe', 'ss'], strtolower($inputString)));
        $matches = [];
        $kp = preg_match_all(self::$pregexAlias, $inputString, $matches);
        if ($kp) {
            return implode('-', $matches[0]);
        }
        return '';
    }

}
