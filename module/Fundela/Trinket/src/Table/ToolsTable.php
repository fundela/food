<?php

namespace Fundela\Trinket\Table;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Db\Adapter\Driver\ResultInterface;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;
use Laminas\Log\Logger;

class ToolsTable implements AdapterAwareInterface
{
    protected Adapter $adapter;

    /**
     * @var Logger
     */
    protected Logger $logger;

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * MySQL
     *
     * @param $table
     * @param $column
     * @return array
     */
    public function getEnumValuesMySQL($table, $column)
    {
        $parameter = new ParameterContainer([$this->adapter->getCurrentSchema(), $table, $column]);
        $stmt = $this->adapter->createStatement('SELECT SUBSTRING(COLUMN_TYPE,5) AS resultstring '
            . 'FROM information_schema.COLUMNS '
            . 'WHERE TABLE_SCHEMA=? '
            . 'AND TABLE_NAME=? '
            . 'AND COLUMN_NAME=?', $parameter);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() == 1) {
            $current = $result->current();
            if (!empty($current['resultstring'])) {
                return str_getcsv(substr($current['resultstring'], 1, strlen($current['resultstring']) - 2), ',', "'");
            }
        }
        return [];
    }

    /**
     * @param string $enum
     * @return array
     */
    public function getEnumValuesPostgreSQL(string $enum): array
    {
        $query = "SELECT enum_range(NULL::$enum)";
        $stmt = $this->adapter->createStatement($query);
        $result = $stmt->execute();
        if ($result->valid() && $result->count() > 0) {
            $current = $result->current();
            $arr = explode(',', str_replace(['{', '}'], '', $current['enum_range']));
            return $arr;
        }
        return ['nix'];
    }
}
