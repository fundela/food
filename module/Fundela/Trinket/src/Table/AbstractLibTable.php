<?php

namespace Fundela\Trinket\Table;

use Fundela\Trinket\Entity\AbstractEntity;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\ParameterContainer;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Select;
use Laminas\Db\TableGateway\AbstractTableGateway;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Log\Logger;

/**
 *
 * @author allapow
 */
class AbstractLibTable extends AbstractTableGateway implements AdapterAwareInterface
{

    /**
     * @var Adapter
     */
    protected $adapter;

    /**
     * @var Logger
     */
    protected $logger;

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new HydratingResultSet();
        $this->initialize();
    }

    /**
     * @return Adapter
     */
    public function getAdapter(): Adapter
    {
        return $this->adapter;
    }

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \Exception $exception
     * @param string $class Typically __CLASS__
     * @param string $function Typically __FUNCTION__
     */
    protected function log(\Exception $exception, string $class = '', string $function = '')
    {
        $this->logger->err('Database error in ' . $class . '()->' . $function . '()');
        $this->logger->err($exception->getMessage());
    }

    public function uuid(): string
    {
        try {
            $resultSet = $this->adapter->query('SELECT uuid_generate_v4() AS new_uuid');
            return $resultSet->execute()->current()['new_uuid'];
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param array $values Reference
     * @param bool $alsoEmptyString
     */
    protected function unsetNullFields(array &$values, bool $alsoEmptyString = false)
    {
        foreach ($values as $key => $val) {
            if ($val === null) {
                unset($values[$key]);
            }
            if ($alsoEmptyString && $val === '') {
                unset($values[$key]);
            }
        }
    }

    /**
     * @return string Timestamp with micro seconds (e.g. for PostgreSQL timestamp)
     */
    protected function getTimestamp(): string
    {
        try {
            $dt = new \DateTime('now');
            return $dt->format('Y-m-d H:i:s.u');
        } catch (\Exception $ex) {
            $this->log($ex, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param AbstractEntity $enity An Entity with not empty primaryKey member. Primary key must be from type UUID.
     * @return string
     */
    public function insertWithEntity(AbstractEntity $enity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        $enity->setPrimaryKeyValue($uuid);
        $enity->unsetEmptyValues();
        try {
            $insert->values($enity->getStorage());
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function computeListQueryBase(Select &$select, ListQueryBase $listQueryBase): void
    {
        if (!empty($limit = $listQueryBase->getLimit())) {
            $select->limit($limit);
        }
        if (!empty($offset = $listQueryBase->getOffset())) {
            $select->offset($offset);
        }
        if (!empty($of = $listQueryBase->getOrderField()) && !empty($od = $listQueryBase->getOrderDirec())) {
            if (!$this->existColumnPostgreSQL($of)) {
                return;
            }
            $select->order("$of $od");
        }
    }

    public function existColumnPostgreSQL(string $column): bool
    {
        $params = new ParameterContainer([':ex_table' => $this->getTable(), ':ex_column' => $column]);
        $stmt = $this->adapter->createStatement(
            'SELECT COUNT(table_catalog) AS ex_c FROM information_schema.columns WHERE table_schema=\'public\' AND table_name=:ex_table AND column_name=:ex_column'
            , $params
        );
        $result = $stmt->execute();
        if ($result->valid()) {
            $c = $result->current();
            return $c['ex_c'] > 0;
        }
        return false;
    }
}
