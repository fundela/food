<?php

namespace Fundela\Trinket\Table;

use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class ConfigTable extends AbstractLibTable
{
    /**
     * @var string
     */
    protected $table = 'config';

    /**
     * @param string $key
     * @return string
     */
    public function getConfigValueByKey(string $key): string
    {
        $select = $this->sql->select();
        try {
            $select->where(['config_key' => $key]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->current()->getArrayCopy()['config_value'];
            }
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() No config found for key: ' . $key);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }
}
