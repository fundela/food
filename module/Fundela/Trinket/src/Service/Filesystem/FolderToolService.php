<?php

namespace Fundela\Trinket\Service\Filesystem;

use Fundela\Trinket\Service\AbstractService;

class FolderToolService extends AbstractService
{
    protected int $chmodOctalFile = 0664;
    protected int $chmodOctalFolder = 0775;

    public function setChmodOctalFile(int $chmodOctalFile): void
    {
        $this->chmodOctalFile = $chmodOctalFile;
    }

    public function setChmodOctalFolder(int $chmodOctalFolder): void
    {
        $this->chmodOctalFolder = $chmodOctalFolder;
    }

    /**
     * Warning: chmod(): Operation not permitted
     * ...you must be the owner of the file!
     *
     * @param string $filePath
     * @return bool False if the file not exist or chmod() returns false.
     */
    public function chmodFile(string $filePath): bool
    {
        if (!file_exists($filePath)) {
            return false;
        }
        return chmod($filePath, $this->chmodOctalFile);
    }

    /**
     * @param string $folder
     * @return bool
     */
    private function mkdirAndChmod(string $folder): bool
    {
        if (!file_exists($folder)) {
            if (mkdir($folder) && chmod($folder, $this->chmodOctalFolder)) {
                return true;
            }
            $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() Can not create folder: ' . $folder);
            return false;
        }
        return true;
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $string01 Skips it when it's empty.
     * @param int $year
     * @param string $month It will make str_pad($month, 2, '0', STR_PAD_LEFT)
     * @param string $string02 Skips it when it's empty.
     * @return string The folder after the root folder with leading slash.
     * It creates the folder hierarchy: $string01 / $year / $month / $string02.
     */
    public function computeStringDateStringFolder(string $rootFolderAbsolute, string $string01, int $year, string $month, string $string02): string
    {
        if (!file_exists($rootFolderAbsolute)) {
            return '';
        }
        $folder = $rootFolderAbsolute;
        $this->mkdirAndChmod($folder);
        if (!empty($string01)) {
            $folder .= DIRECTORY_SEPARATOR . $string01;
            $this->mkdirAndChmod($folder);
        }
        $folder .= DIRECTORY_SEPARATOR . $year;
        $this->mkdirAndChmod($folder);
        $folder .= DIRECTORY_SEPARATOR . str_pad($month, 2, '0', STR_PAD_LEFT);
        $this->mkdirAndChmod($folder);
        if (!empty($string02)) {
            $folder .= DIRECTORY_SEPARATOR . $string02;
            $this->mkdirAndChmod($folder);
        }
        return substr($folder, strlen($rootFolderAbsolute));
    }

    /**
     * @param string $rootFolderAbsolute
     * @param string $string01 Skips it when it's empty.
     * @param int $year
     * @param string $month It will make str_pad($month, 2, '0', STR_PAD_LEFT)
     * @param string $string02 Skips it when it's empty.
     * @return string The folder with $rootFolderAbsolute.
     * Get the folder from create function $this->computeStringDateStringFolder(...)
     */
    public function getStringDateStringFolder(string $rootFolderAbsolute, string $string01, int $year, string $month, string $string02): string
    {
        $folder = realpath($rootFolderAbsolute);
        if (!file_exists($folder)) {
            return '';
        }
        if (!empty($string01)) {
            $folder .= DIRECTORY_SEPARATOR . $string01;
            if (!file_exists($folder)) {
                return '';
            }
        }
        $folder .= DIRECTORY_SEPARATOR . $year;
        if (!file_exists($folder)) {
            return '';
        }
        $folder .= DIRECTORY_SEPARATOR . str_pad($month, 2, '0', STR_PAD_LEFT);
        if (!file_exists($folder)) {
            return '';
        }
        if (!empty($string02)) {
            $folder .= DIRECTORY_SEPARATOR . $string02;
            if (!file_exists($folder)) {
                return '';
            }
        }
        return $folder;
    }

    public function deleteDirRecursive(string $dir): bool
    {
        $dirIter = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($dirIter, \RecursiveIteratorIterator::CHILD_FIRST);
        foreach ($files as $file) {
            $filePath = $file->getRealPath();
            if ($file->isDir()) {
                $this->deleteDirRecursive($filePath);
            } else {
                unlink($filePath);
            }
        }
        return rmdir($dir);
    }
}
