<?php


namespace Fundela\Trinket\Service;


use Laminas\Log\Logger;

class AbstractService
{
    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var string
     */
    protected $message = '';

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger): void
    {
        $this->logger = $logger;
    }

    public function getLogger(): Logger
    {
        return $this->logger;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

}
