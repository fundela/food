<?php

namespace Fundela\Trinket\Controller;

use Laminas\Http\PhpEnvironment\Response as HttpResponse;
use Laminas\Log\Logger;
use Laminas\Mvc\Controller\AbstractRestfulController;
use Laminas\Stdlib\RequestInterface as Request;
use Laminas\Stdlib\ResponseInterface as Response;

class AbstractRestController extends AbstractRestfulController
{

    /**
     *
     * @var Logger
     */
    protected $logger;

    /**
     *
     * @param Logger $logger
     */
    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return HttpResponse|Response
     */
    public function getResponse()
    {
        if ($this->response instanceof HttpResponse) {
            return $this->response;
        }
        throw new \RuntimeException('watt!?!? keine HttpResponse in ' . __CLASS__ . '()->' . __FUNCTION__ . '() on line ' . __LINE__);
    }

    protected function getMatchedRouteName(): string
    {
        return $this->getEvent()->getRouteMatch()->getMatchedRouteName();
    }
}
