<?php

namespace Fundela\Trinket\Controller\Plugin;

use Laminas\I18n\Translator\Translator;
use Laminas\Mvc\Controller\Plugin\AbstractPlugin;

class TranslatorPlugin extends AbstractPlugin
{
    /**
     * @var Translator
     */
    protected $translator;

    /**
     * TranslatorPlugin constructor.
     * @param Translator $translator
     */
    function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function __invoke(string $string, string $textDomain = 'default', $locale = null): string
    {
        return $this->translator->translate($string, $textDomain, $locale);
    }
}
