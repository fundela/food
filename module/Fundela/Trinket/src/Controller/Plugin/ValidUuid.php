<?php

namespace Fundela\Trinket\Controller\Plugin;

use Laminas\Mvc\Controller\Plugin\AbstractPlugin;
use Laminas\Validator\Uuid;

class ValidUuid extends AbstractPlugin
{

    /**
     * @var Uuid
     */
    private $uuidValidator;

    /**
     * ValidUuid constructor.
     */
    public function __construct()
    {
        $this->uuidValidator = new Uuid();
    }

    /**
     * @param string $guid
     * @return bool
     */
    public function __invoke(string $guid)
    {
        return $this->uuidValidator->isValid($guid);
    }
}
