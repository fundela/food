<?php

namespace Fundela\Trinket;

use Fundela\Trinket\Factory\Service\Filesystem\FolderToolServiceFactory;
use Fundela\Trinket\Factory\Table\ConfigTableFactory;
use Fundela\Trinket\Factory\Table\IsoCountryTableFactory;
use Fundela\Trinket\Factory\Table\QuantityunitTableFactory;
use Fundela\Trinket\Factory\Table\ToolsTableFactory;
use Fundela\Trinket\Service\Filesystem\FolderToolService;
use Fundela\Trinket\Table\ConfigTable;
use Fundela\Trinket\Table\IsoCountryTable;
use Fundela\Trinket\Table\QuantityunitTable;
use Fundela\Trinket\Table\ToolsTable;
use Fundela\Trinket\View\Helper\BlNl2nbspBrViewHelper;
use Fundela\Trinket\View\Helper\DisplayMessage;
use Fundela\Trinket\View\Helper\Form\SimpleSelectViewHelper;
use Fundela\Trinket\View\Helper\Printr;

return [
    'router' => [
        'routes' => [],
    ],
    'controllers' => [
        'factories' => [],
        'invokables' => [],
    ],
    'controller_plugins' => [
        'factories' => [
            'translate' => Factory\Controller\Plugin\TranslatorPluginFactory::class,
        ],
        'invokables' => [
            Controller\Plugin\ValidUuid::class => Controller\Plugin\ValidUuid::class
        ],
        'aliases' => [
            'isValidUuid' => Controller\Plugin\ValidUuid::class
        ]
    ],
    'service_manager' => [
        'factories' => [
            'logger' => Factory\Log\LoggerFactory::class,
            // table
            ToolsTable::class => ToolsTableFactory::class,
            IsoCountryTable::class => IsoCountryTableFactory::class,
            QuantityunitTable::class => QuantityunitTableFactory::class,
            ConfigTable::class => ConfigTableFactory::class,
            // service
            FolderToolService::class => FolderToolServiceFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers' => [
        'factories' => [
        ],
        'invokables' => [
            DisplayMessage::class => DisplayMessage::class,
            Printr::class => Printr::class,
            BlNl2nbspBrViewHelper::class => BlNl2nbspBrViewHelper::class,
            SimpleSelectViewHelper::class => SimpleSelectViewHelper::class,
        ],
        'aliases' => [
            'showMessage' => DisplayMessage::class,
            'printr' => Printr::class,
            'blnl2html' => BlNl2nbspBrViewHelper::class,
            'simpleSelect' => SimpleSelectViewHelper::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
//            'pagination/simple-param' => __DIR__ . '/../view/pagination/simple-param.phtml',
            'pagination/simple-query' => __DIR__ . '/../view/pagination/simple-query.phtml',
        ],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'fundela_trinket' => [
        'path_log' => __DIR__ . '/../../../../data/log/app.log',
        'chmod_octal_file' => 0664,
        'chmod_octal_folder' => 0775,
    ]
];
