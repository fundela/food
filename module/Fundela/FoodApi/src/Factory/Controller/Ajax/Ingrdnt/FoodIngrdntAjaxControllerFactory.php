<?php

namespace Fundela\FoodApi\Factory\Controller\Ajax\Ingrdnt;

use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\User\Service\UserService;
use Fundela\FoodApi\Controller\Ajax\Ingrdnt\FoodIngrdntAjaxController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodIngrdntAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodIngrdntAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodIngrdntService($container->get(FoodIngrdntService::class));
        return $controller;
    }
}
