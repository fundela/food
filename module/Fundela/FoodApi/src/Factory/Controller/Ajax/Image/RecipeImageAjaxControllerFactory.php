<?php

namespace Fundela\FoodApi\Factory\Controller\Ajax\Image;

use Fundela\Food\Service\Recipe\FoodRecipeImageService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Image\Form\ImageForm;
use Fundela\User\Service\UserService;
use Fundela\FoodApi\Controller\Ajax\Image\RecipeImageAjaxController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RecipeImageAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new RecipeImageAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setImageForm($container->get(ImageForm::class));
        $controller->setFoodRecipeImageService($container->get(FoodRecipeImageService::class));
        $controller->setFoodRecipeService($container->get(FoodRecipeService::class));
        return $controller;
    }
}
