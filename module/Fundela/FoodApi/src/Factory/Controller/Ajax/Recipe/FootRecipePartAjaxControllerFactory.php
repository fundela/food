<?php

namespace Fundela\FoodApi\Factory\Controller\Ajax\Recipe;

use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\User\Service\UserService;
use Fundela\FoodApi\Controller\Ajax\Recipe\FoodRecipePartAjaxController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FootRecipePartAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodRecipePartAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodRecipePartService($container->get(FoodRecipePartService::class));
        return $controller;
    }
}
