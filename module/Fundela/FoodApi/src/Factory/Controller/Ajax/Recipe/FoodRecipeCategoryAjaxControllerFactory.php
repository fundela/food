<?php

namespace Fundela\FoodApi\Factory\Controller\Ajax\Recipe;

use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\User\Service\UserService;
use Fundela\FoodApi\Controller\Ajax\Recipe\FoodRecipeCategoryAjaxController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRecipeCategoryAjaxControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodRecipeCategoryAjaxController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodRecipeService($container->get(FoodRecipeService::class));
        return $controller;
    }
}
