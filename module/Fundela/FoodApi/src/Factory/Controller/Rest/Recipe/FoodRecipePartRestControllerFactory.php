<?php

namespace Fundela\FoodApi\Factory\Controller\Rest\Recipe;

use Fundela\Food\Form\Recipe\FoodRecipePartForm;
use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\User\Service\UserService;
use Fundela\FoodApi\Controller\Rest\Recipe\FoodRecipePartRestController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRecipePartRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodRecipePartRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodRecipePartForm($container->get(FoodRecipePartForm::class));
        $controller->setFoodRecipePartService($container->get(FoodRecipePartService::class));
        return $controller;
    }
}
