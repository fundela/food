<?php

namespace Fundela\FoodApi\Factory\Controller\Rest\Raw;

use Fundela\Food\Form\Raw\FoodRawPartForm;
use Fundela\Food\Service\Raw\FoodRawPartService;
use Fundela\User\Service\UserService;
use Fundela\FoodApi\Controller\Rest\Raw\FoodRawPartRestController;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRawPartRestControllerFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $controller = new FoodRawPartRestController();
        $controller->setLogger($container->get('logger'));
        $controller->setUserService($container->get(UserService::class));
        $controller->setFoodRawPartForm($container->get(FoodRawPartForm::class));
        $controller->setFoodRawPartService($container->get(FoodRawPartService::class));
        return $controller;
    }
}
