<?php

namespace Fundela\FoodApi\Controller\Rest\Raw;

use Fundela\Food\Form\Raw\FoodRawPartForm;
use Fundela\Food\Service\Raw\FoodRawPartService;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Validator\Uuid;

class FoodRawPartRestController extends AbstractUserRestController
{
    protected FoodRawPartForm $foodRawPartForm;
    protected FoodRawPartService $foodRawPartService;

    public function setFoodRawPartForm(FoodRawPartForm $foodRawPartForm): void
    {
        $this->foodRawPartForm = $foodRawPartForm;
    }

    public function setFoodRawPartService(FoodRawPartService $foodRawPartService): void
    {
        $this->foodRawPartService = $foodRawPartService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $data['food_raw_part_qntty'] = floatval(str_replace(',', '.', $data['food_raw_part_qntty']));
        $this->foodRawPartForm->init();
        $this->foodRawPartForm->setData($data);
        if ($this->foodRawPartForm->isValid()) {
            $d = $this->foodRawPartForm->getData();
            if (!empty($newUuid = $this->foodRawPartService->insertFoodRawPart($d['food_raw_uuid'], $d['food_ingrdnt_id'], $d['food_raw_part_qntty']))) {
                $jsonModel->setSuccess(1);
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
            }
        } else {
            $jsonModel->addMessages($this->foodRawPartForm->getMessages());
        }
        return $jsonModel;
    }

    /**
     * GET foodRawParts for foodRaw
     *
     * @param string $id food_raw_uuid
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setArr($this->foodRawPartService->getFoodRawPartsForFoodRaw($id));
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
