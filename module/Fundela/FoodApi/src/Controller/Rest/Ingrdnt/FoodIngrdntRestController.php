<?php

namespace Fundela\FoodApi\Controller\Rest\Ingrdnt;

use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserRestController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class FoodIngrdntRestController extends AbstractUserRestController
{
    protected FoodIngrdntService $foodIngrdntService;

    public function setFoodIngrdntService(FoodIngrdntService $foodIngrdntService): void
    {
        $this->foodIngrdntService = $foodIngrdntService;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $queryBase = new ListQueryBase($request->getQuery()->toArray());
        $ingrdntName = filter_var($request->getQuery('ingrdnt_name'), FILTER_SANITIZE_STRING);
        $rawUuid = filter_var($request->getQuery('raw_uuid'), FILTER_SANITIZE_STRING);
        $ingrdnts = $this->foodIngrdntService->searchFoodIngrdnt($queryBase, $ingrdntName, $rawUuid);
        $jsonModel->setArr($ingrdnts);
        return $jsonModel;
    }

    /**
     * Delete a foot_ingrdnt with all references in food_raw_part.
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(2)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        if($this->getRequest()->isDelete()) {
            if($this->foodIngrdntService->deleteIngrdntComplete($id)) {
                $jsonModel->setSuccess(1);
            } else {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_500);
            }
        }
        return $jsonModel;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id
     * @param array $data
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        $this->getResponse()->setStatusCode(Response::STATUS_CODE_501);
        return $jsonModel;
    }
}
