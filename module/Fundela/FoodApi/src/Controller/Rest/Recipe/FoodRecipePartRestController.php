<?php

namespace Fundela\FoodApi\Controller\Rest\Recipe;

use Fundela\Food\Form\Recipe\FoodRecipePartForm;
use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Trinket\Filter\SanitizeStringFilter;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserRestController;
use Laminas\Form\FormInterface;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class FoodRecipePartRestController extends AbstractUserRestController
{
    protected FoodRecipePartForm $foodRecipePartForm;
    protected FoodRecipePartService $foodRecipePartService;

    public function setFoodRecipePartForm(FoodRecipePartForm $foodRecipePartForm): void
    {
        $this->foodRecipePartForm = $foodRecipePartForm;
    }

    public function setFoodRecipePartService(FoodRecipePartService $foodRecipePartService): void
    {
        $this->foodRecipePartService = $foodRecipePartService;
    }

    /**
     * POST maps to create().
     * @param array $data
     * @return JsonModel
     */
    public function create($data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $this->foodRecipePartForm->init();
        $this->foodRecipePartForm->setData($data);
        if (!$this->foodRecipePartForm->isValid()) {
            $jsonModel->addMessages($this->foodRecipePartForm->getMessages());
            return $jsonModel;
        }
        if (!empty($partUuid = $this->foodRecipePartService->insertFoodRecipePart($this->foodRecipePartForm->getData(FormInterface::VALUES_AS_ARRAY)))) {
            $jsonModel->setUuid($partUuid);
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @param string $id
     * @return JsonModel
     */
    public function get($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (empty($recipeParts = $this->foodRecipePartService->getFoodRecipePartsForFoodRecipe($id))) {
            return $jsonModel;
        }
        $jsonModel->setArr($recipeParts);
        return $jsonModel;
    }

    /**
     * DELETE maps to delete().
     * @param string $id
     * @return JsonModel
     */
    public function delete($id): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(4)
            || !$this->foodRecipePartService->isUserRecipeOwner($this->userService->getUserUuid(), $id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->foodRecipePartService->deleteFoodRecipePart($id)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * GET
     * @return JsonModel
     */
    public function getList(): JsonModel
    {
        $jsonModel = new JsonModel();
        /**
         * @todo was machen? Wird gerufen wenn die ID im JS leer ist.
         */
        $jsonModel->setSuccess(42);
        return $jsonModel;
    }

    /**
     * PUT maps to update().
     * @param string $id food_recipe_part_uuid
     * @param array $data ['part_qntty' => 0.0, 'part_comment' => '']
     * @return JsonModel
     */
    public function update($id, $data): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(4)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!(new Uuid())->isValid($id) || !$this->foodRecipePartService->isUserRecipeOwner($this->userService->getUserUuid(), $id)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->foodRecipePartService->updateFoodRecipePartQuantity($id, floatval($data['part_qntty'] ?? 0))
            && $this->foodRecipePartService->updateFoodRecipePartComment($id, (new SanitizeStringFilter())->filter($data['part_comment'] ?? ''))) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
