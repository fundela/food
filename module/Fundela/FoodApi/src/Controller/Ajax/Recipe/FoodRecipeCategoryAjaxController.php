<?php

namespace Fundela\FoodApi\Controller\Ajax\Recipe;

use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Validator\Uuid;

class FoodRecipeCategoryAjaxController extends AbstractUserController
{
    protected FoodRecipeService $foodRecipeService;

    public function setFoodRecipeService(FoodRecipeService $foodRecipeService): void
    {
        $this->foodRecipeService = $foodRecipeService;
    }

    /**
     * @return JsonModel
     */
    public function foodRecipeCategoryIdAssocAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->foodRecipeService->getFoodRecipeCategoryIdAssoc(true));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function foodRecipeCategoriesAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!empty($recipeUuid = $this->params('recipe_uuid', '')) && !(new Uuid())->isValid($recipeUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $jsonModel->setVariable('cats', $this->foodRecipeService->getFoodRecipeCategories($recipeUuid));
        $jsonModel->setVariable('catsRest', $this->foodRecipeService->getFoodRecipeCategoriesRest($recipeUuid));
        $jsonModel->setSuccess(1);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function foodRecipeCategoryAddAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (
            empty($recipeUuid = $this->params('recipe_uuid')) || !(new Uuid())->isValid($recipeUuid)
            || empty($categoryId = $this->params('cat_id'))
        ) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if (empty($newId = $this->foodRecipeService->insertFoodRecipeCategory($recipeUuid, $categoryId)) || $newId == 'exist') {
            return $jsonModel;
        }
        $jsonModel->setSuccess(1);
        $jsonModel->setUuid($newId);
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function foodRecipeCategoryDeleteAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (empty($relId = $this->params('rel_id'))) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->foodRecipeService->deleteFoodRecipeCategoryRel($relId) > 0) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
