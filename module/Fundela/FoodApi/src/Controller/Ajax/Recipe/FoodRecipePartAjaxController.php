<?php

namespace Fundela\FoodApi\Controller\Ajax\Recipe;

use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class FoodRecipePartAjaxController extends AbstractUserController
{
    protected FoodRecipePartService $foodRecipePartService;

    public function setFoodRecipePartService(FoodRecipePartService $foodRecipePartService): void
    {
        $this->foodRecipePartService = $foodRecipePartService;
    }

    /**
     * @return JsonModel
     */
    public function updateOrderPriorityAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if($this->foodRecipePartService->updateFoodRecipePartOrderPriority($this->params('partUuids'))) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
