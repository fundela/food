<?php

namespace Fundela\FoodApi\Controller\Ajax\Raw;

use Fundela\Food\Service\Raw\FoodRawPartService;
use Fundela\Food\Service\Raw\FoodRawService;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class FoodRawPartAjaxController extends AbstractUserController
{
    protected FoodRawService $foodRawService;
    protected FoodRawPartService $foodRawPartService;

    public function setFoodRawService(FoodRawService $foodRawService): void
    {
        $this->foodRawService = $foodRawService;
    }

    public function setFoodRawPartService(FoodRawPartService $foodRawPartService): void
    {
        $this->foodRawPartService = $foodRawPartService;
    }

    /**
     * @return JsonModel
     */
    public function updateQuantityAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost()->toArray();
            $partUuid = $post['part_uuid'];
            $partQntty = floatval(str_replace(',', '.', $post['part_qntty']));
            if($this->foodRawPartService->updateFoodRawPartQuantity($partUuid, $partQntty)) {
                $jsonModel->setSuccess(1);
            }
        }

        return $jsonModel;
    }
}
