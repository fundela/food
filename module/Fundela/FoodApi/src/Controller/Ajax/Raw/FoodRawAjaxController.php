<?php

namespace Fundela\FoodApi\Controller\Ajax\Raw;

use Fundela\Food\Service\Raw\FoodRawService;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\Response;
use Laminas\Validator\Uuid;

class FoodRawAjaxController extends AbstractUserController
{
    protected FoodRawService $foodRawService;

    public function setFoodRawService(FoodRawService $foodRawService): void
    {
        $this->foodRawService = $foodRawService;
    }

    /**
     * @return JsonModel
     */
    public function foodRawCategoryIdAssocAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $jsonModel->setObj($this->foodRawService->getFoodRawCategoryIdAssoc());
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function searchAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $recipeUuid = $this->params()->fromQuery('recipe_uuid', '');
        if(!(new Uuid())->isValid($recipeUuid)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        $query = $request->getQuery()->toArray();
        $listQueryBase = new ListQueryBase($query);
        $rawName = $this->params()->fromQuery('raw_name', '');
        $rawCatId = intval(filter_var($this->params()->fromQuery('raw_cat', 0), FILTER_SANITIZE_NUMBER_INT));
        $raws = $this->foodRawService->searchFoodRawForRecipe($listQueryBase, $recipeUuid, $rawName, $rawCatId);
        $jsonModel->setArr($raws);
        return $jsonModel;
    }
}
