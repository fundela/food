<?php

namespace Fundela\FoodApi\Controller\Ajax\Image;

use Fundela\Food\Service\Recipe\FoodRecipeImageService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Image\Form\ImageForm;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\Response;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Validator\Uuid;

class RecipeImageAjaxController extends AbstractUserController
{
    protected ImageForm $imageForm;
    protected FoodRecipeImageService $foodRecipeImageService;
    protected FoodRecipeService $foodRecipeService;

    public function setImageForm(ImageForm $imageForm): void
    {
        $this->imageForm = $imageForm;
    }

    public function setFoodRecipeImageService(FoodRecipeImageService $foodRecipeImageService): void
    {
        $this->foodRecipeImageService = $foodRecipeImageService;
    }

    public function setFoodRecipeService(FoodRecipeService $foodRecipeService): void
    {
        $this->foodRecipeService = $foodRecipeService;
    }

    /**
     * @return JsonModel
     */
    public function uploadImageAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->getRequest()->isPost()) {
            return $jsonModel;
        }
        $recipeUuid = filter_input(INPUT_POST, 'recipe_uuid', FILTER_SANITIZE_STRING);
        if (!$this->userService->isUserInRole(5) || !(new Uuid())->isValid($recipeUuid)
            || !$this->foodRecipeService->checkEditAllowed($recipeUuid, $this->userService->getUserUuid())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $request = $this->getRequest();
        $post = ArrayUtils::merge($request->getPost()->toArray(), $request->getFiles()->toArray());
        $this->imageForm->setData($post);
        if ($this->imageForm->isValid() && (new Uuid())->isValid($recipeUuid)) {
            if (($imageId = $this->foodRecipeImageService->insertAndUploadRecipeImage($this->imageForm->getData(), $recipeUuid)) > 0) {
                $this->getResponse()->setStatusCode(Response::STATUS_CODE_201);
                $jsonModel->setSuccess(1);
                $jsonModel->setUuid($imageId);
            } else {
                $jsonModel->addMessage($this->foodRecipeImageService->getMessage());
            }
        } else {
            $jsonModel->addMessages($this->imageForm->getMessages());
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function recipeImagesAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(5)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        if (!$this->getRequest()->isGet()) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        $recipeUuid = filter_var($this->params('recipe_uuid'), FILTER_SANITIZE_STRING);
        if ((new Uuid())->isValid($recipeUuid)) {
            $jsonModel->setArr($this->foodRecipeImageService->getFoodRecipeImages($recipeUuid));
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }

    /**
     * @return JsonModel
     */
    public function deleteImageAction(): JsonModel
    {
        $jsonModel = new JsonModel();
        $recipeUuid = $this->params('recipeUuid');
        if (!$this->userService->isUserInRole(5) || !(new Uuid())->isValid($recipeUuid)
            || !$this->foodRecipeService->checkEditAllowed($recipeUuid, $this->userService->getUserUuid())) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        $imageId = $this->params('imageId');
        if (!$this->getRequest()->isDelete() || !(new Uuid())->isValid($recipeUuid) || empty($imageId)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_400);
            return $jsonModel;
        }
        if ($this->foodRecipeImageService->deleteImageRel($recipeUuid, $imageId) && $this->foodRecipeImageService->deleteImageComplete($imageId)) {
            $jsonModel->setSuccess(1);
        }
        return $jsonModel;
    }
}
