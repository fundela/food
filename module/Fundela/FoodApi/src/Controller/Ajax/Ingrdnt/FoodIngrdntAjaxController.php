<?php

namespace Fundela\FoodApi\Controller\Ajax\Ingrdnt;

use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\Trinket\View\Model\JsonModel;
use Fundela\User\Controller\AbstractUserController;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Http\PhpEnvironment\Response;

class FoodIngrdntAjaxController extends AbstractUserController
{
    protected FoodIngrdntService $foodIngrdntService;

    public function setFoodIngrdntService(FoodIngrdntService $foodIngrdntService): void
    {
        $this->foodIngrdntService = $foodIngrdntService;
    }

    /**
     * @return JsonModel
     */
    public function updateOrderPriorityAction()
    {
        $jsonModel = new JsonModel();
        if (!$this->userService->isUserInRole(3)) {
            $this->getResponse()->setStatusCode(Response::STATUS_CODE_403);
            return $jsonModel;
        }
        /** @var Request $request */
        $request = $this->getRequest();
        if($request->isPost()) {
            $post = $request->getPost()->toArray();
            $ingrdntId = intval($post['ingrdnt_id']);
            $orderPrio = intval($post['order_priority']);
            if($this->foodIngrdntService->updateFoodIngrdntPriority($ingrdntId, $orderPrio)) {
                $jsonModel->setSuccess(1);
            }
        }

        return $jsonModel;
    }
}
