<?php

namespace Fundela\FoodApi;

use Fundela\FoodApi\Controller\Ajax\Image\RecipeImageAjaxController;
use Fundela\FoodApi\Controller\Ajax\Ingrdnt\FoodIngrdntAjaxController;
use Fundela\FoodApi\Controller\Ajax\Raw\FoodRawAjaxController;
use Fundela\FoodApi\Controller\Ajax\Raw\FoodRawPartAjaxController;
use Fundela\FoodApi\Controller\Ajax\Recipe\FoodRecipeCategoryAjaxController;
use Fundela\FoodApi\Controller\Ajax\Recipe\FoodRecipePartAjaxController;
use Fundela\FoodApi\Controller\Rest\Raw\FoodRawPartRestController;
use Fundela\FoodApi\Controller\Rest\Ingrdnt\FoodIngrdntRestController;
use Fundela\FoodApi\Controller\Rest\Recipe\FoodRecipePartRestController;
use Fundela\FoodApi\Factory\Controller\Ajax\Image\RecipeImageAjaxControllerFactory;
use Fundela\FoodApi\Factory\Controller\Ajax\Ingrdnt\FoodIngrdntAjaxControllerFactory;
use Fundela\FoodApi\Factory\Controller\Ajax\Raw\FoodRawAjaxControllerFactory;
use Fundela\FoodApi\Factory\Controller\Ajax\Raw\FoodRawPartAjaxControllerFactory;
use Fundela\FoodApi\Factory\Controller\Ajax\Recipe\FoodRecipeCategoryAjaxControllerFactory;
use Fundela\FoodApi\Factory\Controller\Ajax\Recipe\FootRecipePartAjaxControllerFactory;
use Fundela\FoodApi\Factory\Controller\Rest\Raw\FoodRawPartRestControllerFactory;
use Fundela\FoodApi\Factory\Controller\Rest\Ingrdnt\FoodIngrdntRestControllerFactory;
use Fundela\FoodApi\Factory\Controller\Rest\Recipe\FoodRecipePartRestControllerFactory;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

/**
 * Naming conventions:
 * REST URLs start with 'api'
 * AJAX URLs start with 'apix'
 */
return [
    'router' => [
        'routes' => [
            /*
             * ####### REST #######
             */
            'fundela_foodapi_rest_raw_foodrawpart' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api-food-rawpart[/:id]',
                    'constraints' => [
                        'id' => '[0-9A-Fa-f-]*',
                    ],
                    'defaults' => [
                        'controller' => FoodRawPartRestController::class,
                    ],
                ],
            ],
            'fundela_foodapi_rest_ingrdnt_foodingrdnt' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api-food-ingrdnt[/:id]',
                    'constraints' => [
                        'id' => '[0-9]*',
                    ],
                    'defaults' => [
                        'controller' => FoodIngrdntRestController::class,
                    ],
                ],
            ],
            'fundela_foodapi_rest_recipe_foodrecipepart' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/api-food-recipe-part[/:id][/]',
                    'constraints' => [
                        'id' => '[0-9a-zA-Z-]*',
                    ],
                    'defaults' => [
                        'controller' => FoodRecipePartRestController::class,
                    ],
                ],
            ],
            /*
             * ####### AJAX #######
             */
            'fundela_foodapi_ajax_ingrdnt_foodingrdnt_updateorderpriority' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/apix-food-ingrdnt-update-orderpriority',
                    'defaults' => [
                        'controller' => FoodIngrdntAjaxController::class,
                        'action' => 'updateOrderPriority'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_raw_foodrawpart_updatequantity' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/apix-food-raw-part-update-quantity',
                    'defaults' => [
                        'controller' => FoodRawPartAjaxController::class,
                        'action' => 'updateQuantity'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_raw_foodraw_categoryassoc' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/apix-food-raw-category-assoc',
                    'defaults' => [
                        'controller' => FoodRawAjaxController::class,
                        'action' => 'foodRawCategoryIdAssoc'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_raw_foodraw_search' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/apix-food-raw-search',
                    'defaults' => [
                        'controller' => FoodRawAjaxController::class,
                        'action' => 'search'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_recipe_foodrecipe_categoryassoc' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/apix-food-recipe-category-assoc',
                    'defaults' => [
                        'controller' => FoodRecipeCategoryAjaxController::class,
                        'action' => 'foodRecipeCategoryIdAssoc'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_recipe_foodrecipe_recipecategories' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/apix-food-recipe-categories[/:recipe_uuid]',
                    'constraints' => [
                        'recipe_uuid' => '[0-9a-fA-F-]*',
                    ],
                    'defaults' => [
                        'controller' => FoodRecipeCategoryAjaxController::class,
                        'action' => 'foodRecipeCategories'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_recipe_foodrecipe_recipecategoryadd' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/apix-food-recipe-category-insert/:recipe_uuid/:cat_id',
                    'constraints' => [
                        'recipe_uuid' => '[0-9a-fA-F-]*',
                        'cat_id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => FoodRecipeCategoryAjaxController::class,
                        'action' => 'foodRecipeCategoryAdd'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_recipe_foodrecipe_recipecategoryremove' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/apix-food-recipe-category-delete/:rel_id',
                    'constraints' => [
                        'rel_id' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => FoodRecipeCategoryAjaxController::class,
                        'action' => 'foodRecipeCategoryDelete'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_image_recipeimage_uploadimage' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/apix-food-recipe-upload-image',
                    'defaults' => [
                        'controller' => RecipeImageAjaxController::class,
                        'action' => 'uploadImage'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_image_recipeimage_recipeimages' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/apix-food-recipe-images[/:recipe_uuid]',
                    'constraints' => [
                        'recipe_uuid' => '[0-9a-fA-F-]*',
                    ],
                    'defaults' => [
                        'controller' => RecipeImageAjaxController::class,
                        'action' => 'recipeImages'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_image_recipeimage_deleteimage' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/apix-food-recipe-delete-image/:recipeUuid/:imageId',
                    'constraints' => [
                        'recipeUuid' => '[0-9a-fA-F-]*',
                        'imageId' => '[0-9]+',
                    ],
                    'defaults' => [
                        'controller' => RecipeImageAjaxController::class,
                        'action' => 'deleteImage'
                    ],
                ],
            ],
            'fundela_foodapi_ajax_recipe_recipepart_updateorderpriority' => [
                'type' => Segment::class,
                'options' => [
                    'route' => '/apix-food-recipepart-orderpriority[/:partUuids]',
                    'constraints' => [
                        'partUuids' => '[0-9a-fA-F,-]*',
                    ],
                    'defaults' => [
                        'controller' => FoodRecipePartAjaxController::class,
                        'action' => 'updateOrderPriority'
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            FoodRawPartRestController::class => FoodRawPartRestControllerFactory::class,
            FoodIngrdntRestController::class => FoodIngrdntRestControllerFactory::class,
            FoodRecipePartRestController::class => FoodRecipePartRestControllerFactory::class,
            // AJAX
            FoodIngrdntAjaxController::class => FoodIngrdntAjaxControllerFactory::class,
            FoodRawPartAjaxController::class => FoodRawPartAjaxControllerFactory::class,
            FoodRecipePartAjaxController::class => FootRecipePartAjaxControllerFactory::class,
            FoodRawAjaxController::class => FoodRawAjaxControllerFactory::class,
            FoodRecipeCategoryAjaxController::class => FoodRecipeCategoryAjaxControllerFactory::class,
            RecipeImageAjaxController::class => RecipeImageAjaxControllerFactory::class,
        ],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [],
        'invokables' => [],
    ],
    'view_helpers' => [
        'factories' => [],
        'invokables' => [],
        'aliases' => [],
    ],
    'view_manager' => [
        'template_map' => [],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
];
