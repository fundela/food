<?php

namespace Fundela\Mail\Mail\Draft;

use Laminas\Log\Logger;
use Laminas\View\Model\ViewModel;
use Laminas\View\Renderer\PhpRenderer;

class DraftRenderer
{

    protected Logger $logger;

    const DEFAULT_DRAFT_TEMPLATE = 'draft/default';

    private PhpRenderer $renderer;

    private string $template;

    private string $content = '';

    private array $viewVariables = [];

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function setTemplate(string $template)
    {
        $this->template = $template;
    }

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    public function setRenderer(PhpRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function setViewVariable(string $key, $value)
    {
        $this->viewVariables[$key] = $value;
    }

    public function setViewVariables(array $viewVariables)
    {
        $this->viewVariables = array_merge($this->viewVariables, $viewVariables);
    }

    /**
     * The default template requires two viewVariables: salutation, content.
     * @return string
     */
    public function render()
    {
        $viewModel = new ViewModel();
        $viewModel->setVariable('content', $this->content);

        if (isset($this->template)) {
            $viewModel->setTemplate($this->template);
        } else {
            $viewModel->setTemplate(self::DEFAULT_DRAFT_TEMPLATE);
        }
        if (!empty($this->viewVariables)) {
            $viewModel->setVariables($this->viewVariables);
        }
        return $this->renderer->render($viewModel);
    }

}
