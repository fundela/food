<?php

namespace Fundela\Mail\Mail;

use Laminas\Log\Logger;
use Laminas\Mail\Transport\Sendmail;
use Laminas\Mail\Transport\Smtp;
use Laminas\Mail\Transport\SmtpOptions;
use Laminas\Mime\Part;
use Laminas\Mime\Mime;
use Laminas\Mime\Message as MimeMessage;
use Laminas\Mail\Message as MailMessage;

class MailWrapper
{

    protected Logger $logger;

    protected string $message = '';

    protected array $mailConfig;

    const FROM_ADMIN = 1;
    const FROM_BRAND = 2;

    protected $textPlain;
    protected $textHtml;
    protected $toName;
    protected $toEmail;

    protected array $BccEmails;
    protected $fromName;
    protected $fromEmail;
    protected $subject;

    protected bool $onlyHtml = false;

    public function setLogger(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function resetMailContent()
    {
        $this->textPlain = '';
        $this->textHtml = '';
    }

    public function resetMailRecipient()
    {
        $this->resetMailContent();
        $this->toName = '';
        $this->toEmail = '';
        $this->BccEmails = [];
    }

    public function resetMailAll()
    {
        $this->resetMailContent();
        $this->toName = '';
        $this->toEmail = '';
        $this->BccEmails = [];
        $this->fromName = '';
        $this->fromEmail = '';
        $this->subject = '';
    }

    /**
     * Set from Module.php
     * @param array $mailConfig
     */
    public function setMailConfig(array $mailConfig)
    {
        $this->mailConfig = $mailConfig;
    }

    public function setTextPlain($textPlain)
    {
        $this->textPlain = str_replace('<br>', "\r\n", $textPlain);
    }

    public function setTextHtml($textHtml)
    {
        $this->textHtml = $textHtml;
    }

    public function setToName($toName)
    {
        $this->toName = $toName;
    }

    public function setToEmail($toEmail)
    {
        $this->toEmail = $toEmail;
    }

    function setBccEmails(array $BccEmails)
    {
        $this->BccEmails = $BccEmails;
    }

    public function setFromName(string $fromName)
    {
        $this->fromName = $fromName;
    }

    public function setFromEmail(string $fromEmail)
    {
        $this->fromEmail = $fromEmail;
    }

    public function setFromAdmin()
    {
        $this->fromName = $this->mailConfig['admin_mail']['name_from'];
        $this->fromEmail = $this->mailConfig['admin_mail']['address_from'];
    }

    public function setFromBrand()
    {
        $this->fromName = $this->mailConfig['brand_mail']['name_from'];
        $this->fromEmail = $this->mailConfig['brand_mail']['address_from'];
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    protected function makeMailParts(): array
    {
        if (empty($this->mailConfig)) {
            return [];
        }
        $parts = [];
        if (!empty($this->textPlain)) {
            $textPart = new Part($this->textPlain);
            $textPart->type = Mime::TYPE_TEXT;
            $textPart->charset = 'utf-8';
            $parts[] = $textPart;
        } else if (!empty($this->textHtml) && !$this->onlyHtml) {
            $this->textPlain = $this->html2text($this->textHtml);
            $textPart = new Part($this->textPlain);
            $textPart->type = Mime::TYPE_TEXT;
            $textPart->charset = 'utf-8';
            $parts[] = $textPart;
        }

        if (!empty($this->textHtml)) {
            $htmlPart = new Part($this->textHtml);
            $htmlPart->type = Mime::TYPE_HTML;
            $htmlPart->charset = 'utf-8';
            $parts[] = $htmlPart;
        }
        return $parts;
    }

    protected function makeMailMessage(MimeMessage $mimeMessage): MailMessage
    {
        $mail = new MailMessage();
        $mail->setEncoding('UTF-8');
        $mail->setFrom($this->fromEmail, $this->fromName);
        $mail->addTo($this->toEmail, $this->toName);
        if (!empty($this->BccEmails)) {
            foreach ($this->BccEmails as $bccEmail) {
                $mail->addBcc($bccEmail, $bccEmail);
            }
        }
        $mail->setSubject($this->subject);
        $mail->setBody($mimeMessage);
        return $mail;
    }

    /**
     *
     * @return int 1 on success, -1 on error
     */
    public function sendMail(): int
    {
        if (empty($parts = $this->makeMailParts())) {
            return -1;
        }
        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts($parts);
        $mail = $this->makeMailMessage($mimeMessage);

        // If it has two parts (HTML + plain), we have to force the correct MIME
        // alternative allows the MUA to choose between HTML and plain text
        // By default it sets multipart/mixed, which makes e.g. thunderbird display both
        if ($this->textPlain && ($this->textHtml && !$this->onlyHtml)) {
            $mail->getHeaders()->get('content-type')->setType('multipart/alternative');
        }

        return $this->send($mail);
    }

    /**
     *
     * @param array $filePathAndType e.g.: array('/path/to/document.pdf' => 'application/pdf', '/path/to/me.jpg' => 'image/jpg')
     * @return int 1 on success
     */
    public function sendMailWithAttachment(array $filePathAndType): int
    {
        if (empty($parts = $this->makeMailParts())) {
            return -1;
        }

        $content = new MimeMessage();
        $content->setParts($parts);
        $contentPart = new Part($content->generateMessage());
        $contentPart->type = 'multipart/alternative;' . PHP_EOL . ' boundary="' . $content->getMime()->boundary() . '"';

        $attachmentParts = [];
        foreach ($filePathAndType as $filePath => $type) {
            if (!file_exists($filePath)) {
                throw new \RuntimeException('$filePath: ' . $filePath . ' does not exist or can not read');
            }
            $fileContent = fopen($filePath, 'r');
            $attachment = new Part($fileContent);
            $attachment->type = $type;
            $filePathExpl = explode(DIRECTORY_SEPARATOR, $filePath);
            $attachment->filename = array_pop($filePathExpl);
            $attachment->disposition = Mime::DISPOSITION_ATTACHMENT;
            $attachment->encoding = Mime::ENCODING_BASE64; // fehlt in ZF2 Doku, ohne ist das PDF etc. leer (unlesbar)!!!
            $attachmentParts[] = $attachment;
        }

        $mimeMessage = new MimeMessage();
        $mimeMessage->setParts(array_merge([$contentPart], $attachmentParts));

        $mail = $this->makeMailMessage($mimeMessage);
        $mail->getHeaders()->get('content-type')->setType('multipart/related');

        return $this->send($mail);
    }

    /**
     *
     * @param MailMessage $mail
     * @return int
     * @todo Use more than on authentication method in config (e.g. also TLS)
     */
    private function send(MailMessage $mail): int
    {
        if ($this->mailConfig['is_smtp']) {
            $transport = new Smtp();
            $optionArray = [
                'name' => $this->mailConfig['smtp_from_servername'],
                'host' => $this->mailConfig['smtp_host'],
                'port' => $this->mailConfig['smtp_port']
            ];
            if (!empty($this->mailConfig['smtp_user'])) {
                $optionArray = array_merge($optionArray,
                    [
                        'connection_class' => $this->mailConfig['connection_class'],
                        'connection_config' => [
                            'username' => $this->mailConfig['smtp_user'],
                            'password' => $this->mailConfig['smtp_passwd'],
                            'ssl' => $this->mailConfig['ssl']
                        ]
                    ]);
            }
            $smtpOptions = new SmtpOptions($optionArray);
            $transport->setOptions($smtpOptions);
            try {
                $transport->send($mail);
            } catch (\RuntimeException $ex) {
                $this->logger->err($ex->getMessage());
                $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
                $this->logger->err('host: ' . $this->mailConfig['smtp_host']);
                return -1;
            }
        } else {
            $transport = new Sendmail();
            try {
                $transport->send($mail);
            } catch (\RuntimeException $ex) {
                $this->message = $ex->getMessage();
                $this->logger->err($ex->getMessage());
                $this->logger->err($ex->getFile() . ' at line: ' . $ex->getLine());
                return -1;
            }
        }
        return 1;
    }

    public function setOnlyHtml(bool $onlyHtml)
    {
        $this->onlyHtml = $onlyHtml;
    }

    /**
     *
     * @param string $textHtml HTML String
     * @return string Plain Text String
     * @throws \RuntimeException
     */
    private function html2text($textHtml): string
    {
        if (strlen(exec('whereis html2text')) < 15) {
            throw new \RuntimeException('There is no html2text installed');
        }
        if (!isset($this->mailConfig['tmp_folder'])) {
            throw new \RuntimeException(__NAMESPACE__ . ' tmp_folder is not set.');
        }
        if (!file_exists($this->mailConfig['tmp_folder'])) {
            throw new \RuntimeException('Folder does not exist: ' . $this->mailConfig['tmp_folder']);
        }
        $now = time();
        $fileIn = $this->mailConfig['tmp_folder'] . '/html2text' . $now;
        $fileOut = $this->mailConfig['tmp_folder'] . '/html2textout' . $now;
        file_put_contents($fileIn, $textHtml);
        exec('html2text ' . $fileIn . ' >> ' . $fileOut);
        $text = file_get_contents($fileOut);
        unlink($fileIn);
        unlink($fileOut);
        return $text;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
