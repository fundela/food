<?php

namespace Fundela\Mail\Mail;

use Laminas\Log\Logger;

/**
 * Simple class to send Emails
 * @author bitkorn
 */
class SimpleMailer
{
    protected Logger $logger;

    private MailWrapper $mailWrapper;

    private array $config;

    protected string $message = '';

    public function __construct(MailWrapper $mailWrapper, array $config, Logger $logger)
    {
        $this->mailWrapper = $mailWrapper;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param string $mailText <br> is replaced with \r\n
     * @param string $subject
     * @return bool
     */
    public function sendAdminEmail(string $mailText, string $subject = 'Admin Email'): bool
    {
        $this->mailWrapper->setTextHtml($mailText);
        $this->mailWrapper->setTextPlain($mailText);
        $this->mailWrapper->setFromEmail($this->config['admin_mail']['address_from']);
        $this->mailWrapper->setFromName($this->config['admin_mail']['name_from']);
        $this->mailWrapper->setToEmail($this->config['admin_mail']['address_to']);
        $this->mailWrapper->setToName($this->config['admin_mail']['name_to']);
        $this->mailWrapper->setSubject($subject);
        $success = false;
        try {
            $success = $this->mailWrapper->sendMail() == 1;
        } catch (\Exception $e) {
            $this->message = $e->getMessage();
            $this->logger->err($e->getMessage());
        }
        return $success;
    }

    /**
     * @param string $toEmail
     * @param string $toName
     * @param string $subject
     * @param string $mailText
     * @param int $from MailWrapper::FROM_ADMIN | MailWrapper::FROM_BRAND
     * @return bool
     */
    public function sendEmailTo(string $toEmail, string $toName, string $subject, string $mailText, int $from): bool
    {
        $this->mailWrapper->setTextHtml($mailText);
        $this->mailWrapper->setTextPlain($mailText);
        switch ($from) {
            case MailWrapper::FROM_ADMIN:
                $this->mailWrapper->setFromEmail($this->config['admin_mail']['address_from']);
                $this->mailWrapper->setFromName($this->config['admin_mail']['name_from']);
                break;
            case MailWrapper::FROM_BRAND:
                $this->mailWrapper->setFromEmail($this->config['brand_mail']['address_from']);
                $this->mailWrapper->setFromName($this->config['brand_mail']['name_from']);
                break;
            default:
                return false;
        }
        $this->mailWrapper->setToEmail($toEmail);
        $this->mailWrapper->setToName($toName);
        $this->mailWrapper->setSubject($subject);
        $success = false;
        try {
            $success = $this->mailWrapper->sendMail() == 1;
        } catch (\Exception $e) {
            $this->logger->err($e->getMessage());
            $this->message = $this->mailWrapper->getMessage();
        }
        return $success;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
