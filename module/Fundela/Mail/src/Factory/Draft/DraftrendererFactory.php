<?php

namespace Fundela\Mail\Factory\Draft;

use Interop\Container\ContainerInterface;
use Interop\Container\Exception\ContainerException;
use Fundela\Mail\Mail\Draft\DraftRenderer;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\HelperPluginManager;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplateMapResolver;

class DraftrendererFactory implements FactoryInterface
{

    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service.
     * @throws ServiceNotCreatedException if an exception is raised when creating a service.
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $draftManager = new DraftRenderer();
        $draftManager->setLogger($container->get('logger'));
        $renderer = new PhpRenderer();
        $resolver = new TemplateMapResolver();
        $resolver->setMap($container->get('ViewTemplateMapResolver'));
        $renderer->setResolver($resolver);
        /** @var HelperPluginManager $helperPluginManager */
        $helperPluginManager = $container->get('ViewHelperManager');
        $renderer->setHelperPluginManager($helperPluginManager);
        $draftManager->setRenderer($renderer);
        return $draftManager;
    }
}
