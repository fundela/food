<?php

namespace Fundela\Mail;

use Fundela\Mail\Factory\Draft\DraftrendererFactory;
use Fundela\Mail\Factory\MailWrapperFactory;
use Fundela\Mail\Factory\SimpleMailerFactory;
use Fundela\Mail\Mail\MailWrapper;
use Fundela\Mail\Mail\SimpleMailer;
use Fundela\Mail\Mail\Draft\DraftRenderer;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
        ],
    ],
    'controllers' => [
        'factories' => [
        ],
    ],
    'service_manager' => [
        'abstract_factories' => [],
        'factories' => [
            MailWrapper::class => MailWrapperFactory::class,
            SimpleMailer::class => SimpleMailerFactory::class,
            DraftRenderer::class => DraftrendererFactory::class
        ],
    ],
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
        'template_map' => [
            'draft/default' => __DIR__ . '/../view/draft/default.phtml',
        ],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    'fundela_mail' => [
        /**
         * https://docs.laminas.dev/laminas-mail/transport/smtp-options
         */
        'is_smtp' => true,
        'smtp_host' => 'bitkorn.de', // defaults to "127.0.0.1"
        'smtp_port' => 25, // defaults to "25": 25, 465 (SSL/TLS), 587 (MSA/STARTTLS)
        'connection_class' => 'login', // plain | login | crammd5 (\Laminas\Mail\Protocol\Smtp\Auth\*) ...NTLM !?!?!?
        'smtp_user' => 'mail@bitkorn.de',
        'smtp_passwd' => 'mysecret',
        'smtp_from_servername' => 'bitkorn.de', // real network Host name
        'ssl' => null, // ssl | tls
        'admin_mail' => [
            'address_to' => 'mail@bitkorn.de',
            'name_to' => 'bitkorn',
            'address_from' => 'mail@bitkorn.de',
            'name_from' => 'admin'
        ],
        'brand_mail' => [
            'address_to' => 'mail@kochrezepte.online',
            'name_to' => 'KochRezepte.online',
            'address_from' => 'mail@kochrezepte.online',
            'name_from' => 'KochRezepte.online'
        ],
        'tmp_folder' => __DIR__ . '/../../../../data/tmp'
    ],
];
