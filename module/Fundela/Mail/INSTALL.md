```shell script
composer require laminas/laminas-mail
```

Folder ```/data/tmp``` must exist and must be writable for the webserver user.

html2text for plaintext Emails:
```shell script
sudo apt install html2text
```
