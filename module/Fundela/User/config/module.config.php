<?php

namespace Fundela\User;

use Fundela\User\Factory\Form\UserFormFactory;
use Fundela\User\Factory\View\IsUserInRoleFactory;
use Fundela\User\Factory\View\UserloginViewHelperFactory;
use Fundela\User\Form\UserForm;
use Fundela\User\View\IsUserInRole;
use Fundela\User\View\UserloginViewHelper;
use Laminas\Session\Container;
use Laminas\Session\Storage\SessionArrayStorage;

return [
    'router' => [
        'routes' => [],
    ],
    'controllers' => [
        'factories' => [],
        'invokables' => [],
    ],
    'service_manager' => [
        'factories' => [
            Service\UserService::class => Factory\Service\UserServiceFactory::class,
            // table
            Table\UserTable::class => Factory\Table\UserTableFactory::class,
            // form
            UserForm::class => UserFormFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helpers' => [
        'factories' => [
            IsUserInRole::class => IsUserInRoleFactory::class,
            UserloginViewHelper::class => UserloginViewHelperFactory::class,
        ],
        'invokables' => [],
        'aliases' => [
            'isUserInRole' => IsUserInRole::class,
            'userLogin' => UserloginViewHelper::class,
        ],
    ],
    'view_manager' => [
        'template_map' => [
            'draft/verify-email' => __DIR__ . '/../view/draft/verify-email.phtml',
        ],
        'template_path_stack' => [],
        'strategies' => [
            'ViewJsonStrategy',
        ],
    ],
    /**
     * https://docs.laminas.dev/laminas-session/application-integration/usage-in-a-laminas-mvc-application/
     */
    'session_containers' => [
        Container::class,
    ],
    'session_storage' => [
        'type' => SessionArrayStorage::class,
    ],
    'session_config'  => [
        'gc_maxlifetime' => 7200,
    ],
];
