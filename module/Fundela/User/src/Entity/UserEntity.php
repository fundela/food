<?php

namespace Fundela\User\Entity;

use Fundela\Trinket\Entity\AbstractEntity;

class UserEntity extends AbstractEntity
{
    const MESSAGE_SEND_VERIFYEMAIL = ['level' => 'info', 'text' => 'Es wurde eine Email, zum Bestätigen deiner Emailadresse versendet'];
    const MESSAGE_REGISTER_VERIFY_SUCCESS = ['level' => 'success', 'text' => 'Dein Account ist jetzt aktiv'];

    protected string $primaryKey = 'user_uuid';

	public array $mapping = [
		'user_uuid' => 'userUuid',
		'user_email' => 'userEmail',
		'user_login' => 'userLogin',
		'user_passwd' => 'userPasswd',
		'user_active' => 'userActive',
		'user_registration_hash' => 'userRegistrationHash',
		'user_forgot_passwd_hash' => 'userForgotPasswdHash',
		'user_lang_iso' => 'userLangIso',
		'user_time_create' => 'userTimeCreate',
		'user_time_update' => 'userTimeUpdate',
		'user_role_id' => 'userRoleId',
        // user_role
        'user_role_alias' => 'userRoleAlias',
        'user_role_desc' => 'userRoleDesc',
        'user_role_route' => 'userRoleRoute',
	];


	/**
	 * @return string
	 */
	public function getUserUuid()
	{
		if(!isset($this->storage['user_uuid'])) {
			return '';
		}
		return $this->storage['user_uuid'];
	}


	/**
	 * @return string
	 */
	public function getUserEmail()
	{
		if(!isset($this->storage['user_email'])) {
			return '';
		}
		return $this->storage['user_email'];
	}


	/**
	 * @return string
	 */
	public function getUserLogin()
	{
		if(!isset($this->storage['user_login'])) {
			return '';
		}
		return $this->storage['user_login'];
	}


	/**
	 * @return string
	 */
	public function getUserPasswd()
	{
		if(!isset($this->storage['user_passwd'])) {
			return '';
		}
		return $this->storage['user_passwd'];
	}


	/**
	 * @return bool
	 */
	public function getUserActive()
	{
		if(!isset($this->storage['user_active'])) {
			return '';
		}
		return $this->storage['user_active'];
	}


	public function getUserRegistrationHash()
	{
		if(!isset($this->storage['user_registration_hash'])) {
			return '';
		}
		return $this->storage['user_registration_hash'];
	}


	public function getUserForgotPasswdHash()
	{
		if(!isset($this->storage['user_forgot_passwd_hash'])) {
			return '';
		}
		return $this->storage['user_forgot_passwd_hash'];
	}


	/**
	 * @return string
	 */
	public function getUserLangIso()
	{
		if(!isset($this->storage['user_lang_iso'])) {
			return '';
		}
		return $this->storage['user_lang_iso'];
	}


	/**
	 * @return string
	 */
	public function getUserTimeCreate()
	{
		if(!isset($this->storage['user_time_create'])) {
			return '';
		}
		return $this->storage['user_time_create'];
	}


	/**
	 * @return string
	 */
	public function getUserTimeUpdate()
	{
		if(!isset($this->storage['user_time_update'])) {
			return '';
		}
		return $this->storage['user_time_update'];
	}


	/**
	 * @return int
	 */
	public function getUserRoleId()
	{
		if(!isset($this->storage['user_role_id'])) {
			return 0;
		}
		return $this->storage['user_role_id'];
	}
}
