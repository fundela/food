<?php

namespace Fundela\User\View;

use Fundela\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

class IsUserInRole extends AbstractHelper
{
    protected UserService $userService;

    /**
     * @param UserService $userService
     */
    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * @param int $roleId
     * @param bool $min
     * @return bool
     */
    public function __invoke(int $roleId, bool $min = true): bool
    {
        return $this->userService->isUserInRole($roleId, $min);
    }
}
