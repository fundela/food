<?php

namespace Fundela\User\View;

use Fundela\User\Service\UserService;
use Laminas\Log\Logger;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

class UserloginViewHelper extends AbstractHelper
{
    protected UserService $userService;

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

	/**
	 * @return string
	 */
	public function __invoke()
	{
		return $this->userService->getUserLogin();
	}
}
