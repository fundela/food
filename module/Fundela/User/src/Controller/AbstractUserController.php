<?php

namespace Fundela\User\Controller;

use Fundela\Trinket\Controller\AbstractHtmlController;
use Fundela\User\Service\UserService;

class AbstractUserController extends AbstractHtmlController
{

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService
     */
    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

}
