<?php

namespace Fundela\User\Controller;

use Fundela\Trinket\Controller\AbstractRestController;
use Fundela\User\Service\UserService;

class AbstractUserRestController extends AbstractRestController
{

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService
     */
    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

}
