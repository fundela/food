<?php

namespace Fundela\User\Form;

use Fundela\Trinket\Filter\SanitizeStringFilter;
use Fundela\Trinket\Form\AbstractForm;
use Laminas\Db\Adapter\Adapter;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Email;
use Laminas\Form\Element\Password;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\I18n\Validator\Alnum;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\NoRecordExists;
use Laminas\Validator\EmailAddress;
use Laminas\Validator\Identical;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class UserForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $langIsoAssoc = [];

    protected Adapter $adapter;
    /**
     * @param array $langIsoAssoc
     */
    public function setLangIsoAssoc(array $langIsoAssoc): void
    {
        foreach ($langIsoAssoc as $l) {
            $this->langIsoAssoc[$l] = $l;
        }
    }

    /**
     * @param Adapter $adapter
     */
    public function setAdapter(Adapter $adapter): void
    {
        $this->adapter = $adapter;
    }

    public function init()
    {
        $this->setAttribute('class', 'w3-container');

        if ($this->primaryKeyAvailable) {
            $this->add([
                'type' => Text::class,
                'name' => 'user_uuid',
            ]);
        }
        $this->add([
            'type' => Email::class,
            'name' => 'user_email',
            'options' => [
                'label' => 'Email',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'user_login',
            'options' => [
                'label' => 'Username/Login',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Password::class,
            'name' => 'user_passwd',
            'options' => [
                'label' => 'Passwort',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Password::class,
            'name' => 'user_passwd_confirm',
            'options' => [
                'label' => 'Passwort wiederholen',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Select::class,
            'name' => 'user_lang_iso',
            'options' => [
                'label' => 'Sprache',
                'value_options' => $this->langIsoAssoc,
            ],
            'attributes' => [
                'class' => 'w3-select',
            ]
        ]);
        $this->add([
            'type' => Submit::class,
            'attributes' => [
                'value' => 'Submit',
                'class' => 'w3-button w3-blue-gray'
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['user_uuid'] = [
                'required' => true,
                'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['user_email'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => EmailAddress::class,
                ],
                [
                    'name' => NoRecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table'   => 'user',
                        'field'   => 'user_email',
                    ]
                ],
            ]
        ];

        $filter['user_login'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => Alnum::class,
                ],
                [
                    'name' => NoRecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table'   => 'user',
                        'field'   => 'user_login',
                    ]
                ],
            ]
        ];

        $filter['user_passwd'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class]
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 8,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['user_passwd_confirm'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class]
            ],
            'validators' => [
                [
                    'name' => Identical::class,
                    'options' => [
                        'token' => 'user_passwd',
                    ],
                ]
            ]
        ];

        $filter['user_lang_iso'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->langIsoAssoc),
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
