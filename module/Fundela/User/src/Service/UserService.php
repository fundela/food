<?php

namespace Fundela\User\Service;

use Fundela\Mail\Mail\Draft\DraftRenderer;
use Fundela\Mail\Mail\MailWrapper;
use Fundela\Mail\Mail\SimpleMailer;
use Fundela\Trinket\Service\AbstractService;
use Fundela\Trinket\Table\ConfigTable;
use Fundela\User\Entity\UserEntity;
use Fundela\User\Table\UserTable;
use Laminas\Session\Container;

class UserService extends AbstractService
{

    protected Container $userContainer;
    protected UserTable $userTable;
    protected UserEntity $user;
    protected SimpleMailer $simpleMailer;
    protected DraftRenderer $draftRenderer;
    protected ConfigTable $configTable;

    public function setUserTable(UserTable $userTable): void
    {
        $this->userTable = $userTable;
    }

    public function setSimpleMailer(SimpleMailer $simpleMailer): void
    {
        $this->simpleMailer = $simpleMailer;
    }

    public function setDraftRenderer(DraftRenderer $draftRenderer): void
    {
        $this->draftRenderer = $draftRenderer;
    }

    public function setConfigTable(ConfigTable $configTable): void
    {
        $this->configTable = $configTable;
    }

    /**
     * @param string $username
     * @param string $passwd
     * @param string $email
     * @param string $lang
     * @param bool $active
     * @return string
     */
    public function registerUser(string $username, string $passwd, string $email, string $lang = 'de', bool $active = false): string
    {
        return $this->userTable->insertUser($username, $this->computePasswordHash($passwd), $email, $lang, $active);
    }

    public function sendVerifyEmail(string $email, string $username): bool
    {
        if (empty($brandName = $this->configTable->getConfigValueByKey('brand_name'))) {
            return false;
        }
        $hash = $this->computeRegistrationHashUnique();
        if (!$this->userTable->updateRegistrationHash($hash, $email)) {
            return false;
        }
        $this->draftRenderer->setTemplate('draft/verify-email');
        $this->draftRenderer->setViewVariable('name', $username);
        $this->draftRenderer->setViewVariable('brandName', $brandName);
        $this->draftRenderer->setViewVariable('baseUrl', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http') . "://$_SERVER[HTTP_HOST]");
        $this->draftRenderer->setViewVariable('hash', $hash);
        if (!$this->simpleMailer->sendEmailTo(
            $email, $username
            , $brandName . ' - bitte bestätige deine Emailadresse'
            , $this->draftRenderer->render(), MailWrapper::FROM_BRAND
        )) {
            $this->userTable->updateRegistrationHash('', $email);
            return false;
        }
        return true;
    }

    public function verifyRegistration(string $hash): bool
    {
        return $this->userTable->verifyRegistration($hash);
    }

    public function loginWithUsernamePasswd(string $username, string $passwd): bool
    {
        $userData = $this->userTable->getUser($username, $this->computePasswordHash($passwd));
        if (empty($userData)) {
            return false;
        }
        $this->user = new UserEntity();
        if (!$this->user->exchangeArrayFromDatabase($userData)) {
            unset($this->user);
            return false;
        }
        $this->userContainer = new Container('user');
        $this->userContainer->user = $this->user;
        $this->userContainer->getManager()->regenerateId();
        return true;
    }

    private function computePasswordHash(string $password): string
    {
        return hash('sha512', $password);
    }

    private function computeRegistrationHashUnique(): string
    {
        $hash = hash('sha512', time() . ' with salt and pepper' . rand(10000, 99999));
        if ($this->userTable->existRegistrationHash($hash)) {
            return $this->computeRegistrationHashUnique();
        }
        return $hash;
    }

    public function loggedIn(): bool
    {
        if (!isset($this->userContainer)) {
            $this->userContainer = new Container('user');
        }
        if (isset($this->userContainer->user)) {
            $user = $this->userContainer->user;
            if (!$user instanceof UserEntity) {
                $this->logger->err(__CLASS__ . '()->' . __FUNCTION__ . '() $user is not instanceof UserEntity');
                return false;
            }
            $this->user = $user;
            return true;
        }
        return false;
    }

    public function logout(): void
    {
        if ($this->loggedIn()) {
            $this->userContainer->user = null;
        }
    }

    public function isUserInRole(int $roleId, bool $min = true): bool
    {
        if (!$this->loggedIn()) {
            return false;
        }
        if ($min) {
            return $roleId >= $this->user->getUserRoleId();
        }
        return $roleId = $this->user->getUserRoleId();
    }

    public function getUserUuid(): string
    {
        if (!$this->loggedIn()) {
            return '';
        }
        return $this->user->getUnique();
    }

    public function getUserLogin(): string
    {
        if (!$this->loggedIn()) {
            return '';
        }
        return $this->user->getUserLogin();
    }
}
