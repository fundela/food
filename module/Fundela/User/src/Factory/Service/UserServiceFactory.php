<?php

namespace Fundela\User\Factory\Service;

use Fundela\Mail\Mail\Draft\DraftRenderer;
use Fundela\Mail\Mail\SimpleMailer;
use Fundela\Trinket\Table\ConfigTable;
use Fundela\User\Service\UserService;
use Fundela\User\Table\UserTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new UserService();
        $service->setLogger($container->get('logger'));
        $service->setUserTable($container->get(UserTable::class));
        $service->setSimpleMailer($container->get(SimpleMailer::class));
        $service->setDraftRenderer($container->get(DraftRenderer::class));
        $service->setConfigTable($container->get(ConfigTable::class));
        return $service;
    }
}
