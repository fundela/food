<?php

namespace Fundela\User\Factory\Form;

use Fundela\Trinket\Table\ToolsTable;
use Fundela\User\Form\UserForm;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new UserForm();
        /** @var ToolsTable $toolsTable */
        $toolsTable = $container->get(ToolsTable::class);
        $form->setLangIsoAssoc($toolsTable->getEnumValuesPostgreSQL('enum_supported_lang_iso'));
        $form->setAdapter($container->get('dbDefault'));
        return $form;
    }
}
