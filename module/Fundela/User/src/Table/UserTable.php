<?php

namespace Fundela\User\Table;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class UserTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'user';

    /**
     * @param string $login
     * @param string $passwordHash
     * @param string $email
     * @param string $lang
     * @param bool $active
     * @return string GUID
     */
    public function insertUser(string $login, string $passwordHash, string $email, string $lang, bool $active): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'user_uuid' => $uuid,
                'user_email' => $email,
                'user_login' => $login,
                'user_passwd' => $passwordHash,
                'user_active' => $active,
                'user_lang_iso' => $lang
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $login
     * @param string $passwdHash
     * @param bool $onlyActive
     * @return array
     */
    public function getUser(string $login, string $passwdHash = '', bool $onlyActive = true): array
    {
        $select = $this->sql->select();
        try {
            $select->join('user_role', 'user_role.user_role_id = user.user_role_id',
                ['user_role_alias', 'user_role_desc', 'user_role_route'], Select::JOIN_LEFT);
            $select->where(['user_login' => $login]);
            if (!empty($passwdHash)) {
                $select->where(['user_passwd' => $passwdHash]);
            }
            if ($onlyActive) {
                $select->where(['user_active' => true]);
            }
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $login
     * @return bool
     */
    public function existUserLogin(string $login): bool
    {
        return !empty($this->getUser($login));
    }

    public function existRegistrationHash(string $hash): bool
    {
        /** @var HydratingResultSet $result */
        $result = $this->select(['user_registration_hash' => $hash]);
        if ($result->valid() && $result->count() > 0) {
            return true;
        }
        return false;
    }

    public function updateRegistrationHash(string $hash, string $email): bool
    {
        return $this->update(['user_registration_hash' => $hash], ['user_email' => $email]) > 0;
    }

    public function verifyRegistration(string $hash): bool
    {
        if (!$this->existRegistrationHash($hash)) {
            return false;
        }
        return $this->update(['user_active' => true, 'user_registration_hash' => ''], ['user_registration_hash' => $hash]) > 0;
    }
}
