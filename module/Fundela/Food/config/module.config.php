<?php

namespace Fundela\Food;

use Fundela\Food\Factory\Form\Ingrdnt\FoodIngrdntFormFactory;
use Fundela\Food\Factory\Form\Raw\FoodRawFormFactory;
use Fundela\Food\Factory\Form\Raw\FoodRawPartFormFactory;
use Fundela\Food\Factory\Form\Recipe\FoodRecipeFormFactory;
use Fundela\Food\Factory\Form\Recipe\FoodRecipePartFormFactory;
use Fundela\Food\Factory\Service\Ingrdnt\FoodIngrdntServiceFactory;
use Fundela\Food\Factory\Service\Raw\FoodRawPartServiceFactory;
use Fundela\Food\Factory\Service\Raw\FoodRawServiceFactory;
use Fundela\Food\Factory\Service\Recipe\FoodRecipeImageServiceFactory;
use Fundela\Food\Factory\Service\Recipe\FoodRecipePartServiceFactory;
use Fundela\Food\Factory\Service\Recipe\FoodRecipeServiceFactory;
use Fundela\Food\Factory\Service\Recipe\JSONLDRecipeServiceFactory;
use Fundela\Food\Factory\Table\Ingrdnt\FoodIngrdntTableFactory;
use Fundela\Food\Factory\Table\Raw\FoodRawCategoryRelTableFactory;
use Fundela\Food\Factory\Table\Raw\FoodRawCategoryTableFactory;
use Fundela\Food\Factory\Table\Raw\FoodRawPartTableFactory;
use Fundela\Food\Factory\Table\Raw\FoodRawTableFactory;
use Fundela\Food\Factory\Table\Recipe\FoodRecipeCategoryRelTableFactory;
use Fundela\Food\Factory\Table\Recipe\FoodRecipeCategoryTableFactory;
use Fundela\Food\Factory\Table\Recipe\FoodRecipeImageRelTableFactory;
use Fundela\Food\Factory\Table\Recipe\FoodRecipePartTableFactory;
use Fundela\Food\Factory\Table\Recipe\FoodRecipeTableFactory;
use Fundela\Food\Factory\View\Helper\IsEditAllowedFactory;
use Fundela\Food\Form\Ingrdnt\FoodIngrdntForm;
use Fundela\Food\Form\Raw\FoodRawForm;
use Fundela\Food\Form\Raw\FoodRawPartForm;
use Fundela\Food\Form\Recipe\FoodRecipeForm;
use Fundela\Food\Form\Recipe\FoodRecipePartForm;
use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\Food\Service\Raw\FoodRawPartService;
use Fundela\Food\Service\Raw\FoodRawService;
use Fundela\Food\Service\Recipe\FoodRecipeImageService;
use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Food\Service\Recipe\JSONLDRecipeService;
use Fundela\Food\Table\Ingrdnt\FoodIngrdntTable;
use Fundela\Food\Table\Raw\FoodRawCategoryRelTable;
use Fundela\Food\Table\Raw\FoodRawCategoryTable;
use Fundela\Food\Table\Raw\FoodRawPartTable;
use Fundela\Food\Table\Raw\FoodRawTable;
use Fundela\Food\Table\Recipe\FoodRecipeCategoryRelTable;
use Fundela\Food\Table\Recipe\FoodRecipeCategoryTable;
use Fundela\Food\Table\Recipe\FoodRecipeImageRelTable;
use Fundela\Food\Table\Recipe\FoodRecipePartTable;
use Fundela\Food\Table\Recipe\FoodRecipeTable;
use Fundela\Food\View\Helper\IsEditAllowed;

return [
    'router'             => [
        'routes' => [],
    ],
    'controllers'        => [
        'factories'  => [],
        'invokables' => [],
    ],
    'service_manager'    => [
        'factories'  => [
            // table
            FoodIngrdntTable::class           => FoodIngrdntTableFactory::class,
            FoodRawPartTable::class           => FoodRawPartTableFactory::class,
            FoodRawTable::class               => FoodRawTableFactory::class,
            FoodRecipePartTable::class        => FoodRecipePartTableFactory::class,
            FoodRecipeTable::class            => FoodRecipeTableFactory::class,
            FoodRecipeImageRelTable::class    => FoodRecipeImageRelTableFactory::class,
            FoodRawCategoryTable::class       => FoodRawCategoryTableFactory::class,
            FoodRawCategoryRelTable::class    => FoodRawCategoryRelTableFactory::class,
            FoodRecipeCategoryTable::class    => FoodRecipeCategoryTableFactory::class,
            FoodRecipeCategoryRelTable::class => FoodRecipeCategoryRelTableFactory::class,
            // service
            FoodIngrdntService::class         => FoodIngrdntServiceFactory::class,
            FoodRawService::class             => FoodRawServiceFactory::class,
            FoodRawPartService::class         => FoodRawPartServiceFactory::class,
            FoodRecipeService::class          => FoodRecipeServiceFactory::class,
            FoodRecipePartService::class      => FoodRecipePartServiceFactory::class,
            FoodRecipeImageService::class     => FoodRecipeImageServiceFactory::class,
            JSONLDRecipeService::class        => JSONLDRecipeServiceFactory::class,
            // form
            FoodIngrdntForm::class            => FoodIngrdntFormFactory::class,
            FoodRawForm::class                => FoodRawFormFactory::class,
            FoodRawPartForm::class            => FoodRawPartFormFactory::class,
            FoodRecipeForm::class             => FoodRecipeFormFactory::class,
            FoodRecipePartForm::class         => FoodRecipePartFormFactory::class,
        ],
        'invokables' => [],
    ],
    'view_helper_config' => [
        'factories'  => [
            IsEditAllowed::class => IsEditAllowedFactory::class,
        ],
        'invokables' => [],
        'aliases'    => [
            'editAllowed' => IsEditAllowed::class,
        ],
    ],
    'view_manager'       => [
        'template_map'        => [],
        'template_path_stack' => [],
        'strategies'          => [
            'ViewJsonStrategy',
        ],
    ],
    'fundela_food'       => [
        'item_count_per_page'       => 10,
        'recipe_ingrdnt_id_mapping' => [
            /**
             * key = fieldname of db.food_recipe.food_recipe_ingrdnt_0*
             * value = value of db.food_ingrdnt.food_ingrdnt_id
             */
            'food_recipe_ingrdnt_01' => 4, // Energie
            'food_recipe_ingrdnt_02' => 2, // Fett
            'food_recipe_ingrdnt_03' => 1, // Kohlehydrate
            'food_recipe_ingrdnt_04' => 5, // Eiweis
            'food_recipe_ingrdnt_05' => 3, // Ballaststoffe
        ],
    ],
];
