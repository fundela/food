<?php

namespace Fundela\Food\Entity;

use Fundela\Trinket\Entity\AbstractEntity;

class FoodRecipeEntity extends AbstractEntity
{
    public array $mapping = [
        'food_recipe_uuid' => 'food_recipe_uuid',
        'food_recipe_label' => 'food_recipe_label',
        'food_recipe_alias' => 'food_recipe_alias',
        'food_recipe_instrctn' => 'food_recipe_instrctn',
        'user_uuid' => 'user_uuid',
        'food_recipe_ingrdnt_01' => 'food_recipe_ingrdnt_01',
        'food_recipe_ingrdnt_02' => 'food_recipe_ingrdnt_02',
        'food_recipe_ingrdnt_03' => 'food_recipe_ingrdnt_03',
        'food_recipe_ingrdnt_04' => 'food_recipe_ingrdnt_04',
        'food_recipe_ingrdnt_05' => 'food_recipe_ingrdnt_05',
        'food_recipe_meta_title' => 'food_recipe_meta_title',
        'food_recipe_meta_keywords' => 'food_recipe_meta_keywords',
        'food_recipe_meta_desc' => 'food_recipe_meta_desc',
        'food_recipe_time_create' => 'food_recipe_time_create',
        'food_recipe_time_update' => 'food_recipe_time_update',
        'food_recipe_prep_time' => 'food_recipe_prep_time',
        'food_recipe_cook_time' => 'food_recipe_cook_time',
        'food_recipe_yield' => 'food_recipe_yield',
        'image_id' => 'image_id',
        'image_label' => 'image_label',
        'image_desc' => 'image_desc',
        'image_filename' => 'image_filename',
        'image_ext' => 'image_ext',
        'image_scaling' => 'image_scaling',
        'image_time_create' => 'image_time_create',
    ];

    public function getFoodRecipeUuid(): string
    {
        if (!isset($this->storage['food_recipe_uuid'])) {
            return '';
        }
        return $this->storage['food_recipe_uuid'];
    }

    public function setFoodRecipeUuid(string $foodRecipeUuid): void
    {
        $this->storage['food_recipe_uuid'] = $foodRecipeUuid;
    }

    public function getFoodRecipeLabel(): string
    {
        if (!isset($this->storage['food_recipe_label'])) {
            return '';
        }
        return $this->storage['food_recipe_label'];
    }

    public function setFoodRecipeLabel(string $foodRecipeLabel): void
    {
        $this->storage['food_recipe_label'] = $foodRecipeLabel;
    }

    public function getFoodRecipeAlias(): string
    {
        if (!isset($this->storage['food_recipe_alias'])) {
            return '';
        }
        return $this->storage['food_recipe_alias'];
    }

    public function setFoodRecipeAlias(string $foodRecipeAlias): void
    {
        $this->storage['food_recipe_alias'] = $foodRecipeAlias;
    }

    public function getFoodRecipeInstrctn(): string
    {
        if (!isset($this->storage['food_recipe_instrctn'])) {
            return '';
        }
        return $this->storage['food_recipe_instrctn'];
    }

    public function setFoodRecipeInstrctn(string $foodRecipeInstrctn): void
    {
        $this->storage['food_recipe_instrctn'] = $foodRecipeInstrctn;
    }

    public function getUserUuid(): string
    {
        if (!isset($this->storage['user_uuid'])) {
            return '';
        }
        return $this->storage['user_uuid'];
    }

    public function setUserUuid(string $userUuid): void
    {
        $this->storage['user_uuid'] = $userUuid;
    }

    public function getFoodRecipeIngrdnt01(): float
    {
        if (!isset($this->storage['food_recipe_ingrdnt_01'])) {
            return 0;
        }
        return $this->storage['food_recipe_ingrdnt_01'];
    }

    public function setFoodRecipeIngrdnt01(float $foodRecipeIngrdnt01): void
    {
        $this->storage['food_recipe_ingrdnt_01'] = $foodRecipeIngrdnt01;
    }

    public function getFoodRecipeIngrdnt02(): float
    {
        if (!isset($this->storage['food_recipe_ingrdnt_02'])) {
            return 0;
        }
        return $this->storage['food_recipe_ingrdnt_02'];
    }

    public function setFoodRecipeIngrdnt02(float $foodRecipeIngrdnt02): void
    {
        $this->storage['food_recipe_ingrdnt_02'] = $foodRecipeIngrdnt02;
    }

    public function getFoodRecipeIngrdnt03(): float
    {
        if (!isset($this->storage['food_recipe_ingrdnt_03'])) {
            return 0;
        }
        return $this->storage['food_recipe_ingrdnt_03'];
    }

    public function setFoodRecipeIngrdnt03(float $foodRecipeIngrdnt03): void
    {
        $this->storage['food_recipe_ingrdnt_03'] = $foodRecipeIngrdnt03;
    }

    public function getFoodRecipeIngrdnt04(): float
    {
        if (!isset($this->storage['food_recipe_ingrdnt_04'])) {
            return 0;
        }
        return $this->storage['food_recipe_ingrdnt_04'];
    }

    public function setFoodRecipeIngrdnt04(float $foodRecipeIngrdnt04): void
    {
        $this->storage['food_recipe_ingrdnt_04'] = $foodRecipeIngrdnt04;
    }

    public function getFoodRecipeIngrdnt05(): float
    {
        if (!isset($this->storage['food_recipe_ingrdnt_05'])) {
            return 0;
        }
        return $this->storage['food_recipe_ingrdnt_05'];
    }

    public function setFoodRecipeIngrdnt05(float $foodRecipeIngrdnt05): void
    {
        $this->storage['food_recipe_ingrdnt_05'] = $foodRecipeIngrdnt05;
    }

    public function getFoodRecipeMetaTitle(): string
    {
        if (!isset($this->storage['food_recipe_meta_title'])) {
            return '';
        }
        return $this->storage['food_recipe_meta_title'];
    }

    public function setFoodRecipeMetaTitle(string $foodRecipeMetaTitle): void
    {
        $this->storage['food_recipe_meta_title'] = $foodRecipeMetaTitle;
    }

    public function getFoodRecipeMetaKeywords(): string
    {
        if (!isset($this->storage['food_recipe_meta_keywords'])) {
            return '';
        }
        return $this->storage['food_recipe_meta_keywords'];
    }

    public function setFoodRecipeMetaKeywords(string $foodRecipeMetaKeywords): void
    {
        $this->storage['food_recipe_meta_keywords'] = $foodRecipeMetaKeywords;
    }

    public function getFoodRecipeMetaDesc(): string
    {
        if (!isset($this->storage['food_recipe_meta_desc'])) {
            return '';
        }
        return $this->storage['food_recipe_meta_desc'];
    }

    public function setFoodRecipeMetaDesc(string $foodRecipeMetaDesc): void
    {
        $this->storage['food_recipe_meta_desc'] = $foodRecipeMetaDesc;
    }

    public function getFoodRecipeTimeCreate(): string
    {
        if (!isset($this->storage['food_recipe_time_create'])) {
            return '';
        }
        return $this->storage['food_recipe_time_create'];
    }

    public function setFoodRecipeTimeCreate(string $foodRecipeTimeCreate): void
    {
        $this->storage['food_recipe_time_create'] = $foodRecipeTimeCreate;
    }

    public function getFoodRecipeTimeUpdate(): string
    {
        if (!isset($this->storage['food_recipe_time_update'])) {
            return '';
        }
        return $this->storage['food_recipe_time_update'];
    }

    public function setFoodRecipeTimeUpdate(string $foodRecipeTimeUpdate): void
    {
        $this->storage['food_recipe_time_update'] = $foodRecipeTimeUpdate;
    }

    public function getFoodRecipePrepTime(): string
    {
        if (!isset($this->storage['food_recipe_prep_time'])) {
            return '';
        }
        return $this->storage['food_recipe_prep_time'];
    }

    public function setFoodRecipePrepTime(string $foodRecipePrepTime): void
    {
        $this->storage['food_recipe_prep_time'] = $foodRecipePrepTime;
    }

    public function getFoodRecipeCookTime(): string
    {
        if (!isset($this->storage['food_recipe_cook_time'])) {
            return '';
        }
        return $this->storage['food_recipe_cook_time'];
    }

    public function setFoodRecipeCookTime(string $foodRecipeCookTime): void
    {
        $this->storage['food_recipe_cook_time'] = $foodRecipeCookTime;
    }

    public function getFoodRecipeYield(): string
    {
        if (!isset($this->storage['food_recipe_yield'])) {
            return '';
        }
        return $this->storage['food_recipe_yield'];
    }

    public function setFoodRecipeYield(string $foodRecipeYield): void
    {
        $this->storage['food_recipe_yield'] = $foodRecipeYield;
    }

    public function getImageId(): int
    {
        if (!isset($this->storage['image_id'])) {
            return 0;
        }
        return $this->storage['image_id'];
    }

    public function setImageId(int $imageId): void
    {
        $this->storage['image_id'] = $imageId;
    }

    public function getImageLabel(): string
    {
        if (!isset($this->storage['image_label'])) {
            return '';
        }
        return $this->storage['image_label'];
    }

    public function setImageLabel(string $imageLabel): void
    {
        $this->storage['image_label'] = $imageLabel;
    }

    public function getImageDesc(): string
    {
        if (!isset($this->storage['image_desc'])) {
            return '';
        }
        return $this->storage['image_desc'];
    }

    public function setImageDesc(string $imageDesc): void
    {
        $this->storage['image_desc'] = $imageDesc;
    }

    public function getImageFilename(): string
    {
        if (!isset($this->storage['image_filename'])) {
            return '';
        }
        return $this->storage['image_filename'];
    }

    public function setImageFilename(string $imageFilename): void
    {
        $this->storage['image_filename'] = $imageFilename;
    }

    public function getImageExt(): string
    {
        if (!isset($this->storage['image_ext'])) {
            return '';
        }
        return $this->storage['image_ext'];
    }

    public function setImageExt(string $imageExt): void
    {
        $this->storage['image_ext'] = $imageExt;
    }

    public function getImageScaling()
    {
        if (!isset($this->storage['image_scaling'])) {
            return '';
        }
        return $this->storage['image_scaling'];
    }

    public function setImageScaling($imageScaling): void
    {
        $this->storage['image_scaling'] = $imageScaling;
    }

    public function getImageTimeCreate(): string
    {
        if (!isset($this->storage['image_time_create'])) {
            return '';
        }
        return $this->storage['image_time_create'];
    }

    public function setImageTimeCreate(string $imageTimeCreate): void
    {
        $this->storage['image_time_create'] = $imageTimeCreate;
    }
}
