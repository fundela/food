<?php

namespace Fundela\Food\Entity;

use Fundela\Trinket\Entity\AbstractEntity;

class FoodRecipePartEntity extends AbstractEntity
{
    public function __toString(): string
    {
        return $this->getFoodRecipePartQntty() . ' ' . $this->getQuantityunitLabel()
            . ' ' . $this->getFoodRawNameEn() . ' ' . $this->getFoodRawNameScient();
    }

    public array $mapping = [
        'food_recipe_part_uuid'           => 'food_recipe_part_uuid',
        'food_recipe_uuid'                => 'food_recipe_uuid',
        'food_raw_uuid'                   => 'food_raw_uuid',
        'food_recipe_part_qntty'          => 'food_recipe_part_qntty',
        'food_recipe_part_comment'        => 'food_recipe_part_comment',
        'food_recipe_part_order_priority' => 'food_recipe_part_order_priority',
        'food_raw_name_de'                => 'food_raw_name_de',
        'food_raw_name_en'                => 'food_raw_name_en',
        'food_raw_name_scient'            => 'food_raw_name_scient',
        'food_raw_desc'                   => 'food_raw_desc',
        'quantityunit_uuid'               => 'quantityunit_uuid',
        'quantityunit_gram'               => 'quantityunit_gram',
        'quantityunit_label'              => 'quantityunit_label',
        'quantityunit_resolution'         => 'quantityunit_resolution',
        'quantityunit_resolution_origin'  => 'quantityunit_resolution_origin',
        'quantityunit_label_origin'       => 'quantityunit_label_origin',
        'ingrdnt_01_percent'              => 'ingrdnt_01_percent',
        'ingrdnt_02_percent'              => 'ingrdnt_02_percent',
        'ingrdnt_03_percent'              => 'ingrdnt_03_percent',
        'ingrdnt_04_percent'              => 'ingrdnt_04_percent',
        'ingrdnt_05_percent'              => 'ingrdnt_05_percent',
    ];

    protected string $primaryKey = 'food_recipe_part_uuid';

    public function getFoodRecipePartUuid(): string
    {
        if (!isset($this->storage['food_recipe_part_uuid'])) {
            return '';
        }
        return $this->storage['food_recipe_part_uuid'];
    }

    public function setFoodRecipePartUuid(string $foodRecipePartUuid): void
    {
        $this->storage['food_recipe_part_uuid'] = $foodRecipePartUuid;
    }

    public function getFoodRecipeUuid(): string
    {
        if (!isset($this->storage['food_recipe_uuid'])) {
            return '';
        }
        return $this->storage['food_recipe_uuid'];
    }

    public function setFoodRecipeUuid(string $foodRecipeUuid): void
    {
        $this->storage['food_recipe_uuid'] = $foodRecipeUuid;
    }

    public function getFoodRawUuid(): string
    {
        if (!isset($this->storage['food_raw_uuid'])) {
            return '';
        }
        return $this->storage['food_raw_uuid'];
    }

    public function setFoodRawUuid(string $foodRawUuid): void
    {
        $this->storage['food_raw_uuid'] = $foodRawUuid;
    }

    public function getFoodRecipePartQntty(): float
    {
        if (!isset($this->storage['food_recipe_part_qntty'])) {
            return 0;
        }
        return $this->storage['food_recipe_part_qntty'];
    }

    public function setFoodRecipePartQntty(float $foodRecipePartQntty): void
    {
        $this->storage['food_recipe_part_qntty'] = $foodRecipePartQntty;
    }

    public function getFoodRecipePartComment(): string
    {
        if (!isset($this->storage['food_recipe_part_comment'])) {
            return '';
        }
        return $this->storage['food_recipe_part_comment'];
    }

    public function setFoodRecipePartComment(string $foodRecipePartComment): void
    {
        $this->storage['food_recipe_part_comment'] = $foodRecipePartComment;
    }

    public function getFoodRecipePartOrderPriority(): int
    {
        if (!isset($this->storage['food_recipe_part_order_priority'])) {
            return 0;
        }
        return $this->storage['food_recipe_part_order_priority'];
    }

    public function setFoodRecipePartOrderPriority(int $foodRecipePartOrderPriority): void
    {
        $this->storage['food_recipe_part_order_priority'] = $foodRecipePartOrderPriority;
    }

    public function getFoodRawNameDe(): string
    {
        if (!isset($this->storage['food_raw_name_de'])) {
            return '';
        }
        return $this->storage['food_raw_name_de'];
    }

    public function setFoodRawNameDe(string $foodRawNameDe): void
    {
        $this->storage['food_raw_name_de'] = $foodRawNameDe;
    }

    public function getFoodRawNameEn(): string
    {
        if (!isset($this->storage['food_raw_name_en'])) {
            return '';
        }
        return $this->storage['food_raw_name_en'];
    }

    public function setFoodRawNameEn(string $foodRawNameEn): void
    {
        $this->storage['food_raw_name_en'] = $foodRawNameEn;
    }

    public function getFoodRawNameScient(): string
    {
        if (!isset($this->storage['food_raw_name_scient'])) {
            return '';
        }
        return $this->storage['food_raw_name_scient'];
    }

    public function setFoodRawNameScient(string $foodRawNameScient): void
    {
        $this->storage['food_raw_name_scient'] = $foodRawNameScient;
    }

    public function getFoodRawDesc(): string
    {
        if (!isset($this->storage['food_raw_desc'])) {
            return '';
        }
        return $this->storage['food_raw_desc'];
    }

    public function setFoodRawDesc(string $foodRawDesc): void
    {
        $this->storage['food_raw_desc'] = $foodRawDesc;
    }

    public function getQuantityunitUuid(): string
    {
        if (!isset($this->storage['quantityunit_uuid'])) {
            return '';
        }
        return $this->storage['quantityunit_uuid'];
    }

    public function setQuantityunitUuid(string $quantityunitUuid): void
    {
        $this->storage['quantityunit_uuid'] = $quantityunitUuid;
    }

    public function getQuantityunitGram(): float
    {
        if (!isset($this->storage['quantityunit_gram'])) {
            return 0;
        }
        return $this->storage['quantityunit_gram'];
    }

    public function setQuantityunitGram(float $quantityunitGram): void
    {
        $this->storage['quantityunit_gram'] = $quantityunitGram;
    }

    public function getQuantityunitLabel(): string
    {
        if (!isset($this->storage['quantityunit_label'])) {
            return '';
        }
        return $this->storage['quantityunit_label'];
    }

    public function setQuantityunitLabel(string $quantityunitLabel): void
    {
        $this->storage['quantityunit_label'] = $quantityunitLabel;
    }

    public function getQuantityunitResolution(): float
    {
        if (!isset($this->storage['quantityunit_resolution'])) {
            return 0;
        }
        return $this->storage['quantityunit_resolution'];
    }

    public function setQuantityunitResolution(float $quantityunitResolution): void
    {
        $this->storage['quantityunit_resolution'] = $quantityunitResolution;
    }

    public function getQuantityunitResolutionOrigin(): string
    {
        if (!isset($this->storage['quantityunit_resolution_origin'])) {
            return '';
        }
        return $this->storage['quantityunit_resolution_origin'];
    }

    public function setQuantityunitResolutionOrigin(string $quantityunitResolutionOrigin): void
    {
        $this->storage['quantityunit_resolution_origin'] = $quantityunitResolutionOrigin;
    }

    public function getQuantityunitLabelOrigin(): string
    {
        if (!isset($this->storage['quantityunit_label_origin'])) {
            return '';
        }
        return $this->storage['quantityunit_label_origin'];
    }

    public function setQuantityunitLabelOrigin(string $quantityunitLabelOrigin): void
    {
        $this->storage['quantityunit_label_origin'] = $quantityunitLabelOrigin;
    }

    public function getIngrdnt01Percent(): float
    {
        if (!isset($this->storage['ingrdnt_01_percent'])) {
            return 0;
        }
        return $this->storage['ingrdnt_01_percent'];
    }

    public function setIngrdnt01Percent(float $ingrdnt01Percent): void
    {
        $this->storage['ingrdnt_01_percent'] = $ingrdnt01Percent;
    }

    public function getIngrdnt02Percent(): float
    {
        if (!isset($this->storage['ingrdnt_02_percent'])) {
            return 0;
        }
        return $this->storage['ingrdnt_02_percent'];
    }

    public function setIngrdnt02Percent(float $ingrdnt02Percent): void
    {
        $this->storage['ingrdnt_02_percent'] = $ingrdnt02Percent;
    }

    public function getIngrdnt03Percent(): float
    {
        if (!isset($this->storage['ingrdnt_03_percent'])) {
            return 0;
        }
        return $this->storage['ingrdnt_03_percent'];
    }

    public function setIngrdnt03Percent(float $ingrdnt03Percent): void
    {
        $this->storage['ingrdnt_03_percent'] = $ingrdnt03Percent;
    }

    public function getIngrdnt04Percent(): float
    {
        if (!isset($this->storage['ingrdnt_04_percent'])) {
            return 0;
        }
        return $this->storage['ingrdnt_04_percent'];
    }

    public function setIngrdnt04Percent(float $ingrdnt04Percent): void
    {
        $this->storage['ingrdnt_04_percent'] = $ingrdnt04Percent;
    }

    public function getIngrdnt05Percent(): float
    {
        if (!isset($this->storage['ingrdnt_05_percent'])) {
            return 0;
        }
        return $this->storage['ingrdnt_05_percent'];
    }

    public function setIngrdnt05Percent(float $ingrdnt05Percent): void
    {
        $this->storage['ingrdnt_05_percent'] = $ingrdnt05Percent;
    }
}
