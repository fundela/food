<?php

namespace Fundela\Food\View\Helper;

use Fundela\Food\Service\Recipe\FoodRecipeService;
use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Model\ViewModel;

class IsEditAllowed extends AbstractHelper
{
    protected FoodRecipeService $foodRecipeService;

    public function setFoodRecipeService(FoodRecipeService $foodRecipeService): void
    {
        $this->foodRecipeService = $foodRecipeService;
    }

    /**
     * @param string $recipeUuid
     * @param string $userUuid
     * @return bool
     */
    public function __invoke(string $recipeUuid, string $userUuid): bool
    {
        return $this->foodRecipeService->checkEditAllowed($recipeUuid, $userUuid);
    }
}
