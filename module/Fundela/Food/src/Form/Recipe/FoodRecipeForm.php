<?php

namespace Fundela\Food\Form\Recipe;

use Fundela\Trinket\Filter\Nl2BlFilter;
use Fundela\Trinket\Filter\SanitizeStringFilter;
use Fundela\Trinket\Form\AbstractForm;
use Fundela\Trinket\Form\Element\Textarea;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class FoodRecipeForm extends AbstractForm implements InputFilterProviderInterface
{

    public function init()
    {
        $this->add([
            'type' => Hidden::class,
            'name' => 'recipe_form_type',
            'attributes' => [
                'value' => 'recipe',
            ]
        ]);

        if ($this->primaryKeyAvailable) {
            $this->add([
                'type' => Hidden::class,
                'name' => 'food_recipe_uuid',
            ]);
        }
        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_label',
            'options' => [
                'label' => 'Label',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_alias',
            'options' => [
                'label' => 'Alias (readonly)',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'readonly' => 'readonly',
            ]
        ]);
        $this->add([
            'type' => Textarea::class,
            'name' => 'food_recipe_instrctn',
            'options' => [
                'label' => 'Anweisungen',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'rows' => 20,
            ]
        ]);

        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_meta_title',
            'options' => [
                'label' => 'META title',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Textarea::class,
            'name' => 'food_recipe_meta_keywords',
            'options' => [
                'label' => 'META keywords',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'title' => 'comma separated',
            ]
        ]);
        $this->add([
            'type' => Textarea::class,
            'name' => 'food_recipe_meta_desc',
            'options' => [
                'label' => 'META desc',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_prep_time',
            'options' => [
                'label' => 'Prep Time',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'title' => 'ISO 8601 Time e.g. PT42M',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_cook_time',
            'options' => [
                'label' => 'Cook Time',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'title' => 'ISO 8601 Time e.g. PT42M',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_yield',
            'options' => [
                'label' => 'Ertrag',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'title' => 'e.g. 6 Schoko Muffins',
            ]
        ]);

        $this->add([
            'type' => Submit::class,
            'attributes' => [
                'name' => 'submit',
                'value' => 'Speichern',
                'class' => 'w3-button w3-blue-gray',
            ],
        ]);
        $this->add([
            'type' => Submit::class,
            'attributes' => [
                'name' => 'submit_back',
                'value' => 'Speichern & zurück',
                'class' => 'w3-button w3-blue-gray',
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['recipe_form_type'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ]
        ];

        if ($this->primaryKeyAvailable) {
            $filter['food_recipe_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => SanitizeStringFilter::class],
                    ['name' => StringTrim::class],
                    ['name' => StripTags::class],
                    ['name' => HtmlEntities::class],
                ],
                'validators' => [
                    [
                        'name' => Uuid::class,
                    ]
                ]
            ];
        }

        $filter['food_recipe_label'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['food_recipe_instrctn'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 62000,
                    ]
                ]
            ]
        ];

        $filter['food_recipe_meta_title'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];
        $filter['food_recipe_meta_keywords'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 1000,
                    ]
                ]
            ]
        ];
        $filter['food_recipe_meta_desc'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
                ['name' => Nl2BlFilter::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 1000,
                    ]
                ]
            ]
        ];
        $filter['food_recipe_prep_time'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 16,
                    ]
                ]
            ]
        ];
        $filter['food_recipe_cook_time'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 16,
                    ]
                ]
            ]
        ];
        $filter['food_recipe_yield'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ], 'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 4,
                        'max' => 80,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
