<?php

namespace Fundela\Food\Form\Recipe;

use Fundela\Trinket\Filter\SanitizeStringFilter;
use Fundela\Trinket\Form\AbstractForm;
use Fundela\Trinket\Validator\FloatValidator;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class FoodRecipePartForm extends AbstractForm implements InputFilterProviderInterface, AdapterAwareInterface
{
    protected Adapter $adapter;
    protected bool $withSubmit = true;

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function setWithSubmit(bool $withSubmit): void
    {
        $this->withSubmit = $withSubmit;
    }

    public function init()
    {
        $this->add([
            'type' => Hidden::class,
            'name' => 'recipe_form_type',
            'attributes' => [
                'value' => 'part',
            ]
        ]);
        if ($this->primaryKeyAvailable) {
            $this->add([
                'type' => Hidden::class,
                'name' => 'food_recipe_part_uuid',
            ]);
        }
        $this->add([
            'type' => Hidden::class,
            'name' => 'food_recipe_uuid',
        ]);
        /*
         * A Form\Element\Select is unsuitable, because there are too much foodRaws.
         * Make your own Select-Popup (or else) to fill this form-field.
         */
        $this->add([
            'type' => Hidden::class,
            'name' => 'food_raw_uuid',
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_part_qntty',
            'options' => [
                'label' => 'Menge',
                'label_attributes' => [
                    'id' => 'food_recipe_part_qntty_label' // to set the label dynamic. E.g. 'Menge in Liter'
                ],
            ],
            'attributes' => [
                'class' => 'w3-input',
                'id' => 'food_recipe_part_qntty',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_recipe_part_comment',
            'options' => [
                'label' => 'Kommentar',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'id' => 'food_recipe_part_comment',
            ]
        ]);

        if ($this->withSubmit) {
            $this->add([
                'type' => Submit::class,
                'attributes' => [
                    'value' => 'Submit',
                    'id' => 'submit_recipe_part',
                    'class' => 'w3-button w3-blue-gray',
                ],
            ]);
        }
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        $filter['recipe_form_type'] = [
            'required' => false,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ]
        ];

        if ($this->primaryKeyAvailable) {
            $filter['food_recipe_part_uuid'] = [
                'required' => true,
                'filters' => [
                    ['name' => SanitizeStringFilter::class],
                ],
                'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['food_recipe_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ],
            'validators' => [
                ['name' => Uuid::class],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'food_recipe',
                        'field' => 'food_recipe_uuid',
                    ]
                ],
            ]
        ];

        $filter['food_raw_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ],
            'validators' => [
                ['name' => Uuid::class],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'food_raw',
                        'field' => 'food_raw_uuid',
                    ],
                ],
            ]
        ];

        $filter['food_recipe_part_qntty'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                ['name' => FloatValidator::class,]
            ]
        ];

        $filter['food_recipe_part_comment'] = [
            'required' => false,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 400,
                    ]
                ]
            ]
        ];

        return $filter;
    }
}
