<?php

namespace Fundela\Food\Form\Raw;

use Fundela\Trinket\Filter\SanitizeStringFilter;
use Fundela\Trinket\Form\AbstractForm;
use Fundela\Trinket\Validator\FloatValidator;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\AdapterAwareInterface;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Filter\ToFloat;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Text;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Db\RecordExists;
use Laminas\Validator\Digits;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class FoodRawPartForm extends AbstractForm implements InputFilterProviderInterface, AdapterAwareInterface
{
    protected Adapter $adapter;

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function init()
    {

        if ($this->primaryKeyAvailable) {
            $this->add([
                'type' => Hidden::class,
                'name' => 'food_raw_part_uuid',
            ]);
        }
        $this->add([
            'type' => Text::class,
            'name' => 'food_raw_uuid',
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_ingrdnt_id',
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_raw_part_qntty',
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['food_raw_part_uuid'] = [
                'required' => true,
                'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['food_raw_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => Uuid::class,
                ],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'food_raw',
                        'field' => 'food_raw_uuid',
                    ]
                ]
            ]
        ];

        $filter['food_ingrdnt_id'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => Digits::class,
                ],
                [
                    'name' => RecordExists::class,
                    'options' => [
                        'adapter' => $this->adapter,
                        'table' => 'food_ingrdnt',
                        'field' => 'food_ingrdnt_id',
                    ]
                ]
            ]
        ];

        $filter['food_raw_part_qntty'] = [
            'required' => false,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => FloatValidator::class,
                ]
            ]
        ];

        return $filter;
    }
}
