<?php

namespace Fundela\Food\Form\Raw;

use Fundela\Trinket\Filter\SanitizeStringFilter;
use Fundela\Trinket\Form\AbstractForm;
use Fundela\Trinket\Validator\FloatValidator;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;
use Laminas\Validator\Uuid;

class FoodRawForm extends AbstractForm implements InputFilterProviderInterface
{
    protected array $quantityunitUuidAssoc = [];

    public function setQuantityunitUuidAssoc(array $quantityunitUuidAssoc): void
    {
        $this->quantityunitUuidAssoc = $quantityunitUuidAssoc;
    }

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add([
                'type' => Hidden::class,
                'name' => 'food_raw_uuid',
            ]);
        }
        $this->add([
            'type' => Text::class,
            'name' => 'food_raw_name_de',
            'options' => [
                'label' => 'Name de',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_raw_name_en',
            'options' => [
                'label' => 'Name en',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_raw_name_scient',
            'options' => [
                'label' => 'Name scient',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Textarea::class,
            'name' => 'food_raw_desc',
            'options' => [
                'label' => 'Beschreibung',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Select::class,
            'name' => 'quantityunit_uuid',
            'options' => [
                'label' => 'Maßeinheit',
//                'empty_option' => '-- unit --',
                'value_options' => $this->quantityunitUuidAssoc,
            ],
            'attributes' => [
                'class' => 'w3-select',
                'id' => 'quantityunit_uuid',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'quantityunit_gram',
            'options' => [
                'label' => 'Gramm pro Einheit',
            ],
            'attributes' => [
                'class' => 'w3-input',
                'value' => 1
            ]
        ]);

        $this->add([
            'type' => Submit::class,
            'attributes' => [
                'value' => 'Submit',
                'class' => 'w3-button w3-blue-gray',
            ],
        ]);
        $this->add([
            'type' => Submit::class,
            'attributes' => [
                'name' => 'submit_back',
                'value' => 'Submit & back',
                'class' => 'w3-button w3-blue-gray',
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['food_raw_uuid'] = [
                'required' => true,
                'validators' => [
                    ['name' => Uuid::class]
                ]
            ];
        }

        $filter['food_raw_name_de'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['food_raw_name_en'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['food_raw_name_scient'] = [
            'required' => false,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['food_raw_desc'] = [
            'required' => false,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'max' => 60000,
                    ]
                ]
            ]
        ];

        $filter['quantityunit_uuid'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
            ],
            'validators' => [
                ['name' => Uuid::class],
                [
                    'name' => InArray::class,
                    'options' => [
                        'haystack' => array_keys($this->quantityunitUuidAssoc)
                    ]
                ]
            ]
        ];

        $filter['quantityunit_gram'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => FloatValidator::class,
                ]
            ]
        ];

        return $filter;
    }
}
