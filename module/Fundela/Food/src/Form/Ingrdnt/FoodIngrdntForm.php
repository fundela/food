<?php

namespace Fundela\Food\Form\Ingrdnt;

use Fundela\Trinket\Filter\SanitizeStringFilter;
use Fundela\Trinket\Form\AbstractForm;
use Laminas\Filter\HtmlEntities;
use Laminas\Filter\StringTrim;
use Laminas\Filter\StripTags;
use Laminas\Form\Element\Hidden;
use Laminas\Form\Element\Number;
use Laminas\Form\Element\Radio;
use Laminas\Form\Element\Submit;
use Laminas\Form\Element\Text;
use Laminas\Form\Element\Textarea;
use Laminas\InputFilter\InputFilterProviderInterface;
use Laminas\Validator\Digits;
use Laminas\Validator\InArray;
use Laminas\Validator\StringLength;

class FoodIngrdntForm extends AbstractForm implements InputFilterProviderInterface
{

    /**
     * @var string[] keys are real boolean. Otherwise the automatic type conversion would make a 'false' from a true
     */
    protected $requireValueOptions = [
        false => 'No',
        true => 'Yes',
    ];

    public function init()
    {
        if ($this->primaryKeyAvailable) {
            $this->add([
                'type' => Hidden::class,
                'name' => 'food_ingrdnt_id',
            ]);
        }
        $this->add([
            'type' => Text::class,
            'name' => 'food_ingrdnt_name_de',
            'options' => [
                'label' => 'Name de',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_ingrdnt_name_en',
            'options' => [
                'label' => 'Name en',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Text::class,
            'name' => 'food_ingrdnt_name_scient',
            'options' => [
                'label' => 'Name scient',
            ],
            'attributes' => [
                'class' => 'w3-input',
            ]
        ]);
        $this->add([
            'type' => Radio::class, // validator inside
            'name' => 'food_ingrdnt_require',
            'options' => [
                'label' => 'mandatory?',
                'value_options' => $this->requireValueOptions,
            ],
            'attributes' => [
                'class' => 'w3-radio',
            ]
        ]);
        $this->add([
            'type' => Submit::class,
            'attributes' => [
                'value' => 'Submit',
                'class' => 'w3-button w3-blue-gray',
            ],
        ]);
        $this->add([
            'type' => Submit::class,
            'attributes' => [
                'name' => 'submit_back',
                'value' => 'Submit & back',
                'class' => 'w3-button w3-blue-gray',
            ],
        ]);
    }

    /**
     * Should return an array specification compatible with
     * {@link \Laminas\InputFilter\Factory::createInputFilter()}.
     * @return array
     */
    public function getInputFilterSpecification()
    {
        $filter = [];

        if ($this->primaryKeyAvailable) {
            $filter['food_ingrdnt_id'] = [
                'required' => true,
                'filters' => [
                    ['name' => SanitizeStringFilter::class],
                    ['name' => StringTrim::class],
                    ['name' => StripTags::class],
                    ['name' => HtmlEntities::class],
                ],
                'validators' => [
                    [
                        'name' => Digits::class,
                    ]
                ]
            ];
        }

        $filter['food_ingrdnt_name_de'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['food_ingrdnt_name_en'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

        $filter['food_ingrdnt_name_scient'] = [
            'required' => true,
            'filters' => [
                ['name' => SanitizeStringFilter::class],
                ['name' => StringTrim::class],
                ['name' => StripTags::class],
                ['name' => HtmlEntities::class],
            ],
            'validators' => [
                [
                    'name' => StringLength::class,
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min' => 2,
                        'max' => 150,
                    ]
                ]
            ]
        ];

//        $filter['food_ingrdnt_require'] = [
//            'required' => true,
//            'filters' => [
//                ['name' => SanitizeStringFilter::class],
//                ['name' => StringTrim::class],
//                ['name' => StripTags::class],
//                ['name' => HtmlEntities::class],
//            ],
//            'validators' => [
//                [
//                    'name' => InArray::class,
//                    'options' => [
//                        'haystack' => array_keys($this->requireValueOptions)
//                    ]
//                ]
//            ]
//        ];

        return $filter;
    }
}
