<?php

namespace Fundela\Food\Factory\Service\Raw;

use Fundela\Food\Service\Raw\FoodRawPartService;
use Fundela\Food\Table\Raw\FoodRawPartTable;
use Fundela\Food\Table\Raw\FoodRawTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRawPartServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FoodRawPartService();
        $service->setLogger($container->get('logger'));
        $service->setFoodRawTable($container->get(FoodRawTable::class));
        $service->setFoodRawPartTable($container->get(FoodRawPartTable::class));
        return $service;
    }
}
