<?php

namespace Fundela\Food\Factory\Service\Ingrdnt;

use Fundela\Food\Service\Ingrdnt\FoodIngrdntService;
use Fundela\Food\Table\Ingrdnt\FoodIngrdntTable;
use Fundela\Food\Table\Raw\FoodRawPartTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodIngrdntServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FoodIngrdntService();
        $service->setLogger($container->get('logger'));
        $service->setFoodIngrdntTable($container->get(FoodIngrdntTable::class));
        $service->setFoodRawPartTable($container->get(FoodRawPartTable::class));
        return $service;
    }
}
