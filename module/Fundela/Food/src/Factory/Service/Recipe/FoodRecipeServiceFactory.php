<?php

namespace Fundela\Food\Factory\Service\Recipe;

use Fundela\Food\Service\Raw\FoodRawService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Food\Table\Recipe\FoodRecipeCategoryRelTable;
use Fundela\Food\Table\Recipe\FoodRecipeCategoryTable;
use Fundela\Food\Table\Recipe\FoodRecipePartTable;
use Fundela\Food\Table\Recipe\FoodRecipeTable;
use Fundela\User\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRecipeServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FoodRecipeService();
        $service->setLogger($container->get('logger'));
        $service->setFoodRecipeTable($container->get(FoodRecipeTable::class));
        $service->setFoodRecipePartTable($container->get(FoodRecipePartTable::class));
        $service->setRecipeIngrdntIdMapping($container->get('config')['fundela_food']['recipe_ingrdnt_id_mapping']);
        $service->setFoodRawService($container->get(FoodRawService::class));
        $service->setFoodRecipeCategoryTable($container->get(FoodRecipeCategoryTable::class));
        $service->setFoodRecipeCategoryRelTable($container->get(FoodRecipeCategoryRelTable::class));
        $service->setUserService($container->get(UserService::class));
        return $service;
    }
}
