<?php

namespace Fundela\Food\Factory\Service\Recipe;

use Fundela\Food\Service\Recipe\JSONLDRecipeService;
use Fundela\Image\View\ImageSrc;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class JSONLDRecipeServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new JSONLDRecipeService();
        $service->setLogger($container->get('logger'));
        $service->setImageSrc($container->get('ViewHelperManager')->get(ImageSrc::class));
        return $service;
    }
}
