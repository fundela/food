<?php

namespace Fundela\Food\Factory\Service\Recipe;

use Fundela\Food\Service\Recipe\FoodRecipePartService;
use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Food\Table\Recipe\FoodRecipePartTable;
use Fundela\Food\Table\Recipe\FoodRecipeTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRecipePartServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FoodRecipePartService();
        $service->setLogger($container->get('logger'));
        $service->setFoodRecipePartTable($container->get(FoodRecipePartTable::class));
        $service->setFoodRecipeTable($container->get(FoodRecipeTable::class));
        $service->setFoodRecipeService($container->get(FoodRecipeService::class));
        return $service;
    }
}
