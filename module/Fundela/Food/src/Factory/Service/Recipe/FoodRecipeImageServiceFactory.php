<?php

namespace Fundela\Food\Factory\Service\Recipe;

use Fundela\Food\Service\Recipe\FoodRecipeImageService;
use Fundela\Food\Table\Recipe\FoodRecipeImageRelTable;
use Fundela\Image\Service\ImageService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRecipeImageServiceFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $service = new FoodRecipeImageService();
        $service->setLogger($container->get('logger'));
        $service->setImageService($container->get(ImageService::class));
        $service->setFoodRecipeImageRelTable($container->get(FoodRecipeImageRelTable::class));
        return $service;
    }
}
