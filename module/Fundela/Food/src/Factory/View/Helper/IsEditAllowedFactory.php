<?php

namespace Fundela\Food\Factory\View\Helper;

use Fundela\Food\Service\Recipe\FoodRecipeService;
use Fundela\Food\View\Helper\IsEditAllowed;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class IsEditAllowedFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $viewHelper = new IsEditAllowed();
        $viewHelper->setFoodRecipeService($container->get(FoodRecipeService::class));
        return $viewHelper;
    }
}
