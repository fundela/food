<?php

namespace Fundela\Food\Factory\Form\Raw;

use Fundela\Food\Form\Raw\FoodRawForm;
use Fundela\Trinket\Table\QuantityunitTable;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Exception\ServiceNotFoundException;
use Laminas\ServiceManager\Factory\FactoryInterface;

class FoodRawFormFactory implements FactoryInterface
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param null|array $options
     * @return object
     * @throws ServiceNotFoundException if unable to resolve the service
     * @throws ServiceNotCreatedException if an exception is raised when creating a service
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $form = new FoodRawForm();
        /** @var QuantityunitTable $quantityunitTable */
        $quantityunitTable = $container->get(QuantityunitTable::class);
        $form->setQuantityunitUuidAssoc($quantityunitTable->getQuantityunitUuidLabelAssoc());
        return $form;
    }
}
