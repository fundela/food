<?php

namespace Fundela\Food\Service\Ingrdnt;

use Fundela\Food\Table\Raw\FoodRawPartTable;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Service\AbstractService;
use Fundela\Food\Table\Ingrdnt\FoodIngrdntTable;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Db\Sql\Select;

class FoodIngrdntService extends AbstractService
{
    protected FoodIngrdntTable $foodIngrdntTable;
    protected FoodRawPartTable $foodRawPartTable;

    /**
     * @param FoodIngrdntTable $foodIngrdntTable
     */
    public function setFoodIngrdntTable(FoodIngrdntTable $foodIngrdntTable): void
    {
        $this->foodIngrdntTable = $foodIngrdntTable;
    }

    /**
     * @param FoodRawPartTable $foodRawPartTable
     */
    public function setFoodRawPartTable(FoodRawPartTable $foodRawPartTable): void
    {
        $this->foodRawPartTable = $foodRawPartTable;
    }

    public function getFoodIngrdnt(int $id): array
    {
        return $this->foodIngrdntTable->getFoodIngrdnt($id);
    }

    public function insertOrUpdateFoodIngrdnt(array $formdata): int
    {
        if (!empty($formdata['food_ingrdnt_id'])) {
            return $this->updateFoodIngrdnt(
                $formdata['food_ingrdnt_id']
                , $formdata['food_ingrdnt_name_de']
                , $formdata['food_ingrdnt_name_en']
                , $formdata['food_ingrdnt_name_scient']
                , $formdata['food_ingrdnt_require']
            );
        } else {
            return $this->insertFoodIngrdnt(
                $formdata['food_ingrdnt_name_de']
                , $formdata['food_ingrdnt_name_en']
                , $formdata['food_ingrdnt_name_scient']
                , $formdata['food_ingrdnt_require']
            );
        }
    }

    public function insertFoodIngrdnt(string $nameDe, string $nameEn, string $nameScient, bool $require): int
    {
        return $this->foodIngrdntTable->insertFoodIngrdnt($nameDe, $nameEn, $nameScient, $require);
    }

    public function updateFoodIngrdnt(int $id, string $nameDe, string $nameEn, string $nameScient, bool $require): int
    {
        return $this->foodIngrdntTable->updateFoodIngrdnt($id, $nameDe, $nameEn, $nameScient, $require);
    }

    public function searchFoodIngrdnt(ListQueryBase $listQueryBase, string $ingrdntName, string $rawUuid): array
    {
        return $this->foodIngrdntTable->searchFoodIngrdnt($listQueryBase, $ingrdntName, $rawUuid);
    }

    public function searchFoodIngrdntDbSelect(ListQueryBase $listQueryBase, string $ingrdntName): Select
    {
        return $this->foodIngrdntTable->searchFoodIngrdntDbSelect($listQueryBase, $ingrdntName);
    }

    public function getFoodIngrdntAdapter(): Adapter
    {
        return $this->foodIngrdntTable->getAdapter();
    }

    public function deleteIngrdntComplete(int $ingrdntId): bool
    {
        /** @var AbstractConnection $connection */
        $connection = $this->foodIngrdntTable->getAdapter()->getDriver()->getConnection();
        $connection->beginTransaction();
        if ($this->foodRawPartTable->deleteFoodRawPartsForFoodIngrdnt($ingrdntId) < 0) {
            $connection->rollback();
            return false;
        }
        if ($this->foodIngrdntTable->deleteFoodIngrdnt($ingrdntId) < 0) {
            $connection->rollback();
            return false;
        }
        $connection->commit();
        return true;
    }

    public function updateFoodIngrdntPriority(int $id, int $prio): bool
    {
        return $this->foodIngrdntTable->updateFoodIngrdntPriority($id, $prio) >= 0;
    }

    public function getFoodIngrdntsIdAssoc(bool $onlyRequired = true): array
    {
        return $this->foodIngrdntTable->getFoodIngrdntsIdAssoc($onlyRequired);
    }
}
