<?php

namespace Fundela\Food\Service\Raw;

use Fundela\Food\Table\Raw\FoodRawCategoryRelTable;
use Fundela\Food\Table\Raw\FoodRawCategoryTable;
use Fundela\Food\Table\Raw\FoodRawPartTable;
use Fundela\Food\Table\Raw\FoodRawTable;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Adapter;
use Laminas\Db\Sql\Select;

class FoodRawService extends AbstractService
{
    protected FoodRawTable $foodRawTable;
    protected FoodRawPartTable $foodRawPartTable;
    protected FoodRawCategoryTable $foodRawCategoryTable;
    protected FoodRawCategoryRelTable $foodRawCategoryRelTable;

    public function setFoodRawTable(FoodRawTable $foodRawTable): void
    {
        $this->foodRawTable = $foodRawTable;
    }

    public function setFoodRawPartTable(FoodRawPartTable $foodRawPartTable): void
    {
        $this->foodRawPartTable = $foodRawPartTable;
    }

    public function setFoodRawCategoryTable(FoodRawCategoryTable $foodRawCategoryTable): void
    {
        $this->foodRawCategoryTable = $foodRawCategoryTable;
    }

    public function setFoodRawCategoryRelTable(FoodRawCategoryRelTable $foodRawCategoryRelTable): void
    {
        $this->foodRawCategoryRelTable = $foodRawCategoryRelTable;
    }

    /**
     * @return array
     */
    public function getFoodRawCategoryIdAssoc(): array
    {
        return $this->foodRawCategoryTable->getFoodRawCategoryIdAssoc();
    }

    /**
     * @param int $rawCategoryId
     * @return array
     */
    public function getFoodRawsForCategory(int $rawCategoryId): array
    {
        return $this->foodRawCategoryRelTable->getFoodRawsForCategory($rawCategoryId);
    }

    public function getFoodRaw(string $uuid): array
    {
        return $this->foodRawTable->getFoodRaw($uuid);
    }

    public function insertOrUpdateFoodRaw(array $formdata): string
    {
        if (!empty($formdata['food_raw_uuid'])) {
            return $this->updateFoodRaw(
                $formdata['food_raw_uuid']
                , $formdata['food_raw_name_de']
                , $formdata['food_raw_name_en']
                , $formdata['food_raw_name_scient'] ?: ''
                , $formdata['food_raw_desc']
                , $formdata['quantityunit_uuid']
                , $formdata['quantityunit_gram']
            );
        } else {
            return $this->insertFoodRaw(
                $formdata['food_raw_name_de']
                , $formdata['food_raw_name_en']
                , $formdata['food_raw_name_scient'] ?: ''
                , $formdata['food_raw_desc']
                , $formdata['quantityunit_uuid']
                , $formdata['quantityunit_gram']
            );
        }
    }

    public function insertFoodRaw(string $nameDe, string $nameEn, string $nameScient, string $desc, string $quantityunitUuid, float $gram): string
    {
        return $this->foodRawTable->insertFoodRaw($nameDe, $nameEn, $nameScient, $desc, $quantityunitUuid, $gram);
    }

    public function updateFoodRaw(string $rawUuid, string $nameDe, string $nameEn, string $nameScient, string $desc, string $quantityunitUuid, float $gram): string
    {
        return $this->foodRawTable->updateFoodRaw($rawUuid, $nameDe, $nameEn, $nameScient, $desc, $quantityunitUuid, $gram);
    }

    public function searchFoodRawDbSelect(ListQueryBase $listQueryBase, string $rawName, int $rawCategoryId): Select
    {
        return $this->foodRawTable->searchFoodRawDbSelect($listQueryBase, $rawName, $rawCategoryId);
    }

    public function searchFoodRaw(ListQueryBase $listQueryBase, string $rawName, int $rawCategoryId): array
    {
        return $this->foodRawTable->searchFoodRaw($listQueryBase, $rawName, $rawCategoryId);
    }

    public function searchFoodRawForRecipe(ListQueryBase $listQueryBase, string $recipeUuid, string $rawName, int $rawCategoryId): array
    {
        return $this->foodRawTable->searchFoodRawForRecipe($listQueryBase, $recipeUuid, $rawName, $rawCategoryId);
    }

    public function getFoodRawAdapter(): Adapter
    {
        return $this->foodRawTable->getAdapter();
    }

    public function getIngrdnts(string $foodRawUuid, array $recipeIngrdntMapping): array
    {
        return $this->foodRawPartTable->getFoodRawPartsForFoodRaw($foodRawUuid, $recipeIngrdntMapping);
    }
}
