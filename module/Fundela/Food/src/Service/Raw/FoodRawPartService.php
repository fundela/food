<?php

namespace Fundela\Food\Service\Raw;

use Fundela\Trinket\Service\AbstractService;
use Fundela\Food\Table\Raw\FoodRawPartTable;
use Fundela\Food\Table\Raw\FoodRawTable;

class FoodRawPartService extends AbstractService
{

    protected FoodRawTable $foodRawTable;
    protected FoodRawPartTable $foodRawPartTable;

    /**
     * @param FoodRawTable $foodRawTable
     */
    public function setFoodRawTable(FoodRawTable $foodRawTable): void
    {
        $this->foodRawTable = $foodRawTable;
    }

    /**
     * @param FoodRawPartTable $foodRawPartTable
     */
    public function setFoodRawPartTable(FoodRawPartTable $foodRawPartTable): void
    {
        $this->foodRawPartTable = $foodRawPartTable;
    }

    public function getFoodRawPartsForFoodRaw(string $rawUuid): array
    {
        return $this->foodRawPartTable->getFoodRawPartsForFoodRaw($rawUuid);
    }

    /**
     * @param string $rawUuid
     * @param int $ingrdntId
     * @param float $quantity
     * @return string An empty string if already exist
     */
    public function insertFoodRawPart(string $rawUuid, int $ingrdntId, float $quantity):string
    {
        if($this->foodRawPartTable->existFoodRawPartForFoodRaw($rawUuid, $ingrdntId)) {
            return '';
        }
        return $this->foodRawPartTable->insertFoodRawPart($rawUuid, $ingrdntId, $quantity);
    }

    public function updateFoodRawPartQuantity(string $partUuid, float $partQntty): bool
    {
        return $this->foodRawPartTable->updateFoodRawPartQuantity($partUuid, $partQntty) >= 0;
    }
}
