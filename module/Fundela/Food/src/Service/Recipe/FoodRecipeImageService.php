<?php

namespace Fundela\Food\Service\Recipe;

use Fundela\Trinket\Service\AbstractService;
use Fundela\Food\Table\Recipe\FoodRecipeImageRelTable;
use Fundela\Image\Service\ImageService;
use Fundela\Image\Tool\ImageScale;
use Laminas\Db\Adapter\Driver\AbstractConnection;
use Laminas\Log\Logger;

class FoodRecipeImageService extends AbstractService
{
    protected ImageService $imageService;
    protected FoodRecipeImageRelTable $foodRecipeImageRelTable;

    public function setImageService(ImageService $imageService): void
    {
        $this->imageService = $imageService;
    }

    public function setFoodRecipeImageRelTable(FoodRecipeImageRelTable $foodRecipeImageRelTable): void
    {
        $this->foodRecipeImageRelTable = $foodRecipeImageRelTable;
    }

    /**
     * @param array $imageFormData Data from \Fundela\Image\Form\ImageForm
     * @param string $recipeUuid
     * @return int -1 on error ELSE the new imageID.
     */
    public function insertAndUploadRecipeImage(array $imageFormData, string $recipeUuid): int
    {
        if (($imageId = $this->imageService->insertAndUploadImageWithStandardScalings($imageFormData)) <= 0) {
            $this->message = $this->imageService->getMessage();
            return -1;
        }
        if ($this->foodRecipeImageRelTable->insertFoodRecipeImage($recipeUuid, $imageId) <= 0) {
            $this->imageService->deleteImageComplete($imageId);
            $this->message = 'Error while save the recipe image';
            return -1;
        }
        return $imageId;
    }

    public function getFoodRecipeImages(string $recipeUuid): array
    {
        $images = $this->foodRecipeImageRelTable->getFoodRecipeImages($recipeUuid);
        foreach ($images as &$image) {
            $image['image_rel_path'] = $this->imageService->getImageRelativePath($image['image_time_create'], $image['image_id'],
                $image['image_filename'] . '.' . $image['image_ext']);
            $relPath = substr($image['image_rel_path'], 0, strrpos($image['image_rel_path'], '/') + 1);
            $m2ss = json_decode($image['image_scaling'], true);
            if (empty($m2ss['m2s']) || !is_array($m2ss['m2s'])) {
                continue;
            }
            $image['image_rel_paths_m2s'] = [];
            foreach ($m2ss['m2s'] as $m2s) {
                $image['image_rel_paths_m2s'][$m2s] = $relPath . $image['image_filename'] . '_'
                    . ImageScale::KEY_MAGIC_TWO_SQUARE . '_' . $m2s . '.' . $image['image_ext'];
            }
        }
        return $images;
    }
    public function deleteImageRel(string $recipeUuid, int $imageId): bool
    {
        if($this->foodRecipeImageRelTable->delete(['food_recipe_uuid' => $recipeUuid, 'image_id' => $imageId]) < 1) {
            return false;
        }
        return true;
    }

    /**
     * Wrapper function for \Fundela\Image\Service\ImageService()->deleteImageComplete(int $imageId).
     * Delete an image complete (db.image and the imageId folder).
     * @param int $imageId
     * @return bool
     */
    public function deleteImageComplete(int $imageId): bool
    {
        return $this->imageService->deleteImageComplete($imageId);
    }
}
