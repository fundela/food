<?php

namespace Fundela\Food\Service\Recipe;

use Fundela\Food\Service\Raw\FoodRawService;
use Fundela\Food\Table\Recipe\FoodRecipeCategoryRelTable;
use Fundela\Food\Table\Recipe\FoodRecipeCategoryTable;
use Fundela\Food\Table\Recipe\FoodRecipePartTable;
use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Filter\BlNl2NbspBrFilter;
use Fundela\Trinket\Service\AbstractService;
use Fundela\Food\Table\Recipe\FoodRecipeTable;
use Fundela\Trinket\Tool\StringTool;
use Fundela\User\Service\UserService;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Sql;

class FoodRecipeService extends AbstractService
{
    protected FoodRecipeTable $foodRecipeTable;
    protected FoodRecipePartTable $foodRecipePartTable;
    protected array $recipeIngrdntIdMapping;
    protected FoodRawService $foodRawService;
    protected FoodRecipeCategoryTable $foodRecipeCategoryTable;
    protected FoodRecipeCategoryRelTable $foodRecipeCategoryRelTable;
    protected UserService $userService;

    public function setFoodRecipeTable(FoodRecipeTable $foodRecipeTable): void
    {
        $this->foodRecipeTable = $foodRecipeTable;
    }

    public function getFoodRecipeTableSql(): Sql
    {
        return $this->foodRecipeTable->getSql();
    }

    public function setFoodRecipePartTable(FoodRecipePartTable $foodRecipePartTable): void
    {
        $this->foodRecipePartTable = $foodRecipePartTable;
    }

    public function setRecipeIngrdntIdMapping(array $recipeIngrdntIdMapping): void
    {
        $this->recipeIngrdntIdMapping = $recipeIngrdntIdMapping;
    }

    public function getRecipeIngrdntIdMapping(): array
    {
        return $this->recipeIngrdntIdMapping;
    }

    public function setFoodRawService(FoodRawService $foodRawService): void
    {
        $this->foodRawService = $foodRawService;
    }

    public function setFoodRecipeCategoryTable(FoodRecipeCategoryTable $foodRecipeCategoryTable): void
    {
        $this->foodRecipeCategoryTable = $foodRecipeCategoryTable;
    }

    public function setFoodRecipeCategoryRelTable(FoodRecipeCategoryRelTable $foodRecipeCategoryRelTable): void
    {
        $this->foodRecipeCategoryRelTable = $foodRecipeCategoryRelTable;
    }

    public function setUserService(UserService $userService): void
    {
        $this->userService = $userService;
    }

    /**
     * @param string $recipeUuid
     * @param int $categoryId
     * @return string
     */
    public function insertFoodRecipeCategory(string $recipeUuid, int $categoryId): string
    {
        if ($this->foodRecipeCategoryRelTable->existFoodRecipeCategoryRel($recipeUuid, $categoryId)) {
            return 'exist';
        }
        return $this->foodRecipeCategoryRelTable->insertFoodRecipeCategoryRel($recipeUuid, $categoryId);
    }

    public function deleteFoodRecipeCategoryRel(int $recipeCategoryRelId): int
    {
        return $this->foodRecipeCategoryRelTable->deleteFoodRecipeCategoryRel($recipeCategoryRelId);
    }

    /**
     * @param bool $withX With a pre 'x' at the ID from IdAssoc?
     * @return array
     */
    public function getFoodRecipeCategoryIdAssoc(bool $withX = false): array
    {
        return $this->foodRecipeCategoryTable->getFoodRecipeCategoryIdAssoc($withX);
    }

    /**
     * @param string $recipeCategoryId
     * @return array
     */
    public function getFoodRecipesForCategory(string $recipeCategoryId): array
    {
        return $this->foodRecipeCategoryRelTable->getFoodRecipesForCategory($recipeCategoryId);
    }

    public function getFoodRecipeCategories(string $recipeUuid): array
    {
        return $this->foodRecipeCategoryRelTable->getFoodRecipeCategories($recipeUuid);
    }

    public function getFoodRecipeCategoriesRest(string $recipeUuid): array
    {
        return $this->foodRecipeCategoryTable->getFoodRecipeCategoriesRest($recipeUuid);
    }

    /**
     * @param string $recipeUuid
     * @return array FoodRecipe without JOINs.
     */
    public function getFoodRecipe(string $recipeUuid): array
    {
        return $this->foodRecipeTable->getFoodRecipe($recipeUuid);
    }

    public function getFoodRecipesAll(): array
    {
        return $this->foodRecipeTable->getFoodRecipesAll();
    }

    public function getFoodRecipeForHTML(string $recipeUuid): array
    {
        $r = $this->foodRecipeTable->getFoodRecipe($recipeUuid);
        $r['food_recipe_instrctn'] = (new BlNl2NbspBrFilter())->filter($r['food_recipe_instrctn']);
        return $r;
    }

    public function getFoodRecipeByAlias(string $recipeAlias): array
    {
        return $this->foodRecipeTable->getFoodRecipeByAlias($recipeAlias);
    }

    public function insertFoodRecipe(array $formData, string $userUuid): string
    {
        return $this->foodRecipeTable->insertFoodRecipe($formData, $userUuid);
    }

    public function updateFoodRecipe(array $formData): string
    {
        return $this->foodRecipeTable->updateFoodRecipe($formData);
    }

    public function insertOrUpdateFoodRecipe(array $formData, string $userUuid): string
    {
        if (!empty($formData['food_recipe_uuid'])) {
            if ($this->updateFoodRecipe($formData) >= 0) {
                return $formData['food_recipe_uuid'];
            }
        } else {
            return $this->insertFoodRecipe($formData, $userUuid);
        }
        return '';
    }

    public function searchFoodRecipes(string $label, int $categoryId, ListQueryBase $listQueryBase, bool $onlyMine = false, string $userUuid = ''): array
    {
        return $this->foodRecipeTable->searchFoodRecipes($label, $categoryId, $listQueryBase, $onlyMine, $userUuid);
    }

    /**
     * @param string $label
     * @param int $categoryId
     * @param ListQueryBase $listQueryBase
     * @param bool $onlyMine
     * @param string $userUuid
     * @return Select E.g. for laminas pagination you need a Select.
     */
    public function searchFoodRecipesSelect(string $label, int $categoryId, ListQueryBase $listQueryBase, bool $onlyMine = false, string $userUuid = ''): Select
    {
        return $this->foodRecipeTable->searchFoodRecipesSelect($label, $categoryId, $listQueryBase, $onlyMine, $userUuid);
    }

    public function checkFoodRecipeUserUuid(string $recipeUuid, string $userUuid): bool
    {
        return $this->foodRecipeTable->checkFoodRecipeUserUuid($recipeUuid, $userUuid);
    }

    public function checkEditAllowed(string $recipeUuid, string $userUuid): bool
    {
        return $this->checkFoodRecipeUserUuid($recipeUuid, $userUuid) || $this->userService->isUserInRole(4, $userUuid);
    }

    /**
     * @param string $recipeUuid
     * @return array
     */
    public function computeFoodRecipeIngrdnts(string $recipeUuid): array
    {
        // parts with joined foodRaws
        $recipeParts = $this->foodRecipePartTable->getFoodRecipePartsForFoodRecipe($recipeUuid);
        // assoc: key = food_recipe_ingrdnt_* ; value = sum of ingrdnt from * parts
        $recipeIngrdnts = [];
        // weight (gram) of the complete recipe
        $recipeGram = 0;
        $flippedMap = array_flip($this->recipeIngrdntIdMapping);
        foreach ($recipeParts as $recipePart) {
            $ingrdnts = $this->foodRawService->getIngrdnts($recipePart['food_raw_uuid'], $this->recipeIngrdntIdMapping);
            if (empty($ingrdnts)) {
                continue;
            }
            $gramPart = $recipePart['food_recipe_part_qntty'] * $recipePart['quantityunit_gram'];
            $recipeGram += $gramPart;
            foreach ($ingrdnts as $ingrdnt) {
                if (!in_array($ingrdnt['food_ingrdnt_id'], $this->recipeIngrdntIdMapping)) {
                    continue;
                }
                $ingrdntId = $flippedMap[$ingrdnt['food_ingrdnt_id']];
                if (!isset($recipeIngrdnts[$ingrdntId])) {
                    $recipeIngrdnts[$ingrdntId] = 0;
                }
                // quantity in recipe
                $qntty = $gramPart * ($ingrdnt['food_raw_part_qntty'] / 100);
                $recipeIngrdnts[$ingrdntId] += $qntty;
            }
        }
        foreach ($recipeIngrdnts as &$ingrdnt) {
            $ingrdnt = ($ingrdnt / $recipeGram) * 100;
        }
        return $recipeIngrdnts;
    }

    public function computeAndSaveFoodRecipeIngrdnts(string $recipeUuid): bool
    {
        return $this->foodRecipeTable->updateFoodRecipeWithStorage($recipeUuid, $this->computeFoodRecipeIngrdnts($recipeUuid));
    }

    public function computeFoodRecipeAliasUnique(string $title): string
    {
        if ($this->foodRecipeTable->existFoodRecipeAlias($alias = StringTool::computeAlias($title))) {
            return $this->computeFoodRecipeAliasUnique($title . rand(1, 999999));
        }
        return $alias;
    }
}
