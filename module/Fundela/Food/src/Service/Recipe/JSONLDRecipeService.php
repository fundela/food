<?php
/**
 * https://github.com/lanthaler/JsonLD
 * or
 * https://github.com/digitalbazaar/php-json-ld
 */

namespace Fundela\Food\Service\Recipe;

use Fundela\Food\Entity\FoodRecipeEntity;
use Fundela\Food\Entity\FoodRecipePartEntity;
use Fundela\Image\View\ImageSrc;
use Fundela\Trinket\Service\AbstractService;

/**
 * Create JSON-LD Data String for a recipe.
 * https://schema.org/Recipe
 */
class JSONLDRecipeService extends AbstractService
{
    protected array $jsonLdData = [];
    protected string $jsonLdString = '';
    protected ImageSrc $imageSrc;
    protected FoodRecipeEntity $recipe;

    /**
     * @var FoodRecipePartEntity[]
     */
    protected array $parts;
    protected array $image;
    protected string $basUrl;

    public function setImageSrc(ImageSrc $imageSrc): void
    {
        $this->imageSrc = $imageSrc;
    }

    public function setRecipe(FoodRecipeEntity $recipe): void
    {
        $this->recipe = $recipe;
    }

    /**
     * @param FoodRecipePartEntity[] $parts
     * @return void
     */
    public function setParts(array $parts): void
    {
        $this->parts = $parts;
    }

    public function setImage(array $image, string $basUrl): void
    {
        $this->image = $image;
        $this->basUrl = $basUrl;
    }

    public function computeJsonLdData(): void
    {
        $parts = [];
        foreach ($this->parts as $part) {
            $parts[] = '' . $part;
        }
        $imageUrl = call_user_func_array($this->imageSrc, [$this->image ?? [], ['m2s' => 1024]]);
        $this->jsonLdData = [
            '@context'           => 'https://schema.org',
            '@type'              => 'Recipe',
            'author'             => 'Open Nutrition Recipes',
            'name'               => $this->recipe->getFoodRecipeLabel(),
            'description'        => $this->recipe->getFoodRecipeMetaDesc(),
            'recipeInstructions' => html_entity_decode(str_replace("\r", '', $this->recipe->getFoodRecipeInstrctn()), ENT_COMPAT, 'UTF-8'),
            'prepTime'           => $this->recipe->getFoodRecipePrepTime(),
            'cookTime'           => $this->recipe->getFoodRecipeCookTime(),
            'recipeYield'        => $this->recipe->getFoodRecipeYield(),
            'datePublished'      => substr($this->recipe->getFoodRecipeTimeCreate(), 0, 10),
            'image'              => $this->basUrl . $imageUrl,
            'recipeIngredient'   => $parts,
            'nutrition'          => [
                '@type'               => 'NutritionInformation',
                "calories"            => "240 kJ / 100 g",
                "fatContent"          => "34 g Fett / 100 g",
                "carbohydrateContent" => "18 g Kohlehydrate / 100 g",
                "proteinContent"      => "10 g Eiweiss / 100 g",
                "fiberContent"        => "20 g Ballaststoffe / 100 g",
            ],
        ];
        $this->jsonLdString = json_encode($this->jsonLdData, JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
    }

    public function getJsonLdString(): string
    {
        return $this->jsonLdString;
    }

    public function getJsonLdStringWithTag(): string
    {
        return '<script type="application/ld+json">' . $this->getJsonLdString() . '</script>';
    }
}
