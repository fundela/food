<?php

namespace Fundela\Food\Service\Recipe;

use Fundela\Food\Table\Recipe\FoodRecipePartTable;
use Fundela\Food\Table\Recipe\FoodRecipeTable;
use Fundela\Trinket\Service\AbstractService;
use Laminas\Db\Adapter\Driver\ConnectionInterface;
use Laminas\Validator\Uuid;

class FoodRecipePartService extends AbstractService
{
    protected FoodRecipePartTable $foodRecipePartTable;
    protected FoodRecipeTable $foodRecipeTable;
    protected FoodRecipeService $foodRecipeService;

    public function setFoodRecipePartTable(FoodRecipePartTable $foodRecipePartTable): void
    {
        $this->foodRecipePartTable = $foodRecipePartTable;
    }

    public function setFoodRecipeTable(FoodRecipeTable $foodRecipeTable): void
    {
        $this->foodRecipeTable = $foodRecipeTable;
    }

    public function setFoodRecipeService(FoodRecipeService $foodRecipeService): void
    {
        $this->foodRecipeService = $foodRecipeService;
    }

    /**
     * @param string $recipeUuid
     * @return array
     */
    public function getFoodRecipePartsForFoodRecipe(string $recipeUuid): array
    {
        return $this->foodRecipePartTable->getFoodRecipePartsForFoodRecipe($recipeUuid);
    }

    /**
     * @param string $recipeUuid
     * @return array From view 'view_recipe_part'.
     */
    public function getFoodRecipeParts(string $recipeUuid): array
    {
        return $this->foodRecipePartTable->getFoodRecipeParts($recipeUuid);
    }

    /**
     * @param array $formData
     * @return string
     */
    public function insertFoodRecipePart(array $formData): string
    {
        return $this->foodRecipePartTable->insertFoodRecipePart($formData);
    }

    public function getFoodRecipePart(string $recipePartUuid): array
    {
        return $this->foodRecipePartTable->getFoodRecipePart($recipePartUuid);
    }

    public function updateFoodRecipePartOrderPriority(string $recipePartUuidsCsv): bool
    {
        $uuids = explode(',', $recipePartUuidsCsv);
        if (!$uuids || !is_array($uuids)) {
            return false;
        }
        $uuidV = new Uuid();
        $ordPrio = 1000;
        foreach ($uuids as $uuid) {
            if (!$uuidV->isValid($uuid)) {
                return false;
            }
            if (!$this->foodRecipePartTable->updateFoodRecipePartOrderPriority($uuid, $ordPrio)) {
                return false;
            }
            $ordPrio -= 10;
        }
        return true;
    }

    protected function beginTransaction(): ConnectionInterface
    {
        $adapter = $this->foodRecipePartTable->getAdapter();
        $conn = $adapter->getDriver()->getConnection();
        return $conn->beginTransaction();
    }

    public function deleteFoodRecipePart(string $recipePartUuid): bool
    {
        $conn = $this->beginTransaction();
        $part = $this->foodRecipePartTable->getFoodRecipePart($recipePartUuid);
        if (!empty($part) && $this->foodRecipePartTable->deleteFoodRecipePart($recipePartUuid) > 0) {
            if (!$this->foodRecipeService->computeAndSaveFoodRecipeIngrdnts($part['food_recipe_uuid'])) {
                $conn->rollback();
                return false;
            }
        } else {
            $conn->rollback();
            return false;
        }
        $conn->commit();
        return true;
    }

    public function updateFoodRecipePartQuantity(string $recipePartUuid, float $recipePartQuantity): bool
    {
        $conn = $this->beginTransaction();
        if (!$this->foodRecipePartTable->updateFoodRecipePartQuantity($recipePartUuid, $recipePartQuantity)) {
            $conn->rollback();
            return false;
        }
        $part = $this->foodRecipePartTable->getFoodRecipePart($recipePartUuid);
        if (!empty($part) && is_array($part)) {
            if (!$this->foodRecipeService->computeAndSaveFoodRecipeIngrdnts($part['food_recipe_uuid'])) {
                $conn->rollback();
                return false;
            }
        } else {
            $conn->rollback();
            return false;
        }
        $conn->commit();
        return true;
    }

    public function updateFoodRecipePartComment(string $recipePartUuid, string $comment): bool
    {
        return $this->foodRecipePartTable->updateFoodRecipePartComment($recipePartUuid, $comment);
    }

    /**
     * @param string $userUuid
     * @param string $recipePartUuid
     * @return bool
     */
    public function isUserRecipeOwner(string $userUuid, string $recipePartUuid): bool
    {
        $part = $this->foodRecipePartTable->getFoodRecipePart($recipePartUuid);
        if (empty($part) || !is_array($part)) {
            return false;
        }
        return $part['user_uuid'] == $userUuid;
    }
}
