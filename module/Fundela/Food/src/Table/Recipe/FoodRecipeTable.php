<?php

namespace Fundela\Food\Table\Recipe;

use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;

class FoodRecipeTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_recipe';

    protected $ingrdntMapping = [
        '' => '',
    ];

    /**
     * @param string $recipeUuid
     * @return array From view 'view_recipe'.
     */
    public function getFoodRecipe(string $recipeUuid): array
    {
        $select = new Select('view_recipe');
        try {
            $select->where(['food_recipe_uuid' => $recipeUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $recipeAlias
     * @return array From view 'view_recipe'.
     */
    public function getFoodRecipeByAlias(string $recipeAlias): array
    {
        $select = new Select('view_recipe');
        try {
            $select->where(['food_recipe_alias' => $recipeAlias]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param array $formData \Fundela\Food\Form\FoodRecipeForm data
     * @param string $userUuid
     * @return string
     */
    public function insertFoodRecipe(array $formData, string $userUuid): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'food_recipe_uuid'          => $uuid,
                'food_recipe_label'         => $formData['food_recipe_label'],
                'food_recipe_alias'         => $formData['food_recipe_alias'],
                'food_recipe_instrctn'      => $formData['food_recipe_instrctn'],
                'user_uuid'                 => $userUuid,
                'food_recipe_meta_title'    => $formData['food_recipe_meta_title'],
                'food_recipe_meta_keywords' => $formData['food_recipe_meta_keywords'],
                'food_recipe_meta_desc'     => $formData['food_recipe_meta_desc'],
                'food_recipe_prep_time'     => $formData['food_recipe_prep_time'],
                'food_recipe_cook_time'     => $formData['food_recipe_cook_time'],
                'food_recipe_yield'         => $formData['food_recipe_yield'],
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param array $formData \Fundela\Food\Form\FoodRecipeForm data
     * @return string
     */
    public function updateFoodRecipe(array $formData): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'food_recipe_label'         => $formData['food_recipe_label'],
                'food_recipe_alias'         => $formData['food_recipe_alias'],
                'food_recipe_instrctn'      => $formData['food_recipe_instrctn'],
                'food_recipe_meta_title'    => $formData['food_recipe_meta_title'],
                'food_recipe_meta_keywords' => $formData['food_recipe_meta_keywords'],
                'food_recipe_meta_desc'     => $formData['food_recipe_meta_desc'],
                'food_recipe_prep_time'     => $formData['food_recipe_prep_time'],
                'food_recipe_cook_time'     => $formData['food_recipe_cook_time'],
                'food_recipe_yield'         => $formData['food_recipe_yield'],
            ]);
            $update->where(['food_recipe_uuid' => $formData['food_recipe_uuid']]);
            if ($this->updateWith($update) >= 0) {
                return $formData['food_recipe_uuid'];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $label
     * @param int $categoryId
     * @param ListQueryBase $listQueryBase
     * @return array From view 'view_recipe'.
     */
    public function searchFoodRecipes(string $label, int $categoryId, ListQueryBase $listQueryBase, bool $onlyMine = false, string $userUuid = ''): array
    {
        $select = $this->searchFoodRecipesSelect($label, $categoryId, $listQueryBase, $onlyMine, $userUuid);
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $label
     * @param int $categoryId
     * @param ListQueryBase $listQueryBase
     * @param bool $onlyMine
     * @param string $userUuid
     * @return Select From view 'view_recipe'.
     */
    public function searchFoodRecipesSelect(string $label, int $categoryId, ListQueryBase $listQueryBase, bool $onlyMine = false, string $userUuid = ''): Select
    {
        $select = new Select('view_recipe');
        if ($label) {
            $label = str_replace(' ', '%', strtolower($label));
            $select->where->like(new Expression('LOWER(food_recipe_label)'), "%$label%");
        }
        if ($categoryId) {
            $selectRel = new Select('food_recipe_category_rel');
            $selectRel->columns(['food_recipe_uuid']);
            $selectRel->where(['food_recipe_category_id' => $categoryId]);
            $select->where->in('food_recipe_uuid', $selectRel);
        }
        if ($onlyMine && !empty($userUuid)) {
            $select->where(['user_uuid' => $userUuid]);
        }
        if (!empty($oq = $listQueryBase->getOrderSql())) {
            $select->order($oq);
        } else {
            $select->order('food_recipe_time_create DESC');
        }
        return $select;
    }

    public function checkFoodRecipeUserUuid(string $recipeUuid, string $userUuid): bool
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'food_recipe_uuid' => $recipeUuid,
                'user_uuid'        => $userUuid,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function updateFoodRecipeWithStorage(string $recipeUuid, array $storage): bool
    {
        $update = $this->sql->update();
        try {
            $storage['food_recipe_time_update'] = new Expression('CURRENT_TIMESTAMP');
            $update->set($storage);
            $update->where(['food_recipe_uuid' => $recipeUuid]);
            if ($this->updateWith($update) >= 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function existFoodRecipeAlias(string $recipeAlias): bool
    {
        $select = $this->sql->select();
        try {
            $select->where([
                'food_recipe_alias' => $recipeAlias,
            ]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function getFoodRecipesAll(): array
    {
        $select = new Select('view_recipe');
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
