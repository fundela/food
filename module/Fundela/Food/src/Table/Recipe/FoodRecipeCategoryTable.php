<?php

namespace Fundela\Food\Table\Recipe;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodRecipeCategoryTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_recipe_category';

    /**
     * @param bool $withX
     * @return array ID assoc with key-preceding 'x' so that JS doesn't automatically sort by numbers.
     */
    public function getFoodRecipeCategoryIdAssoc(bool $withX = false): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            $select->order('food_recipe_category_label ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                $x = '';
                if($withX)
                    $x = 'x';
                foreach ($arr as $row) {
                    $assoc[$x . $row['food_recipe_category_id']] = $row['food_recipe_category_label'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }

    public function getFoodRecipeCategoriesRest(string $recipeUuid): array
    {
        $select = $this->sql->select();
        try {
            $selectRels = new Select('food_recipe_category_rel');
            $selectRels->columns(['food_recipe_category_id']);
            $selectRels->where(['food_recipe_uuid' => $recipeUuid]);

            $select->where->notIn('food_recipe_category_id', $selectRels);
            $select->order('food_recipe_category_label ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
