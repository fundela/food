<?php

namespace Fundela\Food\Table\Recipe;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodRecipeCategoryRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_recipe_category_rel';

    /**
     * @param string $recipeCategoryId
     * @return array
     */
    public function getFoodRecipesForCategory(string $recipeCategoryId): array
    {
        $select = $this->sql->select();
        try {
            $select->join('food_recipe_category', 'food_recipe_category.food_recipe_category_id = food_recipe_category_rel.food_recipe_category_id'
                , ['food_recipe_category_label', 'food_recipe_category_desc'], Select::JOIN_LEFT);
            $select->join('food_recipe', 'food_recipe.food_recipe_uuid = food_recipe_category_rel.food_recipe_uuid'
                , ['food_recipe_label', 'food_recipe_alias', 'food_recipe_instrctn', 'user_uuid'
                    , 'food_recipe_ingrdnt_01', 'food_recipe_ingrdnt_02', 'food_recipe_ingrdnt_03', 'food_recipe_ingrdnt_04'
                    , 'food_recipe_meta_title', 'food_recipe_meta_keywords', 'food_recipe_meta_desc'], Select::JOIN_LEFT);
            $select->join('user', 'user.user_uuid = food_recipe.user_uuid'
                , ['user_email', 'user_login', 'user_active'], Select::JOIN_LEFT);
            $select->where(['food_recipe_category_id' => $recipeCategoryId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $recipeUuid
     * @return array
     */
    public function getFoodRecipeCategories(string $recipeUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('food_recipe_category', 'food_recipe_category.food_recipe_category_id = food_recipe_category_rel.food_recipe_category_id'
                , ['food_recipe_category_label', 'food_recipe_category_desc'], Select::JOIN_LEFT);
            $select->where(['food_recipe_uuid' => $recipeUuid]);
            $select->order('food_recipe_category_label ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFoodRecipeCategoryRel(string $recipeUuid, int $categoryId): bool
    {
        return $this->select(['food_recipe_uuid' => $recipeUuid, 'food_recipe_category_id' => $categoryId])->count() > 0;
    }

    /**
     * @param string $recipeUuid
     * @param int $categoryId
     * @return int The new ID on success and 'exist' if it is already exist and -1 on failure.
     * Insert FoodRecipeCategoryRel only if it not exist.
     */
    public function insertFoodRecipeCategoryRel(string $recipeUuid, int $categoryId): int
    {
        if($this->existFoodRecipeCategoryRel($recipeUuid,  $categoryId)) {
            return 'exist';
        }
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'food_recipe_category_id' => $categoryId,
                'food_recipe_uuid' => $recipeUuid,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $this->getAdapter()->getDriver()->getConnection()
                    ->getLastGeneratedValue('public.food_recipe_category_rel_food_recipe_category_rel_id_seq');
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function deleteFoodRecipeCategoryRel(int $recipeCategoryRelId): int
    {
        return $this->delete(['food_recipe_category_rel_id' => $recipeCategoryRelId]);
    }
}
