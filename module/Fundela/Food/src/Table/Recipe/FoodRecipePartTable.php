<?php

namespace Fundela\Food\Table\Recipe;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Predicate\Predicate;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodRecipePartTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_recipe_part';

    /**
     * @param string $recipePartUuid
     * @return array JOINed db.food_recipe
     */
    public function getFoodRecipePart(string $recipePartUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('food_recipe', 'food_recipe.food_recipe_uuid = food_recipe_part.food_recipe_uuid'
                , ['food_recipe_label', 'food_recipe_alias', 'user_uuid'], Select::JOIN_LEFT);
            $select->where(['food_recipe_part_uuid' => $recipePartUuid]);
            $q = $select->getSqlString($this->getAdapter()->getPlatform());
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() == 1) {
                return $result->toArray()[0];
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $recipeUuid
     * @return array food_recipe_part JOIN food_raw JOIN quantityunit
     */
    public function getFoodRecipePartsForFoodRecipe(string $recipeUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->join('food_raw', 'food_raw.food_raw_uuid=food_recipe_part.food_raw_uuid'
                , ['food_raw_name_de', 'food_raw_name_en', 'food_raw_name_scient', 'food_raw_desc', 'quantityunit_uuid', 'quantityunit_gram']
                , Select::JOIN_LEFT);
            $select->join('quantityunit', 'quantityunit.quantityunit_uuid = food_raw.quantityunit_uuid'
                , ['quantityunit_label'], Select::JOIN_LEFT);
            $select->where(['food_recipe_uuid' => $recipeUuid]);
            $select->order('food_recipe_part_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    /**
     * @param string $recipeUuid
     * @return array From view 'view_recipe_part'.
     */
    public function getFoodRecipeParts(string $recipeUuid): array
    {
        $select = new Select('view_recipe_part');
        try {
            $select->where(['food_recipe_uuid' => $recipeUuid]);
            $select->order('food_recipe_part_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFoodRecipePart(array $formData): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'food_recipe_part_uuid' => $uuid,
                'food_recipe_uuid' => $formData['food_recipe_uuid'],
                'food_raw_uuid' => $formData['food_raw_uuid'],
                'food_recipe_part_qntty' => $formData['food_recipe_part_qntty'],
                'food_recipe_part_comment' => $formData['food_recipe_part_comment'],
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function updateFoodRecipePartOrderPriority(string $recipePartUuid, int $orderPriority): bool
    {
        $update = $this->sql->update();
        try {
            $update->set(['food_recipe_part_order_priority' => $orderPriority]);
            $update->where(['food_recipe_part_uuid' => $recipePartUuid]);
            if ($this->updateWith($update) >= 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function updateFoodRecipePartQuantity(string $recipePartUuid, float $recipePartQuantity): bool
    {
        $update = $this->sql->update();
        try {
            $update->set(['food_recipe_part_qntty' => $recipePartQuantity]);
            $update->where(['food_recipe_part_uuid' => $recipePartUuid]);
            if ($this->updateWith($update) >= 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function updateFoodRecipePartComment(string $recipePartUuid, string $comment): bool
    {
        $update = $this->sql->update();
        try {
            $update->set(['food_recipe_part_comment' => $comment]);
            $update->where(['food_recipe_part_uuid' => $recipePartUuid]);
            if ($this->updateWith($update) >= 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function deleteFoodRecipePart(string $recipePartUuid): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['food_recipe_part_uuid' => $recipePartUuid]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
