<?php

namespace Fundela\Food\Table\Recipe;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodRecipeImageRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_recipe_image_rel';

    /**
     * @param string $recipeUuid
     * @return array
     */
    public function getFoodRecipeImages(string $recipeUuid)
    {
        $select = $this->sql->select();
        try {
            $select->columns(['food_recipe_image_rel_id', 'food_recipe_image_rel_order_priority']);
            $select->join('food_recipe', 'food_recipe.food_recipe_uuid = food_recipe_image_rel.food_recipe_uuid'
                , Select::SQL_STAR, Select::JOIN_LEFT);
            $select->join('image', 'image.image_id = food_recipe_image_rel.image_id'
                , Select::SQL_STAR, Select::JOIN_LEFT);
            $select->where(['food_recipe_image_rel.food_recipe_uuid' => $recipeUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFoodRecipeImage(string $recipeUuid, string $imageId, int $orderPriority = 0): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'food_recipe_uuid' => $recipeUuid,
                'image_id' => $imageId,
                'food_recipe_image_rel_order_priority' => $orderPriority,
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $this->getAdapter()->getDriver()->getConnection()
                    ->getLastGeneratedValue('public.food_recipe_image_rel_food_recipe_image_rel_id_seq');
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
