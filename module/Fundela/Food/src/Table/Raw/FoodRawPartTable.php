<?php

namespace Fundela\Food\Table\Raw;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodRawPartTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_raw_part';

    /**
     * @param string $foodRawUuid
     * @param array $ingrdntIds
     * @return array
     */
    public function getFoodRawPartsForFoodRaw(string $foodRawUuid, array $ingrdntIds = []): array
    {
        $select = $this->sql->select();
        try {
            $select->join('food_ingrdnt', 'food_ingrdnt.food_ingrdnt_id = food_raw_part.food_ingrdnt_id'
                , ['food_ingrdnt_name_de', 'food_ingrdnt_name_en', 'food_ingrdnt_name_scient', 'food_ingrdnt_require', 'food_ingrdnt_order_priority']);
            $select->where(['food_raw_uuid' => $foodRawUuid]);
            if(!empty($ingrdntIds)) {
                $select->where->in('food_raw_part.food_ingrdnt_id', $ingrdntIds);
            }
            $select->order('food_ingrdnt_order_priority DESC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function existFoodRawPartForFoodRaw(string $foodRawUuid, int $ingrdntId): bool
    {
        $select = $this->sql->select();
        try {
            $select->where(['food_raw_uuid' => $foodRawUuid, 'food_ingrdnt_id' => $ingrdntId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return true;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return false;
    }

    public function insertFoodRawPart(string $rawUuid, int $ingrdntId, float $quantity): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'food_raw_part_uuid' => $uuid,
                'food_raw_uuid' => $rawUuid,
                'food_ingrdnt_id' => $ingrdntId,
                'food_raw_part_qntty' => $quantity,
            ]);
            if ($this->insertWith($insert) > 0) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function deleteFoodRawPartsForFoodIngrdnt(int $ingrdntId): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['food_ingrdnt_id' => $ingrdntId]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFoodRawPartQuantity(string $partUuid, float $partQntty): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'food_raw_part_qntty' => $partQntty
            ]);
            $update->where(['food_raw_part_uuid' => $partUuid]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }
}
