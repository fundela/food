<?php

namespace Fundela\Food\Table\Raw;

use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Predicate\Expression;
use Laminas\Db\Sql\Select;

class FoodRawTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_raw';

    /**
     * @param string $foodRawUuid
     * @return array
     */
    public function getFoodRaw(string $foodRawUuid): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['food_raw_uuid' => $foodRawUuid]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFoodRaw(string $nameDe, string $nameEn, string $nameScient, string $desc, string $quantityunitUuid, float $gram): string
    {
        $insert = $this->sql->insert();
        $uuid = $this->uuid();
        try {
            $insert->values([
                'food_raw_uuid' => $uuid,
                'food_raw_name_de' => $nameDe,
                'food_raw_name_en' => $nameEn,
                'food_raw_name_scient' => $nameScient,
                'food_raw_desc' => $desc,
                'quantityunit_uuid' => $quantityunitUuid,
                'quantityunit_gram' => $gram,
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $uuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    /**
     * @param string $rawUuid
     * @param string $nameDe
     * @param string $nameEn
     * @param string $nameScient
     * @param string $desc
     * @param string $quantityunitUuid
     * @param float $gram
     * @return string The foot_raw_uuid, also if updateResult == 0. Only if an error then returns an empty string.
     */
    public function updateFoodRaw(string $rawUuid, string $nameDe, string $nameEn, string $nameScient, string $desc, string $quantityunitUuid, float $gram): string
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'food_raw_name_de' => $nameDe,
                'food_raw_name_en' => $nameEn,
                'food_raw_name_scient' => $nameScient,
                'food_raw_desc' => $desc,
                'quantityunit_uuid' => $quantityunitUuid,
                'quantityunit_gram' => $gram,
            ]);
            $update->where(['food_raw_uuid' => $rawUuid]);
            /** @var HydratingResultSet $result */
            if ($this->updateWith($update) >= 0) {
                return $rawUuid;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return '';
    }

    public function searchFoodRawDbSelect(ListQueryBase $listQueryBase, string $rawName, int $rawCategoryId): Select
    {
        $select = $this->sql->select();
        try {
            $this->computeListQueryBase($select, $listQueryBase);
            $select->join('quantityunit', 'quantityunit.quantityunit_uuid = food_raw.quantityunit_uuid', ['quantityunit_label'], Select::JOIN_LEFT);
            if (!empty($rawName)) {
                $rawName = strtolower($rawName);
                $select->where->like(new Expression('LOWER(food_raw_name_de)'), "%$rawName%")
                    ->OR->like(new Expression('LOWER(food_raw_name_en)'), "%$rawName%")
                    ->OR->like(new Expression('LOWER(food_raw_name_scient)'), "%$rawName%");
            }
            if(!empty($rawCategoryId)) {
                $selectCat = new Select('food_raw_category_rel');
                $selectCat->columns(['food_raw_uuid']);
                $selectCat->where(['food_raw_category_id' => $rawCategoryId]);
                $select->where->in('food_raw_uuid', $selectCat);
            }
            $select->order('food_raw_name_de ASC');
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $select;
    }

    public function searchFoodRaw(ListQueryBase $listQueryBase, string $rawName, int $rawCategoryId): array
    {
        $select = $this->searchFoodRawDbSelect($listQueryBase, $rawName, $rawCategoryId);
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if(!$result->valid()) {
            return [];
        }
        return $result->toArray();
    }

    public function searchFoodRawForRecipe(ListQueryBase $listQueryBase, string $recipeUuid, string $rawName, int $rawCategoryId): array
    {
        $select = $this->searchFoodRawDbSelect($listQueryBase, $rawName, $rawCategoryId);
        $selectFoodRawUuid = new Select('food_recipe_part');
        $selectFoodRawUuid->columns(['food_raw_uuid']);
        $selectFoodRawUuid->where(['food_recipe_uuid' => $recipeUuid]);
        $select->where->notIn('food_raw_uuid', $selectFoodRawUuid);
        /** @var HydratingResultSet $result */
        $result = $this->selectWith($select);
        if(!$result->valid()) {
            return [];
        }
        return $result->toArray();
    }
}
