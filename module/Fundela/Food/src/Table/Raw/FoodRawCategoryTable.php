<?php

namespace Fundela\Food\Table\Raw;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodRawCategoryTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_raw_category';

    /**
     * @return array ID assoc with key-preceding 'x' so that JS doesn't automatically sort by numbers.
     */
    public function getFoodRawCategoryIdAssoc(): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            $select->order('food_raw_category_label ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                foreach ($arr as $row) {
                    $assoc['x' . $row['food_raw_category_id']] = $row['food_raw_category_label'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }
}
