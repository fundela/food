<?php

namespace Fundela\Food\Table\Raw;

use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodRawCategoryRelTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_raw_category_rel';

    /**
     * @param int $rawCategoryId
     * @return array
     */
    public function getFoodRawsForCategory(int $rawCategoryId): array
    {
        $select = $this->sql->select();
        try {
            $select->join('food_raw_category', 'food_raw_category.food_raw_category_id = food_raw_category_rel.food_raw_category_id'
                , ['food_raw_category_label', 'food_raw_category_desc'], Select::JOIN_LEFT);
            $select->join('food_raw', 'food_raw.food_raw_uuid = food_raw_category_rel.food_raw_uuid'
                , ['food_raw_name_de', 'food_raw_name_en', 'food_raw_name_scient', 'food_raw_desc', 'quantityunit_uuid', 'quantityunit_gram']
                , Select::JOIN_LEFT);
            $select->join('quantityunit', 'quantityunit.quantityunit_uuid = food_raw.quantityunit_uuid'
                , ['quantityunit_label', 'quantityunit_resolution', 'quantityunit_resolution_origin'], Select::JOIN_LEFT);
            $select->where(['food_raw_category_id' => $rawCategoryId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }
}
