<?php

namespace Fundela\Food\Table\Ingrdnt;

use Fundela\Trinket\Entity\Database\ListQueryBase;
use Fundela\Trinket\Table\AbstractLibTable;
use Laminas\Db\ResultSet\HydratingResultSet;
use Laminas\Db\Sql\Delete;
use Laminas\Db\Sql\Expression;
use Laminas\Db\Sql\Select;
use Laminas\Db\Sql\Update;
use Laminas\Db\Sql\Where;

class FoodIngrdntTable extends AbstractLibTable
{
    /** @var string */
    protected $table = 'food_ingrdnt';

    /**
     * @param string $foodIngrdntId
     * @return array
     */
    public function getFoodIngrdnt(string $foodIngrdntId): array
    {
        $select = $this->sql->select();
        try {
            $select->where(['food_ingrdnt_id' => $foodIngrdntId]);
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->current()->getArrayCopy();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function insertFoodIngrdnt(string $nameDe, string $nameEn, string $nameScient, bool $require): int
    {
        $insert = $this->sql->insert();
        try {
            $insert->values([
                'food_ingrdnt_name_de' => $nameDe,
                'food_ingrdnt_name_en' => $nameEn,
                'food_ingrdnt_name_scient' => $nameScient,
                'food_ingrdnt_require' => $require,
            ]);
            /** @var HydratingResultSet $result */
            if ($this->insertWith($insert) == 1) {
                return $this->getAdapter()->getDriver()->getConnection()->getLastGeneratedValue('public.food_ingrdnt_food_ingrdnt_id_seq');
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFoodIngrdnt(int $id, string $nameDe, string $nameEn, string $nameScient, bool $require): int
    {
        $update = $this->sql->update();
        try {
            $update->set([
                'food_ingrdnt_name_de' => $nameDe,
                'food_ingrdnt_name_en' => $nameEn,
                'food_ingrdnt_name_scient' => $nameScient,
                'food_ingrdnt_require' => $require,
            ]);
            $update->where(['food_ingrdnt_id' => $id]);
            /** @var HydratingResultSet $result */
            if ($this->updateWith($update) == 1) {
                return $id;
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    /**
     * @param ListQueryBase $listQueryBase
     * @param string $ingrdntName
     * @param string $rawUuid
     * @return array|Select
     */
    public function searchFoodIngrdnt(ListQueryBase $listQueryBase, string $ingrdntName, string $rawUuid = '')
    {
        $select = $this->searchFoodIngrdntDbSelect($listQueryBase, $ingrdntName, $rawUuid);
        try {
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                return $result->toArray();
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return [];
    }

    public function searchFoodIngrdntDbSelect(ListQueryBase $listQueryBase, string $ingrdntName, string $rawUuid = ''): Select
    {
        $select = $this->sql->select();
        try {
            $this->computeListQueryBase($select, $listQueryBase);
            if (!empty($ingrdntName)) {
                $ingrdntName = strtolower($ingrdntName);
                $select->where->like(new Expression('LOWER(food_ingrdnt_name_de)'), "%$ingrdntName%")
                    ->OR->like(new Expression('LOWER(food_ingrdnt_name_en)'), "%$ingrdntName%")
                    ->OR->like(new Expression('LOWER(food_ingrdnt_name_scient)'), "%$ingrdntName%");
            }
            if (!empty($rawUuid)) {
                $selectRaw = new Select('food_raw_part');
                $selectRaw->columns(['food_ingrdnt_id']);
                $selectRaw->where([
                    'food_raw_uuid' => $rawUuid,
                    'food_ingrdnt_id' => new Expression('food_ingrdnt.food_ingrdnt_id')
                ]);
                $select->where->notIn('food_ingrdnt_id', $selectRaw);
            }
            $select->order('food_ingrdnt_order_priority DESC');
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $select;
    }

    public function deleteFoodIngrdnt(int $ingrdntId): int
    {
        $delete = $this->sql->delete();
        try {
            $delete->where(['food_ingrdnt_id' => $ingrdntId]);
            return $this->deleteWith($delete);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function updateFoodIngrdntPriority(int $id, int $prio): int
    {
        $update = $this->sql->update();
        try {
            $update->set(['food_ingrdnt_order_priority' => $prio]);
            $update->where(['food_ingrdnt_id' => $id]);
            return $this->updateWith($update);
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return -1;
    }

    public function getFoodIngrdntsIdAssoc(bool $onlyRequired = true): array
    {
        $select = $this->sql->select();
        $assoc = [];
        try {
            if ($onlyRequired) {
                $select->where(['food_ingrdnt_require' => true]);
            }
            $select->order('food_ingrdnt_order_priority ASC');
            /** @var HydratingResultSet $result */
            $result = $this->selectWith($select);
            if ($result->valid() && $result->count() > 0) {
                $arr = $result->toArray();
                foreach ($arr as $row) {
                    $assoc[$row['food_ingrdnt_id']] = $row['food_ingrdnt_name_de'];
                }
            }
        } catch (\Exception $exception) {
            $this->log($exception, __CLASS__, __FUNCTION__);
        }
        return $assoc;
    }
}
