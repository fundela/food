<?php
/**
 * List of enabled modules for this application.
 *
 * This should be an array of module namespaces used in the application.
 */
return [
    'Laminas\I18n\Module',
    'Laminas\Paginator\Adapter\LaminasDb',
    'Laminas\Session',
    'Laminas\Log',
    'Laminas\Db',
    'Laminas\Form',
    'Laminas\Hydrator',
    'Laminas\InputFilter',
    'Laminas\Filter',
    'Laminas\I18n',
    'Laminas\Router',
    'Laminas\Validator',
    'Fundela\Trinket',
    'Fundela\User',
    'Fundela\Mail',
    'Fundela\Image',
    'Fundela\Food',
    'Fundela\FoodFront',
    'Fundela\FoodBack',
    'Fundela\FoodAdmin',
    'Fundela\FoodApi',
    'Application',
];
