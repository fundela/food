<?php

/**
 * Default DBConfig
 */
$dbParams = array(
    'hostname' => '127.0.0.1',
    'database' => 'food',
    'username' => 'postgres',
    'password' => 'secret',
);

/**
 * Secondary DBConfig
 */
//$dbParamsSecondary = array(
//    'hostname' => '127.0.0.1',
//    'database' => 'db_name_mysql',
//    'username' => 'root',
//    'password' => 'secret',
//);

return array(
//    'service_manager' => array(
//        'factories' => array(
//            'Laminas\Db\Adapter\Adapter' => function ($sm) use ($dbParamsGfmstaff) {
//                return new Laminas\Db\Adapter\Adapter(array(
//                    'driver' => 'Pdo_Mysql', // Pdo_Mysql, pdo auch OK
//                    'dsn' => 'mysql:dbname=' . $dbParamsGlobal['database'] . ';host=' . $dbParamsGlobal['hostname'] . ';charset=utf8',
//                    'database' => $dbParamsGlobal['database'],
//                    'username' => $dbParamsGlobal['username'],
//                    'password' => $dbParamsGlobal['password'],
//                    'hostname' => $dbParamsGlobal['hostname'],
//                ));
//            },
//        ),
//    ),
    'service_manager' => array(
        'abstract_factories' => array(
            \Laminas\Db\Adapter\AdapterAbstractServiceFactory::class,
        ),
    ),
    'db' => array(
        // for primary db adapter that called
        // by $sm->get('Laminas\Db\Adapter\Adapter')
        // required in development mode
//        'username' => $dbParamsGlobal['username'],
//        'password' => $dbParamsGlobal['password'],
//        'driver' => 'Pdo_Mysql',
//        'dsn' => 'mysql:dbname=' . $dbParamsGlobal['database'] . ';host=' . $dbParamsGlobal['hostname'] . ';charset=utf8',
//        'driver_options' => array(
//            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
//        ),
        'adapters' => array(
            'dbDefault' => array(
                'username' => $dbParams['username'],
                'password' => $dbParams['password'],
                'driver' => 'Pdo_Pgsql',
                'dsn' => 'pgsql:dbname=' . $dbParams['database'] . ';host=' . $dbParams['hostname']
            ),
//            'dbSecondary' => array(
//                'username' => $dbParamsSecondary['username'],
//                'password' => $dbParamsSecondary['password'],
//                'driver' => 'Pdo_Mysql',
//                'dsn' => 'mysql:dbname=' . $dbParamsSecondary['database'] . ';host=' . $dbParamsSecondary['hostname'] . ';charset=utf8',
//                'driver_options' => array(
//                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\''
//                ),
//            ),
        ),
    ),
);
