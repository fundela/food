
$(document).ready(function () {
    $('.ffr-sortable').each(function (i, e){
        const ordF = $('input[name="order_field"]').val();
        const ordD = $('input[name="order_direc"]').val();
        if($(e).data('sort') === ordF) {
            switch (ordD) {
                case 'desc':
                    $(e).append('<i class="fas fa-angle-down fa-sort"></i>');
                    break;
                case 'asc':
                default:
                    $(e).append('<i class="fas fa-angle-up fa-sort"></i>');
                    break;
            }
        }
    });

    $('.ffr-sortable').click(function () {
        $('input[name="order_field"]').val($(this).data('sort'));
        const odInput = $('input[name="order_direc"]');
        const odVal = odInput.val();
        $('i.fa-sort').remove();
        switch (odVal) {
            case 'desc':
                odInput.val('asc');
                $(this).append('<i class="fas fa-angle-down fa-sort"></i>');
                break;
            case 'asc':
            default:
                odInput.val('desc');
                $(this).append('<i class="fas fa-angle-up fa-sort"></i>');
                break;
        }
        $('form#ffr-sort').submit();
    });
});
