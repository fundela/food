export const name = 'common-gui';

export function glueFormMessages(messages = []) {
    clearMessagesOnElements();
    messages.forEach((message, index) => {
        const ms = message.split('|#|');
        if (ms.length !== 2) {
            console.log('glueFormMessages has invalid message: ' + message);
        }
        const elem = document.getElementById(ms[0]);
        if (elem) {
            const tempElem = document.createElement('div');
            tempElem.style.color = 'red';
            tempElem.innerText = ms[1];
            tempElem.classList.add('elemessage');
            $(elem).after(tempElem.outerHTML);
            $(elem).css('background-color', 'red');
        }
    });
}

export function clearMessagesOnElements() {
    $('.elemessage').remove();
    $('input').css('background-color', '#777777');
    $('select').css('background-color', '#777777');
    $('textarea').css('background-color', '#777777');
}

/**
 * This function display a message like PHP view helper \Fundela\Trinket\View\Helper\DisplayMessage
 * @param text Message Text
 * @param level Message level (error, warn, success, info)
 */
export function displayMessage(text, level) {
    let cssClass = '';
    switch (level) {
        case 'error':
            cssClass = 'w3-red';
            break;
        case 'warn':
            cssClass = 'w3-orange';
            break;
        case 'success':
            cssClass = 'w3-green';
            break;
        case 'info':
            cssClass = 'w3-blue';
            break;
    }
    $('.killme').remove();
    $('body').append('<div class="w3-panel ' + cssClass + ' display-message killme"><span onclick="this.parentElement.remove()" class="w3-button w3-blue-gray w3-right"><i class="fas fa-times"></i></span><p>' + text + '</p></div>');
}
