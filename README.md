# Open NutriRecipe

Food recipes and their ingredients energy, fat, carb, protein and fibers.

Documents (help, draft, diagrams, etc.) are in the Application module (module/Application/data/doc).

The working live installation of NutriRecipe is at [NutriRecipe.org](https://nutrirecipe.org)
